import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AngularCommonComponent } from './angular-common.component';

describe('AngularCommonComponent', () => {
  let component: AngularCommonComponent;
  let fixture: ComponentFixture<AngularCommonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
