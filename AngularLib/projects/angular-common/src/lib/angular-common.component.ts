import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-angular-common',
  template: `
    <p>
      angular-common works!
    </p>
  `,
 styleUrls: ['./angular-common.component.scss'],
})
export class AngularCommonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
