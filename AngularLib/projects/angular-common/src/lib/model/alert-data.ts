export class AlertModelData {
    approve?: boolean;
    confirm?: boolean;
    message?: string;
    model?: any;

}
export class AlertInfoModel {
    title?: string;
    discription?: string;
    imageUrl?: string;
    isFullPath?: boolean;
    cancelButtonText?: string;
    confirmButtonText?: string;
    requiredRemark?: boolean;
    showRemark?: boolean;
    inputPlaceholder? : string;
    dismissible? : boolean = false;
    constructor(value: AlertInfoModel) {
        if (value) {
            this.title = value.title;
            this.discription = value.discription;
            this.imageUrl = value.imageUrl;
            this.isFullPath = value.isFullPath;
            this.cancelButtonText = value.cancelButtonText;
            this.confirmButtonText = value.confirmButtonText;
            this.requiredRemark = value.requiredRemark || false;
            this.showRemark = value.showRemark || false;
            this.inputPlaceholder = value.inputPlaceholder;
        }
    }
}

export class AlertData {
    private static data: any = '';
    constructor() { }
    static getConfig() {
        return AlertData.data;
    }
    static setConfig(data) {
        AlertData.data = data;
    }
}