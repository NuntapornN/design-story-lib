import { AssetsService } from './../services/assets.service';
import { Attachment } from './venio.model';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Observable, BehaviorSubject, iif } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import heic2any from 'heic2any';
import { DEFAULT_PICTURE_PLACEHOLDER } from '../constansts/global.constansts';
import { LibConfig, Configuration } from './config';

export class CloudAttachment extends Attachment {
  private localUrl: SafeResourceUrl;
  public isLoaded = false;
  public localUrl$: Observable<SafeResourceUrl>;
  localUrlSubject: BehaviorSubject<SafeResourceUrl>;

  /**
   *
   */
  constructor(
    private base: Attachment,
    private httpClient: HttpClient,
    private sanitizer: DomSanitizer,
    private assets: AssetsService) {
    super(base);
    this.localUrlSubject = new BehaviorSubject<SafeResourceUrl>(this.localUrl);
    this.localUrl$ = this.localUrlSubject.asObservable();
  }

  downloadData(isAllCompany = false) {
    const url = this.getFullPath(this.base.resourceUrl || this.base.url || this.base.thumbnail);
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'blob',
      headers: isAllCompany ? null : {
        'x-goog-encryption-key': Configuration.getConfig().storageEncryptionKey,
        'x-goog-encryption-key-sha256': Configuration.getConfig().storageEncryptionKeyHash,
        'x-goog-encryption-algorithm': 'AES256'
      },
    });
  }

  download(isAllCompany = false) {
    if (this.isLoaded) { return; }
    const url = this.getFullPath(this.resourceUrl || this.url || this.thumbnail);
    if (url === null) {
      this.localUrlSubject.next(this.sanitizer.bypassSecurityTrustResourceUrl(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.UnSupport)));
      return;
    }

    // const imageStorage = DependenciesInjector.getInjector().get(ImageStorageService);

    // const cached = imageStorage.getBlobUrl(url);
    // if (cached) {
    //   this.localUrlSubject.next(this.sanitizer.bypassSecurityTrustResourceUrl(cached));
    //   return;
    // }
    this.httpClient.get(url, {
      observe: 'response',
      responseType: 'blob',
      headers: isAllCompany ? null : {
        'x-goog-encryption-key': Configuration.getConfig().storageEncryptionKey,
        'x-goog-encryption-key-sha256': Configuration.getConfig().storageEncryptionKeyHash,
        'x-goog-encryption-algorithm': 'AES256'
      },
    }).toPromise().then((response: HttpResponse<Blob>) => {
      if (response.body.type === 'image/heic') {
        const blob: Blob = response.body;
        heic2any({
          blob,
          toType: 'image/jpeg',
          quality: 0.1,
        }).then((conversionResult) => {
          this.isLoaded = true;
          const dataUrl = URL.createObjectURL(conversionResult);
          // imageStorage.addBlobUrl(url, dataUrl);
          this.localUrlSubject.next(this.sanitizer.bypassSecurityTrustResourceUrl(dataUrl));
        });
      } else {
        this.isLoaded = true;
        const dataUrl = URL.createObjectURL(response.body);
        // imageStorage.addBlobUrl(url, dataUrl);
        this.localUrlSubject.next(this.sanitizer.bypassSecurityTrustResourceUrl(dataUrl));
      }
    }).catch(error => {
      this.localUrlSubject.next(error);
    });
  }

  private getFullPath(url: string) {
    if (url) { return url.startsWith('http') ? url : Configuration.getConfig().apiUrlBase + url; }
    return null;
  }
}
