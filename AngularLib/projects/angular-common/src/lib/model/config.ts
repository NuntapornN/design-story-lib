
import { BehaviorSubject, Observable } from 'rxjs';
import { LANGUAGE_KEY } from '../constansts/global.constansts';
export enum PhraseProject {
    GofiveCore = 'GofiveCore',
    Venio = 'Venio',
    empeo = 'empeo',
}
export enum Theme {
    VenioLight = 'venio-light',
    SalesbearLight = 'salesbear-light',
    EmpeoLight = 'empeo-light',
}
export enum Application {
    Venio = 'venio',
    Salesbear = 'salesbear',
    Empeo = 'empeo',
}
export class LibConfig {
    token?: string;
    storageEncryptionKey?: string;
    storageEncryptionKeyHash?: string;
    apiUrlBase?: string;
    phraseProject?: PhraseProject;
    assetsEnvironment? : string;
    theme? : Theme;
    application? : Application;
    production?: boolean; // เปลี่ยนเป็นใช้ assetsEnvironment แค่
}
export class Configuration {
    private static config = { 
        phraseProject: PhraseProject.GofiveCore,
        application: Application.Venio,
        theme : Theme.VenioLight
    } as LibConfig;
    private static configSubject = new BehaviorSubject(Configuration.config);
    private static language = new BehaviorSubject(LANGUAGE_KEY.EN);
    private static theme = new BehaviorSubject(Theme.VenioLight); 

    static configChange: Observable<LibConfig> = Configuration.configSubject.asObservable();
    static languageChange: Observable<string> = Configuration.language.asObservable();
    static themeChange: Observable<string> = Configuration.theme.asObservable();

    static getConfig() {
        return Configuration.config;
    }
    static setLibConfig(config: LibConfig) {
        Configuration.config = { ...Configuration.config, ...config };
        Configuration.configSubject.next(Configuration.config);
    }
    static setLanguage(language) {
        localStorage.setItem('user_prefered_language', language);
        Configuration.language.next(language);
    }
    static setTheme(theme) {
        Configuration.theme.next(theme)
    }
}