import { Configuration } from './config';
import { ElementRef } from '@angular/core';
import { Theme } from './config';
export class BaseComponent {
    public cssClass : string = 'gofive'
    constructor(public elementRef: ElementRef) {
        Configuration.themeChange.subscribe(res => {
            elementRef.nativeElement.setAttribute(`class`, this.getCssClass(res));
        })
    }

    getCssClass(theme : string){
        theme = theme? theme : Theme.VenioLight;
        return `${this.cssClass}-${theme}`; 
    }

}
