export enum AccessLogTypes {
  Login = 90001,
  PrePlan = 90002,
  Plan = 90003,
  ApprovedPlan = 90004,
  RevisedPlan = 90005,
  Checkin = 90006,
  Checkout = 90007,
  SubmitActivity = 90008,
  Accept = 90009,
  Reject = 90010,
  UnAccept = 90011,
  CancelPrePlan = 90012,
  CancelPlan = 90013,
}

export enum TodoTypes {
  WaitingApprovePlan = 1,
  WaitingCheckin = 2,
  WaitingCheckout = 3,
  WaitingApproveExpense = 4,
  WaitingApproveInventory = 5,
  WaitingApprovePR = 6,
  WaitingAccept = 7,
  ConversationInterested = 8,
  WaitingFeedback = 9,
  WorkIn = 10,
  eMemo = 11,
}

export enum FeedbackTypeEnum {
  RequestForAction = 1,
  FYI = 2,
}

export enum TypeFilterExpense {
  Filter = 0,
  Admin = 1,
  AllExpenses = 2,
  Approval = 3,
  MyExpense = 4,
  Other = 5,
}

export enum TypeDDLEmployeeGroup {
  Admin = 0,
  All = 1,
  MyTeam = 2,
  OtherTeam = 3,
}

export enum TypeFilterMemo {
  Filter = 0,
  Admin = 1,
  AllMemo = 2,
  Approval = 3,
  MyMemo = 4,
  Other = 5,
}

export enum TypeFilterFeedback {
  Filter = 0,
  AllFeedback = 1,
  WaitingRespond = 2,
  Related = 3,
  Other = 4,
}

export enum TypeFilterSaleOrder {
  Admin = 0,
  All = 1,
  MySaleOrder = 2,
  Other = 3,
}

export enum FeedbackActivityRelatedType {
  FromActivity = 1,
  ToActivity = 2,
}

export enum GroupPreferences {
  Activity = 150001,
  Expense = 150002,
  DataPolicy = 150003,
  SalesTarget = 150004,
  Security = 150005,
  Customer = 150006,
  Quotation = 150007,
  People = 150008,
}

export enum SettingPreferenceType {
  Boolean = 1,
  Number = 2,
  Dropdown = 3,
  TimeRange = 4,
  Decimal = 5,
  TextReadOnly = 6,
  TextReadAndWrite = 7
}

export enum TypeCaseEditUrgent {
  EditNote = 1,
  EditAssign = 2,
  EditFeedbackActivity = 3,
  EditFeedbackDeal = 4,
}


export class Permissions {

}

export class MasterTypes {

  public medKeyActivities: number[];
}

export class ActivityMeta {

  public startLatitude: number;
  public startLongitude: number;
  public startLocation: string;
  public startLocationName: string;
  public visitLatitude: number;
  public visitLongitude: number;
  public visitLocation: string;
  public visitLocationName: string;
  public returnLatitude: number;
  public returnLongitude: number;
  public returnLocation: string;
  public returnLocationName: string;
  public activityTypeId: number;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public assignToUserId: string;
}

export class ActivityDetailMeta {

  public hospitalId: number;
  public numberOfParticipant: number;
  public otherContact: string;
  public insuredName: string;
}

export class MyExpense {

  public statusId: number;
  public statusName: string;
  public expenses: Todo[];
}

export class MyFeedback {

  public priority: number;
  public date: Date;
  public statusId: number;
  public statusName: string;
  public feedbacks: Todo[];
}

export class Todo {

  public id: number;
  public activityId: number;
  public expenseId: number;
  public userId: string;
  public participants: ActivityParticipant[];
  public staffName: string;
  public subject: string;
  public createType: number;
  public createdBy: vwUserInfo;
  public teamName: string;
  public dateStart: Date;
  public dateEnd: Date;
  public planStart: Date;
  public planEnd: Date;
  public approveSequence: number;
  public staffs: vwUserInfo[];
  public dateApproved: Date;
  public dateCreated: Date;
  public locationName: string;
  public location: string;
  public keyActivity: string;
  public detailActivity: string;
  public statusId: number;
  public statusName: string;
  public pictureUrl: string;
  public hasAttachment: boolean;
  public attachments: Attachment[];
  public expenseTypeName: string;
  public expenseTypeId: string;
  public expenseTypeNames: string[];
  public expenseTypeIds: number[];
  public conversationTypeId: number;
  public conversationTypeName: string;
  public conversationSubTypeId: number;
  public conversationSubTypeName: string;
  public conversationOutcomeId: number;
  public conversationOutcomeName: string;
  public conversationQuantifiedId: number;
  public conversationQuantifiedName: string;
  public conversationActionId: number;
  public conversationActionName: string;
  public groupId: number;
  public customerId: number;
  public customerName: string;
  public customer: Customer;
  public total: number;
  public feedbackTypeName: string;
  public feedbackCategoryName: string;
  public feedbackCategory: string;
  public feedbackCategories: string[];
  public feedbackCateoryId: string;
  public feedbackCategoryIds: number[];
  public feedbackAnswer: string;
  public feedbackFrom: string;
  public feedbackFromName: string;
  public note: string;
  public reason: string;
  public poNo: string;
  public prNo: string;
  public invNo: string;
  public lastPostFullName: string;
  public canViewUserProfile: boolean;
  public todoType: TodoTypes;
  public dateProfileModified: Date;
  public feedbackCategoryDetail: vwFeedbackCategoryDetail[];
  public vendorName: string;
  public inventoryItems: vwInventoryItem[];
  public inventoryItemsStr: string;
  public waitingApproveByUserId: string;
  public isEdit: boolean;
  public dateFeedbackModified: Date;
  public totalExpense: number;
  public totalFeedback: number;
  public totalDistance: number;
  public latitude: number;
  public longitude: number;
  public visitPlaceId: string;
}

export class Detail {
  public detailType: string;
  public order: Order;
  public deal: Deal;
  public vwDeals: vwDeal[];
  public attachments: Attachment[];
  public activity: Activity;
  public expenses: Expense;
  public feedbacks: Feedback[];
  public user: vwUserInfo;
  public expensesIsEdit: boolean;
  public feedbackIsEdit: boolean;
  public activityId: number;
  public expensesId: number;
  public feedbackIds: number[];
}

export class MyActivity {

  public date: Date;
  public activities: vwActivityLite[];
  public count: number;
}

export class UserAvailable {

  public userId: string;
  public fullname: string;
  public isAvailable: boolean;
  public teamId: number;
  public teamName: string;
  public remark: string;
  public province: string;
  public pictureUrl: string;
  public roleName: string;
}

export class SearchStaffWithStatus {

  public staffs: string[];
  public start: Date;
  public end: Date;
  public getAll: boolean;
}

export class ApproveCancelModel {

  public ids: number[];
  public reason: string;
}

export class CopyModel {

  public id: number;
}

export class ActivityCentric {

  public staffName: string;
  public dateTime: Date;
}

export class CustomerCentric {

  public customer: Customer;
  public activity: ActivityCentric;
  public totalActivity: number;
  public totalContact: number;
  public totalConversation: number;
  public totalDistance: number;
  public group: number;
}

export class CustomerCentricGroup {

  public group: number;
  public customerCentrics: CustomerCentric[];
}

export class CustomerCentricAllGroup {

  public customerGroups: CustomerGroup[];
  public customerCentricGroups: CustomerCentricGroup[];
}

export class KeyValue {

  public key: string;
  public value: string;
}

export class DateRangeFilterModel {

  public date: Date;
  public dateStart: Date;
  public dateEnd: Date;
}

export class DataPaginationRequestModel {

  public keyword: string;
  public start: number;
  public pageLength: number;
}

export class GroupRolePermission {

  public roleId: string;
  public roleName: string;
  public groupRolePermissionByparent: GroupRolePermissionByParent[];
}

export class GroupRolePermissionByParent {

  public parent: number;
  public rolePermission: vwRolePermission[];
}

export class FeedbackAnswers {

  public feedbackAnswer: FeedbackAnswer;
  public feedback: vwFeedback;
  public feedbackStates: FeedbackState[];
}

export class FeedbackConversation {

  public caseId: number;
  public userInfo: vwUserInfo;
  public customer: Customer;
  public dateLastAnswer: Date;
  public feedbackAnswers: FeedbackAnswer[];
  public isReopen: boolean;
  public isClose: boolean;
}

export class NotificationActionCase {

  public caseId: number;
  public subjectCase: string;
  public linkSubject: string;
  public userId: string;
  public name: string;
  public isActivity: boolean;
  public isLink: boolean;
  public isAssign: boolean;
  public type: number;
}

export class LocationAPIGoogle {

  public routes: RouteAPIGoogle[];
}

export class RouteAPIGoogle {

  public legs: RouteLegAPIGoogle[];
}

export class RouteLegAPIGoogle {

  public distance: DistanceAPIGoogle;
  public startAddress: string;
  public startLocation: Location;
  public endAddress: string;
  public endLocation: Location;
}

export class DistanceAPIGoogle {

  public text: string;
  public value: number;
}

export class Location {

  public lat: number;
  public lng: number;
}

export class FeedbackAnswer {

  public attachments: Attachment[];
  public createdByUser: vwUserInfo;
  public type: number;
  public assignedToUserId: string;
  public statusId: number;
  public stateId: number;
  public subject: string;
  public feedbackAnswerId: number;
  public feedbackId: number;
  public answer: string;
  public isTrue: boolean;
  public hasAttachment: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public feedback: Feedback;
}

export class vwUserCustomerVisit {

  public visitedInfo: string;
  public planCount: number;
  public activityCount: number;
  public contactCount: number;
  public lastVisited: Date;
  public userId: string;
  public customerId: number;
  public customerName: string;
  public contactId: number;
  public contactName: string;
  public statusId: number;
  public activityId: number;
  public dateVisited: Date;
  public keyActivity: string;
  public orgString: string;
}

export class vwActivityState {

  public pictureUrl: string;
  public activityStateId: number;
  public activityId: number;
  public statusId: number;
  public statusName: string;
  public remark: string;
  public createdByUserId: string;
  public fullname: string;
  public dateCreated: Date;
  public dateProfileModified: Date;
}

export class vwFeedback {

  public feedbackNo: string;
  public pictureUrl: string;
  public lastDateAgo: string;
  public feedbackId: number;
  public activityId: number;
  public teamId: number;
  public teamName: string;
  public dateCreated: Date;
  public activityTypeId: number;
  public activityTypeName: string;
  public feedbackTypeId: number;
  public feedbackTypeName: string;
  public feedbackCategories: string;
  public feedbackCategoryIds: string;
  public feedbackFromId: number;
  public feedbackFrom: string;
  public feedbackFromName: string;
  public note: string;
  public statusId: number;
  public statusName: string;
  public createdByUserName: string;
  public updatedByUserName: string;
  public createdByUserId: string;
  public feedbackAnswer: string;
  public answerBy: string;
  public detailActivities: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public activityStatusId: number;
  public staffCode: string;
  public staffId: string;
  public username: string;
  public fullname: string;
  public userRoleId: string;
  public userOrgString: string;
  public userOrgLevel: number;
  public roleName: string;
  public customerName: string;
  public customerAlias: string;
  public userId: string;
  public companyId: number;
  public customerId: number;
  public dateLastAnswer: Date;
  public dateUpdated: Date;
  public lastPostUserId: string;
  public lastPostFullName: string;
  public lastAnswer: string;
  public subject: string;
}

export class Activity {

  public activityNo: string;
  public durationTime: number;
  public createType: number;
  public isEdit: boolean;
  public isCancel: boolean;
  public canApprove: boolean;
  public isVisibleCustomerDetail: boolean;
  public rowIndex: number[];
  public staffName: string;
  public detailActivities: number[];
  public detailActivity: string;
  public detailActivityModels: ActivityType[];
  public contactIds: number[];
  public assignToUserIds: string[];
  public assignToFullnames: string[];
  public assignToUsers: vwUserInfo[];
  public remark: string;
  public description: string;
  public contact: string;
  public contacts: Contact[];
  public keyActivity: string;
  public assignTo: string;
  public statusName: string;
  public assignToUser: vwUserInfo;
  public assignedBy: string;
  public cancelReason: string;
  public attachments: Attachment[];
  public customerName: string;
  public pictureUrl: string;
  public uploadedAttachments: Attachment[];
  public totalFeedbackAmount: number;
  public reviseReason: string;
  public saleReport: SaleOrder;
  public site: Site;
  public isEditWorkIn: boolean;
  public isActiveSaleReport: boolean;
  public comment: ActivityLog[];
  public waitingApproveBy: vwUserInfo[];
  public activityId: number;
  public teamId: number;
  public subject: string;
  public groupId: number;
  public activityTypeId: number;
  public customerId: number;
  public projectId: number;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public notes: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public isActive: boolean;
  public statusId: number;
  public isSelectLocation: boolean;
  public isEditActivityReport: boolean;
  public isExpireActivityReport: boolean;
  public isAdHoc: boolean;
  public assignToUserId: string;
  public dateStartAgreement: Date;
  public dateEndAgreement: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public startLatitude: number;
  public startLongitude: number;
  public startLocation: string;
  public startLocationName: string;
  public startPlaceId: string;
  public visitLatitude: number;
  public visitLongitude: number;
  public visitLocation: string;
  public visitLocationName: string;
  public visitPlaceId: string;
  public returnLatitude: number;
  public returnLongitude: number;
  public returnLocation: string;
  public returnLocationName: string;
  public returnPlaceId: string;
  public totalDistance: number;
  public estimatedTravelAmount: number;
  public totalExpenseAmount: number;
  public dateAppointmented: Date;
  public appointmentedByUserId: string;
  public dateApproved: Date;
  public approvedByUserId: string;
  public dateRejected: Date;
  public rejectedByUserId: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public userRoleId: string;
  public userOrgString: string;
  public userOrgLevel: number;
  public userHeadUserId: string;
  public checkoutLatitude: number;
  public checkoutLongitude: number;
  public checkoutLocation: string;
  public checkoutLocationName: string;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public type: number;
  public siteId: number;
  public deals: Deal[];
  public dealIds: number[];
  public activityContacts: ActivityContact[];
  public activityParticipants: ActivityParticipant[];
  public activityRoutes: ActivityRoute[];
  public activityStates: ActivityState[];
  public activitySubtypes: ActivitySubtype[];
  public expenses: Expense[];
  public feedbacks: Feedback[];
  public customer: Customer;
  public userInfo: UserInfo;
  public activityReportAttachments: Attachment[];
  public activityReport: string;
  public activityServiceId: number;
  public ownerUserId: string;
  public fromAvailablePermission: boolean;
  public activityType: ActivityType;
  public planOwner;
}

export class AspNetRole {

  public nameRole: string;
  public roleLevel: string;
  public users: vwUserInfo[];
  public id: string;
  public name: string;
  public teamId: number;
  public level: number;
  public roleName: string;
  public isActive: boolean;
  public isReportExclude: boolean;
  public companyId: number;
  public normalizedName: string;
  public concurrencyStamp: string;
  public rolePermissions: RolePermission[];
}

export class ContactUs {

  public companyName: string;
  public name: string;
  public email: string;
  public phone: string;
  public message: string;
}
export class CustomerConfig {
  search?;
  isMerge?;
  skip?;
  take?;
  type?;
  customerId?;
  hasContact?;
}
export class Customer {
  public name: string;
  public uniqueName: string;
  public secondaryName: string;
  public aliasNames: string;
  public customerOwnersStr: string[];
  public pictureUrl: string;
  public picture: Attachment;
  public attachment: object;
  public customerOwners: vwUserInfo[];
  public createdByUserName: string;
  public customerGroupName: string;
  public distance: number;
  public assign: ActivityCentric;
  public conversation: ActivityCentric;
  public conversion: ActivityCentric;
  public upcoming: ActivityCentric;
  public lastCheckin: ActivityCentric;
  public lastDeal: ActivityCentric;
  public contact: Contact;
  public customerClassification: CustomerClassification;
  public customerId: number;
  public companyId: number;
  public referenceId: number;
  public customerCode: string;
  public customerName: string;
  public customerNameEN: string;
  public type: number;
  public customerType: number;
  public branch: string;
  public addressPart1: string;
  public addressPart2: string;
  public province: string;
  public zipCode: string;
  public phone: string;
  public mobile: string;
  public fax: string;
  public notes: string;
  public isActive: boolean;
  public dateChangeToCustomer: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public placeId: string;
  public customerGroupId: number;
  public leadStatus: number;
  public hasPicture: boolean;
  public email: string;
  public companyName: string;
  public socialAccount: string;
  public sourceOfLead: number;
  public dateMerged: Date;
  public mergeDetails: string;
  public creditTerm: number;
  public classificationId: number;
  public activities: Activity[];
  public addresses: Address[];
  public contacts: Contact[];
  public conversations: Conversation[];
  public customerAlias: CustomerAlia[];
  public customerInterests: CustomerInterest[];
  public customerTargets: CustomerTarget[];
  public customerWallets: CustomerWallet[];
  public deals: Deal[];
  public inventories: Inventory[];
  public leadStates: LeadState[];
  public userMapToCustomers: UserMapToCustomer[];
  public company: Company;
  public customerGroup: CustomerGroup;
  public masterType: MasterType;
  public taxId: string;
  public customFields: CustomerDynamicData[];
}

export class Customer_Contact_Result {

  public pictureUrl: string;
  public picture: Attachment;
  public contactId: number;
  public contactName: string;
  public email: string;
  public position: string;
  public phone: string;
  public mobile: string;
  public dateCreated: Date;
  public dateModified: Date;
  public hasPicture: boolean;
  public notes: string;
  public totalPlan: number;
  public totalActivity: number;
  public totalFeedback: number;
}

export class Team {

  public users: vwUserInfo[];
  public teamId: number;
  public parentTeamId: number;
  public groupId: number;
  public companyId: number;
  public teamName: string;
  public note: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public teams: Team[];
  public userTeams: UserTeam[];
  public parentTeam: Team;
}

export class TeamMetadata {

  public teamName: string;
}

export class Contact {

  public recordId: number;
  public branchCode: string;
  public nameContact: string;
  public pictureUrl: string;
  public customerName: string;
  public picture: Attachment;
  public attachments: Attachment[];
  public attachmentsFb: object;
  public activityContact: ActivityContact;
  public dateVisit: Date;
  public userVisit: vwUserInfo;
  public contactId: number;
  public customerId: number;
  public contactName: string;
  public contactNameEN: string;
  public nickname: string;
  public position: string;
  public email: string;
  public phone: string;
  public mobile: string;
  public notes: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public teamId: number;
  public hasPicture: boolean;
  public birthDate: Date;
  public activityContacts: ActivityContact[];
  public customer: Customer;
}

export class ContactMetadata {

  public email: string;
}

export class ImportModels {
  public attachments: object;
}
export class ChatFilter {
  search: string;
  customerIds: number[];
  fromTimestamp: number;
  toTimestamp: number;
  customerId: number;
  interestedIn: number;
  employeeId: number;
}
export class Mentionees {
  index: number;
  length: number;
  userId: string;
  name: string;
  user: User;
}
export class Mention {
  mentionees: Mentionees[];
}
export class Message {
  vwId?: string;

  id?: string;
  roomId: string;
  user?: User;
  content?: string;
  vwContent?: string;
  mention?: Mention;
  timestamp?: number;
  attachment?: ChatAttachment;
  toUser?: User;
  followupTimestamp?: number;
}
export class User {
  id: string; // venio_user_id
  companyId?: number;
  displayName?: string;
  originalName: string;
  avatar: string;
  role: string;
  phoneNo?: string;
  mobileNo?: string;
  email?: string;
  lineId?: string;
  facebookId?: string;
  note?: string;
  dateAssigned?: number;
}
export class ChatAttachment {
  attachmentId: number;
  filename: string;
  extension: string;
  fileSize: number;
  resourceUrl: string;
}
export class Room {
  id: string;
  user: User;
  assignToUser?: User;
  lastMessage: Message;
  unread?: number;
  userMessageTimestamp: number;
  isReplied?: boolean;
  followupTimestamp?: number;
  followupContent?: string;
  participants?: User[];
  attachments?: ChatAttachment[];
}
export class Chat extends Room {
  messages: Message[];
  typingUser?: User[];
  messageContent?: string;
  customerName?: string;
  visible: boolean;
}

export class ContactChat {

}


export class Attachment {
  constructor(att?: Attachment) {
    if (att) {
      this.externalLink = att.externalLink;
      this.data = att.data;
      this.url = att.url;
      if (att.thumbnail) {
        if (att.thumbnail.startsWith('&') === false) { this.thumbnail = att.thumbnail; }
      }
      this.attachmentId = att.attachmentId;
      this.refId = att.refId;
      this.rawFile = att.rawFile;
      this.type = att.type;
      this.filename = att.filename;
      this.extension = att.extension;
      this.fileSize = att.fileSize;
      this.isActive = att.isActive;
      this.createdByUserId = att.createdByUserId;
      this.dateCreated = att.dateCreated;
      this.resourceUrl = att.resourceUrl;
      this.urlUpload = att.urlUpload;
      this.localFileURL = att.localFileURL;
      this.urlResponseUpload = att.urlResponseUpload;
      this.isProcessed = att.isProcessed;
      this.companyId = att.companyId;
      this.publicUrl = att.publicUrl;
      this.isLocal = att.isLocal;
    }
  }
  public externalLink: string;
  public data: string;
  public url: string;
  public thumbnail: string;
  public rawFile: string;
  public attachmentId: number;
  public refId: number;
  public type: number;
  public filename: string;
  public extension: string;
  public fileSize: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public resourceUrl: string;
  public urlUpload: string;
  public localFileURL: string;
  public urlResponseUpload: string;
  public isProcessed: boolean;
  public companyId: number;
  public publicUrl: string;
  public isLocal: boolean;
}
export class LocaleStringResource {

  public key: string;
  public id: string;
  public resourceValue: string;
  public localeResourceKeyId: string;
  public languageId: string;
  public language: Language;
  public localeResourceKey: LocaleResourceKey;
}

export class Language {

  public stringResources: object;
  public id: string;
  public name: string;
  public languageCulture: string;
  public flagImageFileName: string;
  public rightToLeft: boolean;
  public isActive: boolean;
  public localeStringResources: LocaleStringResource[];
}

export class vwActivityLite {

  public approvable: boolean;
  public pictureUrl: string;
  public totalExpense: number;
  public totalFeedback: number;
  public attachment: Attachment[];
  public assignToUsers: vwUserInfo[];
  public addresses: Address[];
  public userId: string;
  public staffName: string;
  public orgString: string;
  public orgLevel: number;
  public teamId: number;
  public teamName: string;
  public subject: string;
  public activityId: number;
  public statusId: number;
  public statusName: string;
  public dateActivity: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateActivityStart: Date;
  public dateActivityEnd: Date;
  public activityTypeId: number;
  public keyActivity: string;
  public detailActivityId: number;
  public detailActivities: string;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public isAdHoc: boolean;
  public dateCreated: Date;
  public cancelReason: string;
  public customerId: number;
  public customerName: string;
  public customerAlias: string;
  public contactNames: string;
  public assignedByUserId: string;
  public assignedBy: string;
  public notes: string;
  public dateProfileModified: Date;
  public companyId: number;
  public hasAttachment: boolean;
  public groupId: number;
  public approvedByUserId: string;
  public dateApproved: Date;
  public staffFirstname: string;
  public staffLastname: string;
  public customerPlaceId: string;
  public visitPlaceId: string;
  public totalDistance: number;
}

export class CreatedPlan {

  public activityId: number;
  public startLatitude: number;
  public startLongitude: number;
  public startLocation: string;
  public startLocationName: string;
  public visitLocation: string;
  public visitLocationName: string;
  public visitLatitude: number;
  public visitLongitude: number;
  public returnLocation: string;
  public returnLocationName: string;
  public returnLatitude: number;
  public returnLongitude: number;
  public totalDistance: number;
  public subject: string;
  public application: string;
  public phoneNumber: string;
  public province: string;
  public customerId: number;
  public contactIds: number[];
  public notes: string;
  public contactPosition: string;
  public keyActivity: string;
  public activityTypeId: number;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public assignToUserId: string;
  public staffName: string;
  public detailActivities: number[];
  public userInfo: UserInfo;
}

export class ActivitySubtype {

  public typeName: string;
  public activityTypeId: number;
  public activityId: number;
  public isActive: boolean;
  public activity: Activity;
  public activityType: ActivityType;
}

export class ActivityDetail {

  public transferRemark: string;
  public transferFrom: string;
  public transferReason: string;
  public dateMemo: Date;
  public groupTypeId: number;
}

export class ActivityType {

  public detailActivity: ActivityType[];
  public activityTypeId: number;
  public parentId: number;
  public companyId: number;
  public teamId: number;
  public groupTypeId: number;
  public seq: number;
  public activityTypeName: string;
  public activityTypeDesc: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public activitySubtypes: ActivitySubtype[];
  public isRequireReport: boolean;
  public isRequiredVisit: boolean;
  public labelColorCode: string;
}

export class ActivityTypeMeta {

  public activityTypeName: string;
}

export class vwUserInfo {
  public uniqueName: string;
  public notes: string;
  public pictureUrl: string;
  public permissions: number[];
  public preferences: object;
  public preferedLanguage: string;
  public teamIds: number[];
  public statusUser: number;
  public isSelected: boolean;
  public needOnboard: boolean;
  public isAgreementPDPA: boolean;
  public companyAddresses: CompanyAddres[];
  public workInSites: Site[];
  public userCompanies: vwUserCompany[];
  public hiddenMenus: Object[];
  public specialPermissions: Object[];
  public dateAssigned: Date;
  public userId: string;
  public username: string;
  public companyId: number;
  public companyName: string;
  public teamId: number;
  public teamName: string;
  public staffCode: string;
  public staffId: string;
  public title: string;
  public firstname: string;
  public lastname: string;
  public fullname: string;
  public nickname: string;
  public position: string;
  public positionName: string;
  public division: number;
  public divisionName: number;
  public department: number;
  public departmentName: number;
  public email: string;
  public extensionNo: number;
  public phoneNumber: string;
  public picture: string;
  public parentId: number;
  public orgString: string;
  public orgLevel: number;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public udid1: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public hasPicture: boolean;
  public head1: string;
  public head2: string;
  public headUserId: string;
  public isReceiveImportEmail: boolean;
  public role: string;
  public roleId: string;
  public level: number;
  public placeId: string;
  public siteId: number;
  public storageEncryptionKey: string;
  public storageEncryptionKeyHash: string;
  public user: vwUserInfo;
  public allowedToChangePictureProfile: boolean;
  public expireIn: number;
  public subscriptionStatusId: number;
  public announcementPictureUrl: string;
}

export class GetEmployeeFeedbackByCustomerId_Result {

  public pictureUrl: string;
  public userId: string;
  public firstname: string;
  public lastname: string;
  public dateCreated: Date;
  public dateModified: Date;
}

export class GroupTeam {

  public teamId: number;
  public team: string;
  public user: vwUserInfo[];
  public staffs: UserAvailable[];
}

export class TeamStaffs {

  public teamId: number;
  public team: string;
  public user: Employee_Activity_Result[];
}

export class Employee_Activity_Result {

  public pictureUrl: string;
  public staffCode: string;
  public role: string;
  public teams: TeamStaffs[];
  public userId: string;
  public fullname: string;
  public firstname: string;
  public lastname: string;
  public position: string;
  public positionName: string;
  public email: string;
  public phoneNumber: string;
  public hasPicture: boolean;
  public picture: string;
  public teamId: number;
  public teamName: string;
  public dateCreated: Date;
  public dateModified: Date;
  public orgString: string;
  public isActive: boolean;
  public totalPlan: number;
  public totalActivity: number;
  public totalFeedback: number;
  public nickname: string;
}

export class PlanDate {

  public date: Date[];
}

export class Feedback {

  public feedbackNo: string;
  public isDeletable: boolean;
  public feedbackTypeName: string;
  public createBy: string;
  public answerBy: string;
  public feedbackFrom: string;
  public feedbackCategories: FeedbackCategory[];
  public feedbackCategory: string;
  public statusName: string;
  public isClosed: boolean;
  public isAnswer: boolean;
  public dateCheckout: Date;
  public isEdit: boolean;
  public pictureUrl: string;
  public activityIds: number[];
  public relatedActivities: Todo[];
  public relatedDeals: Deal[];
  public dealIds: number[];
  public feedbackTypeMapping: number;
  public categoryId: number;
  public assignTo: vwUserInfo;
  public attachments: Attachment[];
  public customerName: string;
  public isReopen: boolean;
  public type: number;
  public feedbackId: number;
  public companyId: number;
  public customerId: number;
  public activityId: number;
  public toActivityId: number;
  public subject: string;
  public statusId: number;
  public feedbackTypeId: number;
  public feedbackFromId: number;
  public feedbackFromName: string;
  public note: string;
  public assignedToUserId: string;
  public dateAssigned: Date;
  public dateCase: Date;
  public dateDue: Date;
  public feedbackAnswer: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public feedbackActivities: FeedbackActivity[];
  public feedbackAnswers: FeedbackAnswer[];
  public feedbackCategoryDetails: FeedbackCategoryDetail[];
  public feedbackDeals: FeedbackDeal[];
  public feedbackStates: FeedbackState[];
  public activity: Activity;
  public feedbackType: FeedbackType;
}

export class FeedbackCategoryDetail {

  public feedbackCategoryName: string;
  public feedbackId: number;
  public feedbackCategoryId: number;
  public isActive: boolean;
  public feedback: Feedback;
  public feedbackCategory: FeedbackCategory;
}

export class AutoNumber {
  public autoNumberId: number;
  public companyId: number;
  public moduleId: number;
  public dateFormate: boolean;
  public numberStart: number;
  public numberEnd: number;
  public lastNumber: number;
  public prefix: string;
  public suffix: string;
  public length: number;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public culture: string;
  public recurring: string;
  public description: string;
  public lastRunningNumber: string;
  public isActive: boolean;
  public cultureFormat: number;
  public dateFormat: number;
  public recurringFormat: boolean;
}

export class Report_ActivityBreakdownByDetailActivity_Result {

  public statusGroup: string;
  public rowNumber: number;
  public recordCount: number;
  public dateCreatePlan: Date;
  public datePlan: Date;
  public keyActivity: string;
  public detailActivity: string;
  public customerName: string;
  public contactName: string;
  public staffCode: string;
  public staffName: string;
  public staffLevel: string;
  public managerName1: string;
  public managerName2: string;
  public teamName: string;
  public actionBySup: string;
  public dateActionBySup: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public checkinLocation: string;
  public planLocation: string;
  public workingMinutes: number;
  public locationName: string;
  public statusName: string;
  public statusId: number;
  public activityId: number;
  public notes: string;
}

export class Report_KeyActivity_Result {

  public percent: number;
  public keyActivity: string;
  public count: number;
  public activityTypeId: number;
  public totalHours: number;
  public companyId: number;
}

export class Expense {

  public expenseNo: string;
  public canApprove: boolean;
  public isEdit: boolean;
  public deletable: boolean;
  public total: number;
  public statusName: string;
  public reason: string;
  public activityLogs: any[];
  public activities: Todo[];
  public attachments: Attachment[];
  public dateCheckout: Date;
  public totalDistance: number;
  public customerName: string;
  public dateCheckin: Date;
  public note: string;
  public pictureUrl: string;
  public expenseId: number;
  public teamId: number;
  public activityId: number;
  public statusId: number;
  public dateTransaction: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public dateApproved: Date;
  public modifiedByUserId: string;
  public remark: string;
  public subject: string;
  public expenseDetails: ExpenseDetail[];
  public expenseStates: ExpenseState[];
  public activity: Activity;
}

export class GetExpense {

  public expenses: Expense[];
  public approveExpenses: Expense[];
}

export class Report_DashboardTaskOverview_Result {

  public statusGroup: string;
  public userManager: User_ReportingUser_Result[];
  public visitUserManager: string[];
  public statusId: number;
  public dateAppointmentStart: Date;
  public statusName: string;
  public isCheckin: number;
  public count: number;
  public userId: string;
  public firstName: string;
  public lastName: string;
  public fullname: string;
  public roleName: string;
}

export class Report_DashboardTaskByZone_Result {

  public statusGroup: string;
  public userManager: User_ReportingUser_Result[];
  public visitUserManager: string[];
  public statusId: number;
  public statusName: string;
  public dateAppointmentStart: Date;
  public isCheckin: number;
  public count: number;
  public userId: string;
  public firstName: string;
  public lastName: string;
  public fullname: string;
  public roleName: string;
  public managerFullName: string;
  public managerUserId: string;
  public teamId: number;
  public teamName: string;
}

export class VisitStaff {

  public zoneName: string;
  public currentName: string;
  public sellerName: string;
  public isVisit: boolean;
  public visitSellerManager: string[];
  public nonVisitSellerManager: string[];
}

export class UserZones {

  public zoneName: string;
  public visitSellerManager: number;
  public nonVisitSellerManager: number;
}

export class Report_ExpenseTable_Result {

  public managerId1: string;
  public managerId2: string;
  public manager1: User_ReportingManager_Result;
  public manager2: User_ReportingManager_Result;
  public zoneAgent: string;
  public trainings: vwExpenseDetail[];
  public others: vwExpenseDetail[];
  public columnHeader: string[];
  public columnHeader1: string[];
  public userId: string;
  public staffName: string;
  public staffCode: string;
  public headName: string;
  public staffLevel: number;
  public staffRole: string;
  public teamName: string;
  public dateActivity: Date;
  public keyActivity: string;
  public detailActivity: string;
  public place: string;
  public customer: string;
  public contact: string;
  public planDistance: number;
  public hours: number;
  public minutes: number;
  public expenseId: number;
  public remark: string;
  public teamId: number;
  public activityId: number;
  public createdByUserId: string;
  public dateTransaction: Date;
  public dateCreated: Date;
  public fuelQty: number;
  public fuelAmt: number;
  public milesStart: number;
  public milesEnd: number;
  public usedFuelCard: boolean;
  public accommodationQty: number;
  public accommodationAmt: number;
  public mealStaffQty: number;
  public mealStaffAmt: number;
  public entertainmentAmt: number;
  public taxi: number;
  public tollway: number;
  public parking: number;
  public totalTravel: number;
  public allowanceAmt: number;
  public trainingAmt: number;
  public otherAmt: number;
  public total: number;
  public locationName: string;
  public firstSubmittedDate: Date;
  public dateApproved: Date;
  public dateRejected: Date;
  public location: string;
  public visitLocationName: string;
  public subject: string;
}

export class UserManagement {

  public userId: string;
  public username: string;
  public teamId: number;
  public firstname: string;
  public lastname: string;
  public email: string;
  public phoneNumber: string;
  public roleId: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public head1: string;
  public staffCode: string;
  public picture: string;
  public hasPicture: boolean;
  public companyId: number;
  public nickname: string;
}

export class vwExpensesLite {

  public isEdit: boolean;
  public canApprove: boolean;
  public attachment: Attachment[];
  public expenseType: number[];
  public pictureUrl: string;
  public dateStart: Date;
  public dateEnd: Date;
  public expenseId: number;
  public companyId: number;
  public teamId: number;
  public userId: string;
  public staffName: string;
  public orgString: string;
  public orgLevel: number;
  public role: string;
  public companyName: string;
  public teamName: string;
  public statusId: number;
  public statusName: string;
  public expenseTypeIds: string;
  public expenseTypeNames: string;
  public activityId: number;
  public dateActivity: Date;
  public keyActivityId: number;
  public keyActivity: string;
  public detailActivities: string;
  public contactNames: string;
  public customerName: string;
  public customerAlias: string;
  public total: number;
  public dateCreated: Date;
  public dateTransaction: Date;
  public dateProfileModified: Date;
  public hasAttachment: boolean;
  public remark: string;
  public dateApproved: Date;
  public approvedByUserId: string;
  public approvedByFullname: string;
  public waitingApproveByUserId: string;
  public dateCheckout: Date;
  public waitingApproveByRoleId: string;
  public reason: string;
  public subject: string;
  public expenseDetails: ExpenseDetail[];
}

export class vwInventory {

  public isEdit: boolean;
  public pictureUrl: string;
  public userinfo: vwUserInfo;
  public inventoryItem: InventoryItem[];
  public inventoryStates: InventoryState[];
  public inventoryId: number;
  public statusId: number;
  public statusName: string;
  public statusNameEn: string;
  public isActive: boolean;
  public customerId: number;
  public customerName: string;
  public customerAlias: string;
  public addressPart1: string;
  public inventoryRemark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approvedByUserId: string;
  public dateApproved: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public inventoryStateId: number;
  public stateRemark: string;
  public actionByUserId: string;
  public dateCreatedStates: Date;
  public createUserName: string;
  public userOrgString: string;
  public orgLevel: number;
  public firstname: string;
  public lastname: string;
  public fullName: string;
  public teamId: number;
  public teamName: string;
  public email: string;
  public phoneNumber: string;
  public subject: string;
  public companyId: number;
  public total: number;
  public waitingApproveByUserId: string;
  public invNo: string;
  public waitingApproveByRoleId: string;
}

export class ActivityTypeList {

  public activityTypeId: number;
  public activityTypeName: string;
}

export class ExpenseDetail {

  public dateCreated: Date;
  public expenseDetailId: number;
  public expenseId: number;
  public expenseTypeId: number;
  public categoryId: number;
  public categoryName: string;
  public title: string;
  public milesStart: number;
  public milesEnd: number;
  public quantity: number;
  public amount1: number;
  public amount2: number;
  public amount3: number;
  public note: string;
  public hours: number;
  public minutes: number;
  public usedFuelCard: boolean;
  public expense: Expense;
  public expenseType: ExpenseType;
}

export class EventModel {

  public name: string;
  public date: Date;
}

export class CompanyDemo {

  public companyName: string;
  public domainName: string;
  public dateDemo: string;
}

export class CreateInventory {

  public inventoryId: number;
  public statusId: number;
  public isActive: boolean;
  public subject: string;
  public customerId: number;
  public companyId: number;
  public remark: string;
  public createByUserId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public dateCreated: Date;
  public dateModified: Date;
  public dateApproved: Date;
  public userInfo: vwUserInfo;
  public inventoryDetail: InventoryDetais[];
}

export class InventoryDetais {

  public partId: number;
  public part: Part;
  public qty: number;
  public price: number;
}

export class PartType {

  public selectmore: boolean;
  public partTypeId: number;
  public parentId: number;
  public partTypeName: string;
  public productCode: number;
  public companyId: number;
  public orgString: string;
  public orgLevel: number;
  public isActive: boolean;
  public parts: Part[];
}

export class BasicFilter {

  public dateCreated: string;
  public searchText: string;
  public categoryId: string;
  public categoryIds: string[];
  public statusId: string;
  public id: string;
  public typeFilter: number;
  public userIds: string[];
  public userId: string;
  public statusIds: string[] | number[];
  public dateFrom: Date;
  public dateTo: Date;
  public customerId: number;
  public customerIds: number[];
  public skip: number;
  public pagelength: number;
  public refId: number;
  public viewType: number;
  public type: any;
}

export class vwPurchaseRequisition {

  public pictureUrl: string;
  public attachmentArray: object;
  public purchaseItems: PurchaseItem[];
  public subjectView: string;
  public lastState: PurchaseState;
  public attachments: Attachment[];
  public printPoUrl: string;
  public printPrUrl: string;
  public isEdit: boolean;
  public purchaseRequisitionId: number;
  public department: string;
  public dateRequired: Date;
  public isActive: boolean;
  public remark: string;
  public createdByUserId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public dateCreated: Date;
  public dateModified: Date;
  public dateApproved: Date;
  public username: string;
  public firstname: string;
  public lastname: string;
  public statusId: number;
  public statusName: string;
  public statusNameEn: string;
  public subject: string;
  public companyId: number;
  public orgString: string;
  public status: number;
  public purchaseStateId: number;
  public expr1: string;
  public createByUserId: string;
  public expr2: Date;
  public vendorName: string;
  public position: string;
  public email: string;
  public phoneNumber: string;
  public total: number;
  public fullName: string;
  public teamId: number;
  public teamName: string;
  public vendorAddress: string;
  public prNo: string;
  public poNo: string;
  public waitingApproveByUserId: string;
  public role: string;
  public waitingApproveByRoleId: string;
}

export class PurchaseRequisition {

  public lastState: PurchaseState;
  public attachments: Attachment[];
  public purchaseRequisitionId: number;
  public prNo: string;
  public poNo: string;
  public vendorId: number;
  public vendorName: string;
  public vendorAddress: string;
  public department: string;
  public dateRequired: Date;
  public status: number;
  public isActive: boolean;
  public subject: string;
  public companyId: number;
  public remark: string;
  public createdByUserId: string;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public dateCreated: Date;
  public dateModified: Date;
  public dateApproved: Date;
  public purchaseItems: PurchaseItem[];
  public purchaseStates: PurchaseState[];
  public userInfo: UserInfo;
}

export class Part {

  public pictureUrl: string;
  public attachments: Attachment;
  public attachmentGroupSKU: Attachment;
  public partId: number;
  public partTypeId: number;
  public partCode: string;
  public productCode: number;
  public partName: string;
  public name: string;
  public qty: number;
  public price: number;
  public notes: string;
  public companyId: number;
  public unitId: number;
  public unit: string;
  public salesUnit: string;
  public isForSales: boolean;
  public isForInventory: boolean;
  public isForPurchase: boolean;
  public isActive: boolean;
  public isEdit: boolean;
  public inventoryItems: InventoryItem[];
  public orderDetails: OrderDetail[];
  public purchaseItems: PurchaseItem[];
  public saleOrderDetails: SaleOrderDetail[];
  public siteItems: SiteItem[];
  public unitConversions: UnitConversion[];
  public partType: PartType;
  public copy: number;
  public weight: number;
  public weightUnitId: number;
  public brand: Brand;
  public brandId: number;
  public brandName: string;
  public productSKU: Part[];
  public listAnother: Part[];
  public productValues: productProperty[];
  public propertiesSelect: string;
}

export class PartMeta {

  public partTypeId: number;
  public partName: string;
  public price: number;
  public unit: string;
  public qty: number;
}

export class CustomerGroup {

  public customerGroupId: number;
  public companyId: number;
  public customerGroupName: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public customers: Customer[];
}

export class CustomerGroupMeta {

  public customerGroupName: number;
}

export class MyPurchase {

  public statusId: number;
  public statusName: string;
  public purchase: vwPurchaseRequisition[];
}

export class MyInventory {

  public statusId: number;
  public statusName: string;
  public inventory: vwInventory[];
}

export class MyVwActivityLite {

  public statusId: number;
  public statusName: string;
  public activities: vwActivityLite[];
}

export class FeedbackGroup {

  public key: number;
  public typeName: string;
  public priority: number;
  public vwFeedbacks: vwFeedback[];
}

export class FeedbackState {

  public createdByFullName: string;
  public feedbackAnswers: FeedbackAnswer[];
  public feedbackStateId: number;
  public feedbackId: number;
  public statusId: number;
  public stateId: number;
  public subject: string;
  public note: string;
  public assignedToUserId: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public feedback: Feedback;
}

export class InventoryGroup {

  public key: number;
  public vwInventorys: vwInventory[];
}

export class ExpensesGroup {

  public statusId: number;
  public statusName: string;
  public expenses: vwExpensesLite[];
}

export class vwActivityLiteGroup {

  public date: Date;
  public activityList: vwActivityLite[];
}

export class AgendaActivityGroup {

  public date: Date;
  public activity: vwActivity[];
}

export class vwActivity {

  public dateOfMonth: Date;
  public startOfDay: Date;
  public endOfDay: Date;
  public assignToUsers: vwUserInfo[];
  public assignToUser: vwUserInfo;
  public attachment: Attachment[];
  public customer: Customer;
  public activityId: number;
  public teamId: number;
  public subject: string;
  public groupId: number;
  public activityTypeId: number;
  public activityTypeName: string;
  public detailActivities: string;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public notes: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public isActive: boolean;
  public statusId: number;
  public statusName: string;
  public isSelectLocation: boolean;
  public assignToUserId: string;
  public assignToUserFullName: string;
  public orgString: string;
  public orgLevel: number;
  public companyId: number;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public startLatitude: number;
  public startLongitude: number;
  public startLocation: string;
  public startLocationName: string;
  public visitLatitude: number;
  public visitLongitude: number;
  public visitLocation: string;
  public visitLocationName: string;
  public returnLatitude: number;
  public returnLongitude: number;
  public returnLocation: string;
  public returnLocationName: string;
  public totalDistance: number;
  public dateAppointmented: Date;
  public appointmentedByUserId: string;
  public dateApproved: Date;
  public approvedByUserId: string;
  public dateRejected: Date;
  public rejectedByUserId: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public userRoleId: string;
  public userOrgString: string;
  public userOrgLevel: number;
  public customerId: number;
  public customerName: string;
  public isAdHoc: boolean;
}

export class AddPurchase {

  public subjectView: string;
  public vendorNameView: string;
  public vendorAddress: string;
  public remark: string;
  public item: ItemPurchase[];
}

export class ItemPurchase {

  public partId: number;
  public price: number;
  public qty: number;
}

export class ActivityCreateType {

}

export class vwCustomer {

  public uniqueName: string;
  public customerAlias: CustomerAlia[];
  public pictureUrl: string;
  public customerOwnersStr: string[];
  public customerOwners: vwUserInfo[];
  public addresses: Address[];
  public leadState: LeadState;
  public conversation: vwConversation;
  public modifiedByUser: string;
  public createByUser: string;
  public customerInterests: CustomerInterest[];
  public topicInterest: string[];
  public sourceName: string;
  public attachments: Attachment[];
  public recentContact: ActivityCentric;
  public customerClassification: CustomerClassification;
  public contact: Contact;
  public customerGroup: CustomerGroup;
  public customerGroupName: string;
  public customerId: number;
  public companyId: number;
  public customerGroupId: number;
  public customerCode: string;
  public customerName: string;
  public locationName: string;
  public customerNameEn: string;
  public customerType: number;
  public addressPart1: string;
  public isActive: boolean;
  public editable: boolean;
  public notes: string;
  public expr1: boolean;
  public addressPart2: string;
  public dateCheckin: Date;
  public location: string;
  public fax: string;
  public mobile: string;
  public phone: string;
  public dateCreated: Date;
  public dateModified: Date;
  public modifiedByUserId: string;
  public createdByUserId: string;
  public socialAccount: string;
  public companyName: string;
  public email: string;
  public sourceOfLead: number;
  public type: number;
  public taxId: string;
  public customFields: CustomerDynamicData[];
}

export class Customer_List_Result {

  public uniqueName: string;
  public customerAliasList: CustomerAlia[];
  public pictureUrl: string;
  public customerId: number;
  public companyId: number;
  public customerType: number;
  public customerName: string;
  public customerAlias: string;
  public customerGroupName: string;
  public notes: string;
  public recentActivity: Date;
  public staffName: string;
  public dateCreateActivity: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateCreateFeedback: Date;
  public dateCreateExpense: Date;
  public dateCreateCustomer: Date;
  public dateCreateContact: Date;
  public dateUpdateCustoner: Date;
  public dateUpdateContact: Date;
  public dateModifiedCustomer: Date;
  public typeCustomerGroup: number;
}

export class Customer_TimeLine_Result {

  public staffs: vwUserInfo[];
  public pictureUrl: string;
  public no: number;
  public id: number;
  public subject: string;
  public activityTypeId: number;
  public activityTypeName: string;
  public statusId: number;
  public statusName: string;
  public dateTimeLine: Date;
  public userId: string;
  public username: string;
  public fullname: string;
  public notes: string;
  public customerId: number;
  public customerName: string;
  public cancelReason: string;
  public contactId: number;
  public contactName: string;
  public type: number;
  public typeName: string;
  public typeConversation: number;
  public typeNameConversation: string;
  public subType: number;
  public subTypeName: string;
  public outcomeName: string;
  public quantified: number;
  public quantifiedName: string;
}

export class CustomerTimeLine {

  public date: string;
  public customers: Customer_TimeLine_Result[];
}

export class vwConversation {

  public pictureUrl: string;
  public user: vwUserInfo;
  public canCreatePlan: boolean;
  public conversationId: number;
  public customerId: number;
  public type: number;
  public typeName: string;
  public subType: number;
  public subTypeName: string;
  public outcome: number;
  public outcomeName: string;
  public quantified: number;
  public quantifiedName: string;
  public action: number;
  public actionName: string;
  public dateFollowUp: Date;
  public description: string;
  public dateConversation: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public userOrgString: string;
  public userOrgLevel: number;
  public userDateCreated: Date;
  public userDateModified: Date;
  public firstname: string;
  public lastname: string;
  public fullname: string;
  public customerName: string;
  public companyId: number;
  public isSelect: boolean;
}

export class Employee_Customer_Activity_Result {

  public pictureUrl: string;
  public customerId: number;
  public customerName: string;
  public addressPart1: string;
  public addressPart2: string;
  public isActive: boolean;
  public dateModified: Date;
  public dateCreated: Date;
  public dateCheckin: Date;
  public totalPlan: number;
  public totalActivity: number;
  public totalContact: number;
  public totalFeedback: number;
}

export class ExpenseState {

  public createByUser: vwUserInfo;
  public expenseStatesId: number;
  public expenseId: number;
  public statusId: number;
  public reason: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public expense: Expense;
}

export class InventoryState {

  public createByUser: vwUserInfo;
  public inventoryStateId: number;
  public inventoryId: number;
  public statusId: number;
  public remark: string;
  public createByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public inventory: Inventory;
}

export class ActivityState {

  public createByUser: vwUserInfo;
  public activityStateId: number;
  public activityId: number;
  public statusId: number;
  public remark: string;
  public userAgent: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public activity: Activity;
  public status: Status;
}

export class Dropdown {

  public id: number;
  public name: string;
  public nameEn: string;
  public dataId: number;
}

export class HomeActivityGroup {

  public date: Date;
  public activity: Todo[];
}

export class ConversationGroup {

  public date: Date;
  public vwConversations: vwConversation[];
}


export class WorkInGroup {

  public month: Date;
  public activities: Activity[];
}

export class Deal {

  public dealNo: string;
  public attachments: Attachment[];
  public userId: string[];
  public participantUserIds: string[];
  public isParticipant: boolean;
  public isAction: boolean;
  public isEdit: boolean;
  public isDelete: boolean;
  public statusName: string;
  public user: vwUserInfo;
  public userAssigns: vwUserInfo[];
  public userParticipants: DealsAssign[];
  public customerIds: number[];
  public dealStageId: number;
  public dealsId: number;
  public customerId: number;
  public dealsName: string;
  public dealsValue: number;
  public statusId: number;
  public dealsDate: Date | string;
  public dateFollowUp: Date | string;
  public expectedCloseDate: Date | string;
  public closeDate: Date | string;
  public probability: number;
  public notes: string;
  public followUpNotes: string;
  public isActive: boolean;
  public dateCreated: Date;
  public createByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public dealsAssigns: DealsAssign[];
  public dealsInterests: DealsInterest[];
  public dealsStates: DealsState[];
  public feedbackDeals: FeedbackDeal[];
  public customer: Customer;
  public quotation: Order;
  public salesOrder: Order;
  public orderId: number;
  public saleOrderId: number;
  public dealQuantity: number;
  public measure: number;
  public boardId: number;
  public saleOrderDeal: any;
  public orderDeal: any;
  public measureUnit: any;
  public calTargetDate: any;
  public userOwners: vwUserInfo[];
  public topicInterests: TopicInterest[];
}

export class DealsAssign {

  public user: vwUserInfo;
  public userId: string;
  public dealsId: number;
  public companyId: number;
  public dateCreated: Date;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public deal: Deal;
}

export class DealsState {

  public user: vwUserInfo;
  public statusName: string;
  public dealCategoryId: number;
  public dealsStateId: number;
  public dealsId: number;
  public statusId: number;
  public dealsValue: number;
  public dealsDate: Date;
  public calTargetDate: Date;
  public expectedCloseDate: Date;
  public closeDate: Date;
  public probability: number;
  public notes: string;
  public reason: string;
  public dateCreated: Date;
  public createByUserId: string;
  public deal: Deal;
}


export class DealStage {
  public dealStageId: number;
  public stageName: string;
  public notes: string;
  public dateCreated: Date;
  public createByuserId: number;
  public dateModified: Date;
  public modifiedByUserId: string;
  public isActive: boolean;
  public companyId: number;
  public seq: number;
  public countDeal: number;
  public boardId: number;
}

export class DealCategory {
  public dealCategoryId: number;
  public dealCategoryName: string;
  public dealCategoryType: number;
  public dateCreated: Date;
  public isActive: boolean;
  public companyId: number;
}


export class DealsGroup {
  public statusId: number;
  public statusName: string;
  public isStage: boolean;
  public deals: Deal[];
  public vwDealAssign: vwDealAssign[];
}

export class Conversation {

  public dateAlert: Date;
  public conversationId: number;
  public customerId: number;
  public type: number;
  public subType: number;
  public outcome: number;
  public quantified: number;
  public dealId: number;
  public action: number;
  public description: string;
  public dateConversation: Date;
  public dateFollowUp: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public customer: Customer;
  public callMinutes: number;
}

export class Memo {

  public attachments: Attachment[];
  public isEdit: boolean;
  public isCancel: boolean;
  public isApprove: boolean;
  public createdByUser: vwUserInfo;
  public statusName: string;
  public memoId: number;
  public memoCode: string;
  public subject: string;
  public dateDue: Date;
  public statusId: number;
  public description: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public memoStates: MemoState[];
}

export class MemoGroup {

  public statusId: number;
  public statusName: string;
  public memo: vwMemo[];
}

export class vwMemo {

  public attachment: Attachment[];
  public pictureUrl: string;
  public memoId: number;
  public memoCode: string;
  public subject: string;
  public dateDue: Date;
  public statusId: number;
  public description: string;
  public dateCreated: Date;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public staffCode: string;
  public fullname: string;
  public statusName: string;
  public lastStatusId: number;
  public hasAttachment: boolean;
  public companyId: number;
  public orgString: string;
}

export class GetDateVisitAllContactByCustomerId_Result {

  public pictureUrl: string;
  public userInfo: vwUserInfo;
  public contactId: number;
  public contactName: string;
  public activityId: number;
  public dateCheckin: Date;
  public position: string;
  public phone: string;
  public mobile: string;
  public email: string;
  public assignToUserId: string;
  public firstname: string;
}

export class MemoState {

  public createdByUser: vwUserInfo;
  public memoStateId: number;
  public memoId: number;
  public statusId: number;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public memo: Memo;
}

export class Report_OpportunityByUserCompany_Result {

  public userInfo: vwUserInfo;
  public userId: string;
  public target: number;
  public won: number;
  public potential: number;
  public miss: number;
  public totalDeal: number;
  public wonDeal: number;
  public missDeal: number;
  public avgCloseDate: number;
  public achieve: number;
  public wonRate: number;
  public growthRate: number;
  public totalNew: number;
  public totalInProgress: number;
  public totalWon: number;
  public probability: number;
}

export class vwDealAssign {

  public dealsInterests: DealsInterest[];
  public user: vwUserInfo;
  public customer: Customer;
  public userId: string;
  public companyId: number;
  public companyName: string;
  public orgString: string;
  public headUserId: string;
  public head1: string;
  public dealsId: number;
  public dealsName: string;
  public dealsValue: number;
  public statusId: number;
  public dealsDate: Date;
  public expectedCloseDate: Date;
  public closeDate: Date;
  public notes: string;
  public pictureUrl: string;
  public customerId: number;
  public type: number;
  public customerName: string;
  public dateCreated: Date;
  public createByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public isActive: boolean;
}

export class Employee_List_Result {

  public pictureUrl: string;
  public userId: string;
  public fullname: string;
  public staffCode: string;
  public role: string;
  public teamName: string;
  public recentlyActivity: Date;
  public dateCreateActivity: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateCreateFeedback: Date;
  public dateCreated: Date;
}

export class SendNoti {

  public userId: string;
  public notiTitle: string;
  public notiBody: string;
  public data: object;
}

export class ExpensesGroupByDate {

  public date: Date;
  public expensesLites: vwExpensesLite[];
}

export class FeedbackGroupByDate {

  public date: Date;
  public vwFeedbacks: vwFeedback[];
}

export class WorkInGroupByMonth {

  public month: Date;
  public workIn: vwActivity[];
}

export class Employee_TimeLine_Result {

  public staffs: vwUserInfo[];
  public no: number;
  public id: number;
  public activityId: number;
  public type: number;
  public typeName: string;
  public subject: string;
  public customerName: string;
  public status: number;
  public statusName: string;
  public note: string;
  public dateStart: Date;
  public dateEnd: Date;
  public dateAction: Date;
  public staffName: string;
  public totalPrice: number;
}

export class Target {

  public targetPeriod: Date;
  public assignToUserId: string[];
  public userInfo: vwUserInfo;
  public customerId: number;
  public customerWallet: CustomerWallet;
  public customer: vwCustomer;
  public measureProduct: MeasureProduct[];
  public targetId: number;
  public companyId: number;
  public name: string;
  public measure: number;
  public productTypeId: number;
  public interval: number;
  public targetFor: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public targetItems: TargetItem[];
}

export class TargetSummary {

  public dateStart: Date;
  public dateEnd: Date;
  public won: number;
  public potential: number;
  public target: number;
  public walletSize: number;
  public targetId: number;
  public isAdd: boolean;
  public isEdit: boolean;
  public removed: boolean;
  public customerWallet: CustomerWallet;
  public dealsGroup: DealsGroup[];
  public targetGroups: TargetGroup[];
  public totalDeal: number;
  public totalOrder: number;
}

export class EditDeleteSaleTarget {

  public targetId: number;
  public userId: string;
}

export class EditDeleteCustomerTarget {

  public targetId: number;
  public customerId: number;
  public measureId: number;
}

export class MeasureProduct {

  public measureId: number;
  public measureName: string;
  public hasTarget: boolean;
}

export class DealTargetGroup {

  public date: Date;
  public actual: number;
  public target: number;
  public deals: Deal[];
}

export class TargetGroup {

  public date: Date;
  public dealsTargetGroups: DealTargetGroup[];
}

export class TargetGroupByDate {

  public date: Date;
  public saleOrders: SaleOrder[];
  public deals: Deal[];
}

export class SaleOrder {

  public attachments: Attachment[];
  public customer: Customer;
  public parts: Part[];
  public userInfo: vwUserInfo;
  public statusName: string;
  public isEdit: boolean;
  public isApprove: boolean;
  public reason: string;
  public total: number;
  public customerIds: number[];
  public saleOrderId: number;
  public saleOrderNo: string;
  public status: number;
  public dateOrder: Date;
  public customerId: number;
  public orderType: number;
  public siteId: number;
  public isActive: boolean;
  public isExcludeVat: boolean;
  public remark: string;
  public paymentTermDescription: string;
  public note: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public dateModified: Date;
  public dateApproved: Date;
  public activityId: number;
  public discount: number;
  public discountNote: string;
  public grandTotal: number;
  public saleOrderDetails: SaleOrderDetail[];
  public saleOrderStates: SaleOrderState[];
  public site: Site;
  public user: vwUserInfo;
}

export class SaleOrderState {

  public createdByUser: vwUserInfo;
  public saleOrderStatesId: number;
  public saleOrderId: number;
  public statusId: number;
  public reason: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public saleOrder: SaleOrder;
}

export class SaleOrderGroup {

  public date: Date;
  public saleOrderType: number;
  public saleOrderGroupDetails: SaleOrderGroupDetail[];
}

export class SaleOrderGroupDetail {

  public saleOrderId: number;
  public status: number;
  public saleOrderDetails: SaleOrderDetail[];
  public total: number;
}

export class SaleOrderDetail {

  public name: string;
  public unit: string;
  public totalQty: number;
  public saleOrderDetailId: number;
  public saleOrderId: number;
  public partId: number;
  public qty: number;
  public unitId: number;
  public price: number;
  public discount: number;
  public totalPrice: number;
  public part: Part;
  public saleOrder: SaleOrder;
}

export class Customer_ListFilter_Result {

  public uniqueName: string;
  public pictureUrl: string;
  public customerId: number;
  public companyId: number;
  public customerType: number;
  public customerName: string;
  public customerAlias: string;
  public customerGroupName: string;
  public notes: string;
  public recentActivity: Date;
  public staffName: string;
  public dateCreateActivity: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateCreateFeedback: Date;
  public dateCreateExpense: Date;
  public dateCreateCustomer: Date;
  public dateCreateContact: Date;
  public dateUpdateCustoner: Date;
  public dateUpdateContact: Date;
  public dateModifiedCustomer: Date;
}

export class StoragesModel {
  public storageLimit: number;
  public available: number;
  public usages: StoragesData[];
}

export class StoragesData {
  public type: number;
  public size: number;
  public color: string;
}


export class SaleOrderGroupByType {

  public id: number;
  public name: string;
  public items: vwSaleOrder[];
}

export class vwCustomerOwner {

  public uniqueName: string;
  public pictureUrl: string;
  public userId: string;
  public customerId: number;
  public companyId: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public customerGroupName: string;
  public customerGroupId: number;
  public customerCode: string;
  public customerName: string;
  public customerNameEn: string;
  public customerType: number;
  public customerDateCreated: Date;
  public customerDateModified: Date;
  public customerIsActive: boolean;
}

export class CustomerOwnerUpdate {

  public userId: string;
  public customerIds: number[];
}

export class CustomerCentricFilter {

  public filterId: number;
  public filterName: string;
  public filterValue: string;
  public filterKeyValue: KeyValue[];
}

export class CustomerCentricByGroup {

  public search: string;
  public groupId: number;
  public skip: number;
  public take: number;
  public latitude: number;
  public longitude: number;
  public filters: CustomerCentricFilter[];
}

export class GroupCount {

  public name: string;
  public id: number;
  public count: number;
  public customers: Customer[];
  public accessAllData: boolean;
}

export class CustomerCountAndFilter {

  public group: GroupCount[];
  public filters: CustomerCentricFilter[];
}

export class ImportFile {

  public importRolePermission: ImportRolePermission[];
  public importEmployees: ImportEmployee[];
  public importFileCustomer: ImportFileCustomer[];
  public importCustomersContact: ImportCustomersContact[];
  public importCustomerLocation: ImportCustomerLocation[];
  public importActivity: ImportActivity[];
  public importCase: ImportCase[];
  public importItemCategory: ImportItemCategory[];
  public importItem: ImportItem[];
  public importProductCategory: ImportProductCategory[];
  public importProduct: ImportProduct[];
}

export class ImportFileEmployee {

  public importRolePermission: ImportRolePermission[];
  public importEmployees: ImportEmployee[];
}

export class ImportFileCustomers {

  public success: number;
  public fail: number;
  public importFileCustomer: ImportFileCustomer[];
  public importCustomersContact: ImportCustomersContact[];
  public importCustomerLocation: ImportCustomerLocation[];
}

export class ImportFileMaster {

  public importActivity: ImportActivity[];
  public importCase: ImportCase[];
}

export class ImportFileInventory {

  public importItemCategory: ImportItemCategory[];
  public importItem: ImportItem[];
}

export class ImportFileProduct {

  public importProductCategory: ImportProductCategory[];
  public importProduct: ImportProduct[];
}

export class SaleOrderTimeline {

  public dateOrder: Date;
  public target: number;
  public actual: number;
  public percent: number;
  public saleOrders: vwSaleOrder[];
}

export class vwSaleOrder {

  public saleOrderDetails: vwSaleOrderDetail[];
  public saleOrderId: number;
  public saleOrderNo: string;
  public status: number;
  public statusName: string;
  public dateOrder: Date;
  public customerId: number;
  public customerName: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public totalPrice: number;
  public fullname: string;
  public orgString: string;
  public isActive: boolean;
  public remark: string;
  public companyId: number;
  public orderType: number;
}

export class DealsTimeline {

  public dateCreated: Date;
  public status: number;
  public deals: Deals_Timeline_Result[];
}

export class InventoriesTimeline {

  public dateCreated: Date;
  public inventories: Inventories_Timeline_Result[];
}

export class CustomerVisit {

  public avgMonth: number;
  public avgMonthRatio: number[];
  public customerVisit: number[][];
}

export class CustomerEngagement {

  public avgEngagement: number;
  public increasePercent: number;
  public graph: CustomerEngagement_Summary_Result[];
  public table: Customer_Engagement_Result[];
}

export class Customer_Engagement_Result {

  public user: vwUserInfo;
  public employee: string;
  public totalEngagement: number;
  public engagementRatio: number;
  public lastEngagement: Date;
  public averageTime: number;
}

export class SummaryGroups {

  public dateGroups: Date;
  public target: number;
  public actual: number;
}

export class SaleOrderReport {

  public staff: string[];
  public productType: string[];
  public teamId: string[];
  public search: string;
  public dateStart: Date;
  public dateEnd: Date;
  public interval: number;
  public order: number;
  public skip: number;
  public take: number;
  public orderColumns: Array<Object>;
}

export class ImportHistory {

  public fileName: string;
  public byteArray: string;
  public dateCreated: Date;
  public statusId: number;
  public fileTypeId: number;
  public total: number;
  public read: number;
  public load: number;
  public pass: number;
  public fail: number;
  public percentage: number;
  public createdByUser: vwUserInfo;
  public createdByUserId: string;
  public url?: string;
}

export class ImportResultSummary {

  public success: number;
  public fail: number;
  public failDetails: ImportFailSummary[];
}

export class ImportFailSummary {

  public sheet: number;
  public row: number;
  public name: string;
  public reason: string;
}

export class DealFilter {

  public customerId: number[];
  public stage: number[];
  public label: number;
  public labelIds: number[];
  public orderBy: string[];
  public statusId: number;
  public userId: string;
  public isTodo: boolean;
  public topic: number;
  public interest: number[];
  public user: string[];
  public dateStart: Date;
  public dateEnd: Date;
  public probability: number[];
  public probabilityText: string[];
  public search: string;
  public skip: number;
  public take: number;
  public viewType: number;
  public boardId: number;
  public orderColumns: Array<Object>;
}


export class UsageInformation {

  public activeUsers: number;
  public customers: number;
  public activities: number;
  public deals: number;
  public cases: number;
  public package: string;
  public totalUsers: number;
}

export class ActivityLocation {

  public activityId: number;
  public startLatitude: number;
  public startLongitude: number;
  public startLocation: string;
  public startLocationName: string;
  public startPlaceId: string;
  public visitLatitude: number;
  public visitLongitude: number;
  public visitLocation: string;
  public visitLocationName: string;
  public visitPlaceId: string;
}

export class ActivityLog {

  public activityLocation: ActivityLocation;
  public userInfo: vwUserInfo;
  public attachments: Attachment[];
  public activityLogId: number;
  public refId: number;
  public refActivityLogId: number;
  public comment: string;
  public jsonLog: string;
  public logType: number;
  public status: number;
  public dateModified: Date;
  public dateCreated: Date;
  public createdByUserId: string;
  public modifiedByUserId: string;
  public activity: vwActivity;
  public deal: Deal;
}

export class Address {

  public addressType: number;
  public addressId: number;
  public customerId: number;
  public placeId: string;
  public branch: string;
  public addressPart1: string;
  public addressPart2: string;
  public province: string;
  public zipCode: string;
  public phone: string;
  public mobile: string;
  public fax: string;
  public notes: string;
  public isActive: boolean;
  public isLocal: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public customer: Customer;
  public customerAddressTypes: any[];
}

export class CaseGroup {

  public statusId: number;
  public statusName: string;
  public case: vwCase[];
}

export class CaseFilter {

  public userId: string;
  public search: string;
  public status: number;
  public categoryIds: number[];
  public skip: number;
  public pageLength: number;
}

export class vwCase {

  public userInfo: vwUserInfo;
  public feedbackId: number;
  public companyId: number;
  public subject: string;
  public statusId: number;
  public note: string;
  public assignedToUserId: string;
  public assignedToName: string;
  public assignedToFullName: string;
  public statusName: string;
  public feedbackCategories: string;
  public feedbackCategoryIds: string;
  public dateAssigned: Date;
  public dateCase: Date;
  public dateDue: Date;
  public feedbackAnswer: string;
  public createdByUserId: string;
  public createdByName: string;
  public createdByFullName: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public customerId: number;
  public customerName: string;
  public lastFeedbackAnswer: string;
  public lastFeedbackHasAttachment: boolean;
  public lastFeedbackById: string;
  public lastFeedbackByName: string;
  public dateLastAnswer: Date;
  public userOrgString: string;
}

export class PermissionData {

  public permissionId: number;
  public isActive: boolean;
}

export class RolePermissionData {

  public roleId: string;
  public permissionData: PermissionData[];
}

export class CompanyPreferenceGroup {

  public groupType: number;
  public preferenceSettings: PreferenceSetting[];
}

export class Preference {

  public preferenceId: number;
  public value: string;
  public typeValue: string;
}

export class PreferenceSetting {

  public value: string;
  public subPreferences: PreferenceSetting[];
  public preferenceId: number;
  public parentId: number;
  public settingType: number;
  public groupId: number;
  public options: any[];
  public isFalsePositive: boolean;
}

export class Statuses {

  public statusGroup: object;
}

export class ActivityContact {

  public activityId: number;
  public contactId: number;
  public isActive: boolean;
  public activity: Activity;
  public contact: Contact;
}

export class ActivityGroup {

  public groupId: number;
  public dateCreated: Date;
  public createdByUserId: string;
}

export class ActivityParticipant {

  public activityParticipantId: number;
  public activityId: number;
  public groupId: number;
  public userId: string;
  public status: number;
  public remark: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public dateModified: Date;
  public modifiedByUserId: string;
  public activity: Activity;
}

export class ActivityRoute {

  public activityRouteId: number;
  public activityId: number;
  public northeastLatitude: number;
  public northeastLongitude: number;
  public southLatitude: number;
  public southLongitude: number;
  public polylinePoint: string;
  public activity: Activity;
}

export class AdditionalInformation {

  public additionalInformationId: number;
  public type: number;
  public refType: number;
  public refId: number;
  public description: string;
  public createdByUserId: string;
  public dateCreated: Date;
}

export class ApprovalFlow {

  public approvalFlowId: number;
  public companyId: number;
  public moduleId: number;
  public seq: number;
  public approvalLevel: number;
  public approvalRole: string;
  public approvalMinRoleLevel: number;
  public approvalMaxRoleLevel: number;
  public isActive: boolean;
}

export class AuthorizationPolicy {

  public policyId: number;
  public policyName: string;
  public isActive: boolean;
  public authorizationPolicyRequirements: AuthorizationPolicyRequirement[];
}

export class AuthorizationPolicyRequirement {

  public policyId: number;
  public requirementType: string;
  public refId: string;
  public isOptional: boolean;
  public authorizationPolicy: AuthorizationPolicy;
}

export class Company {

  public companyId: number;
  public parentCompanyId: number;
  public companyName: string;
  public addressPart1: string;
  public addressPart2: string;
  public province: string;
  public zipCode: string;
  public phone: string;
  public mobile: string;
  public fax: string;
  public notes: string;
  public isActive: boolean;
  public codeRegister: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public package: string;
  public volumnLicense: number;
  public companyType: number;
  public taxId: number;
  public companyAddres: CompanyAddres[];
  public customers: Customer[];
  public userCompanies: UserCompany[];
  public userInfos: UserInfo[];
}

export class CompanyAddres {

  public companyAddressId: number;
  public companyId: number;
  public placeId: string;
  public addressPart1: string;
  public addressPart2: string;
  public province: string;
  public zipCode: string;
  public phone: string;
  public mobile: string;
  public fax: string;
  public notes: string;
  public latitude: number;
  public longitude: number;
  public radius: number;
  public isActive: boolean;
  public company: Company;
}

export class CompanyPreference {

  public companyId: number;
  public preferenceId: number;
  public value: string;
  public dateModified: Date;
}

export class CreditTerm {

  public creditTermId: number;
  public companyId: number;
  public name: string;
  public description: string;
  public value: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
}

export class CustomerAlia {

  public customerAliasId: number;
  public customerId: number;
  public aliasName: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public customer: Customer;
}

export class CustomerClassification {

  public customerClassificationId: number;
  public customerClassificationName: string;
  public companyId: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
}

export class CustomerInterest {

  public customerId: number;
  public topicId: number;
  public isActive: boolean;
  public customer: Customer;
  public topicInterest: TopicInterest;
}

export class CustomerTarget {

  public customerTargetId: number;
  public customerId: number;
  public targetItemId: number;
  public amount: number;
  public isActive: boolean;
  public dateStart: Date;
  public dateEnd: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public customer: Customer;
  public targetItem: TargetItem;
}

export class CustomerWallet {

  public customerWalletId: number;
  public customerId: number;
  public measure: number;
  public productTypeId: number;
  public amount: number;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public customer: Customer;
}

export class Comment {
  public fullname: string;
  public dateComment: Date;
  public commentNote: string;
}
export class DealsInterest {

  public dealsId: number;
  public topicId: number;
  public isActive: boolean;
  public deal: Deal;
  public topicInterest: TopicInterest;
  public topic?: TopicInterest;
}

export class Department {

  public companyId: number;
  public divisionId: string;
  public departmentId: number;
  public departmentName: string;
  public departmentCodeName: string;
  public isActive: boolean;
}

export class Division {

  public companyId: number;
  public divisionId: string;
  public divisionName: string;
  public isActive: boolean;
}

export class ExpenseType {

  public expenseTypeId: number;
  public name: string;
  public isActive: boolean;
  public createdByUsername: string;
  public dateCreated: Date;
  public updatedByUsername: string;
  public dateUpdated: Date;
  public expenseDetails: ExpenseDetail[];
}

export class Feature {

  public featureId: number;
  public featureName: string;
  public description: string;
  public isActive: boolean;
  public parentFeatureId: number;
  public featureSettingTypeId: number;
  public featureBasePermissionId: number;
  public displayOrderNo: number;
  public featureOptions: FeatureOption[];
}

export class FeatureOption {

  public featureId: number;
  public optionId: number;
  public optionName: string;
  public isActive: boolean;
  public displayOrderNo: number;
  public featureOptionPermissions: FeatureOptionPermission[];
  public feature: Feature;
}

export class FeatureOptionPermission {

  public featureId: number;
  public optionId: number;
  public permissionId: number;
  public isEnable: boolean;
  public featureOption: FeatureOption;
  public permission: Permission;
}

export class FeedbackActivity {

  public feedbackId: number;
  public activityId: number;
  public relatedType: number;
  public createByUserId: string;
  public dateCreated: Date;
  public feedback: Feedback;
}

export class FeedbackCategory {

  public feedbackCategoryId: number;
  public feedbackCategoryName: string;
  public teamId: number;
  public isActive: boolean;
  public companyId: number;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public feedbackCategoryDetails: FeedbackCategoryDetail[];
}

export class FeedbackDeal {

  public feedbackId: number;
  public dealId: number;
  public dateCreated: Date;
  public createByUserId: string;
  public deal: Deal;
  public feedback: Feedback;
}

export class FeedbackStateLog {

  public feedbackStateLog: number;
  public feedbackId: number;
}

export class FeedbackType {

  public feedbackTypeId: number;
  public feedbackTypeName: string;
  public companyId: number;
  public teamId: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public feedbacks: Feedback[];
}

export class ImportActivity {

  public recordId: number;
  public activityId: number;
  public keyActivity: string;
  public activityDetails: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public companyId: number;
  public roundId: number;
}

export class ImportCase {

  public recordId: number;
  public caseId: number;
  public caseCategory: string;
  public caseExplain: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public companyId: number;
  public roundId: number;
}

export class ImportContact {

  public recordId: number;
  public importContactId: number;
  public customerCode: string;
  public contactName: string;
  public position: string;
  public email: string;
  public phone: string;
  public mobile: string;
  public notes: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public customerName: string;
}

export class ImportCustomer {

  public recordId: number;
  public importCustomerId: number;
  public companyId: number;
  public customerCode: string;
  public customerName: string;
  public addressPart1: string;
  public addressPart2: string;
  public province: string;
  public zipCode: string;
  public phone: string;
  public mobile: string;
  public fax: string;
  public notes: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public type: string;
  public address: string;
  public branch: string;
  public email: string;
  public socialAccount: string;
  public customerAlias: string;
  public customerGroup: string;
  public customerInterest: string;
  public source: string;
  public staffOwner: string;
}

export class ImportCustomerLocation {

  public recordId: number;
  public customerLocationId: number;
  public customerCode: string;
  public customerName: string;
  public branch: string;
  public address: string;
  public province: string;
  public zipCode: string;
  public mobile: string;
  public phone: string;
  public email: string;
  public locationName: string;
  public latitude: string;
  public longitude: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public companyId: number;
  public roundId: number;
}

export class ImportCustomersContact {

  public recordId: number;
  public customersContactId: number;
  public customerCode: string;
  public customerName: string;
  public contactName: string;
  public nickname: string;
  public position: string;
  public email: string;
  public mobile: string;
  public telephone: string;
  public dateCreated: Date;
  public filename: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public companyId: number;
  public roundId: number;
}

export class ImportEmployee {

  public employeeId: number;
  public recordId: number;
  public employeeCode: string;
  public firstname: string;
  public lastname: string;
  public nickname: string;
  public email: string;
  public tel: string;
  public roleName: string;
  public team: string;
  public headCode: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public filename: string;
  public companyId: number;
  public roundId: number;
}

export class ImportFileCustomer {

  public customersId: number;
  public recordId: number;
  public customerCode: string;
  public customerName: string;
  public type: string;
  public address: string;
  public branch: string;
  public province: string;
  public zipcode: string;
  public mobile: string;
  public telephone: string;
  public email: string;
  public socialAccount: string;
  public fax: string;
  public customerAlias: string;
  public customerGroup: string;
  public customerInterest: string;
  public source: string;
  public staffOwner: string;
  public note: string;
  public filename: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public companyId: number;
  public roundId: number;
}

export class ImportItem {

  public recordId: number;
  public itemId: number;
  public itemCode: string;
  public itemname: string;
  public itemCategory: string;
  public price: string;
  public qty: string;
  public unit: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public fileName: string;
  public companyId: number;
  public roundId: number;
}

export class ImportItemCategory {

  public recordId: number;
  public itemCategoryId: number;
  public itemCategory: string;
  public fileName: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public companyId: number;
  public roundId: number;
}

export class ImportProduct {

  public productId: number;
  public recordId: number;
  public productCode: string;
  public productName: string;
  public productCategory: string;
  public price: string;
  public qty: string;
  public unit: string;
  public fileName: string;
  public companyId: number;
  public dateCreated: Date;
  public createdByUserId: string;
  public isSuccess: boolean;
  public remark: string;
  public roundId: number;
}

export class ImportProductCategory {

  public recordId: number;
  public productCategoryId: number;
  public productCategory: string;
  public fileName: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public companyId: number;
  public roundId: number;
}

export class ImportRolePermission {

  public recordId: number;
  public rolePermissionId: number;
  public roleName: string;
  public rolePermission: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public filename: string;
  public companyId: number;
  public roundId: number;
}

export class ImportStaff {

  public recordId: number;
  public staffCode: string;
  public firstname: string;
  public lastname: string;
  public email: string;
  public tel: string;
  public role: string;
  public team: string;
  public headCode: string;
  public isSuccess: boolean;
  public remark: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public filename: string;
}

export class Inventory {

  public inventoryId: number;
  public invNo: string;
  public status: number;
  public isActive: boolean;
  public subject: string;
  public companyId: number;
  public customerId: number;
  public remark: string;
  public createdByUserId: string;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public dateCreated: Date;
  public dateModified: Date;
  public dateApproved: Date;
  public inventoryItems: InventoryItem[];
  public inventoryStates: InventoryState[];
  public customer: Customer;
  public userInfo: UserInfo;
}

export class InventoryItem {

  public inventoryItemId: number;
  public inventoryId: number;
  public partId: number;
  public qty: number;
  public price: number;
  public totalPrice: number;
  public inventory: Inventory;
  public part: Part;
}

export class LeadState {

  public leadStateId: number;
  public customerId: number;
  public statusId: number;
  public createByUserId: string;
  public dateCreated: Date;
  public customer: Customer;
}

export class LocaleResourceKey {

  public id: string;
  public name: string;
  public notes: string;
  public dateAdded: Date;
  public localeStringResources: LocaleStringResource[];
}

export class MasterType {

  public typeId: number;
  public companyId: number;
  public typeParent: number;
  public typeName: string;
  public typeDesc: string;
  public branchId: number;
  public userChannelId: number;
  public status: boolean;
  public typeValuePreference: string;
  public customers: Customer[];
}

export class NotificationLog {

  public notificationLogId: number;
  public userId: string;
  public title: string;
  public body: string;
  public jsonLog: string;
  public dateCreated: Date;
}

export class NumberGroup {

  public numberGroupId: number;
  public numberTypeId: number;
  public isLoop: boolean;
  public description: string;
  public numberType: NumberType;
}

export class NumberType {

  public numberTypeId: number;
  public numberTypeDescription: string;
  public numberGroups: NumberGroup[];
}

export class Order {

  public orderId: number;
  public saleOrderId: number;
  public orderType: number;
  public refOrderId: number;
  public orderNo: string;
  public subject: string;
  public statusId: number;
  public status: number;
  public creditTerm: number;
  public paymentTermDescription: string;
  public customerId: number;
  public customerName: string;
  public contactId: number;
  public contactName: string;
  public discount: number;
  public discountNote: string;
  public grandTotal: number;
  public totalVat: number;
  public totalWht: number;
  public netToPay: number;
  public note: string;
  public remark: string;
  public version: number;
  public dealId: number;
  public dateOrder: Date;
  public dateExpire: Date;
  public dateShipment: Date;
  public userOrgString: string;
  public userOrgLevel: number;
  public dateSend: Date;
  public isSend: boolean;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public waitingApproveByUserId: string;
  public waitingApproveByRoleId: string;
  public approvedByUserId: string;
  public modifiedByUserId: string;
  public currencyCode: string;
  public thaiBahtText: string;
  public dateModified: Date;
  public dateApproved: Date;
  public subTotal: number;
  public isShowContact: boolean;
  public isCreateDeal: boolean;
  public isExcludeVat: boolean;
  public canEdit: boolean;
  public canDelete: boolean;
  public canApproved: boolean;
  public canSend: boolean;
  public canRevise: boolean;
  public isEdit: boolean;
  public isApprove: boolean;
  public isDelete: boolean;
  public isDiscountPercentage: boolean;
  public discountPercentage: number;
  public orderHeaderTemplateId: number;
  public saleOrderHeaderTemplateId: number;
  public total: number;
  public shipToId: number;
  public shipToAddress: string;
  public unit: string;
  public template;
  public customer: Customer;
  public user: vwUserInfo;
  public deal: Deal;
  public attachments: Attachment[];
  public orderDetails: OrderDetail[];
  public saleOrderDetails: OrderDetail[];
  public orderStates: OrderState[];
  public creditTermData: CreditTerm;
  public saleOrderStates: SaleOrderState[];
  public config: any;

}

export class OrderDetail {
  orderDetailId: number;
  saleOrderDetailId: number;
  orderId: number;
  saleOrderId: number;
  itemId: number;
  itemName: string;
  partId: number;
  qty: number;
  unitId: number;
  price: number;
  vwDiscount: string;
  vwPrice: string;
  discount: number;
  isDiscountPercentage: boolean;
  discountPercentage: number;
  totalPrice: number;
  note: string;
  notes: string;
  dateCreated: string;
  item: Part;
  order: Order;
  partCode: string;
  partName: string;
  name: string;
  subTotal: number;
  unitName: string;
  unit: string;
  vatTaxId: number;
  vatTaxValue: number;
  whtTaxId: number;
  whtTaxValue: number;
  totalVat: number;
  totalWht: number;
  copy: number;
  partParent: number;
  checkAnother: boolean;
  indexItem: number;
  editDetail: boolean;
  productValues: productProperty[];
  propertyItem: productProperty[];
  propertySelected: productProperty[];
  isManual: boolean;
}

export class OrderState {

  public orderStateId: number;
  public orderId: number;
  public statusId: number;
  public reason: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public order: Order;
}

export class PaymentInvoice {
  public dateDue: Date;
  public dateNextBilling: Date;
  public dateStartOn: Date;
  public invoiceId: number;
  public statusId: number;
  public billEvery: number;
  public billingInterval: number;
  public statusName: string;
  public subscriptionName: string;
  public totalAmount: number;
}

export class Permission {

  public permissionId: number;
  public permissionName: string;
  public permissionDesc: string;
  public parent: number;
  public featureOptionPermissions: FeatureOptionPermission[];
  public rolePermissions: RolePermission[];
}

export class Position {

  public companyId: number;
  public positionCode: string;
  public positionName: string;
  public positionNameEn: string;
  public isActive: boolean;
}

export class PurchaseItem {

  public purchaseItemId: number;
  public purchaseRequisitionId: number;
  public partId: number;
  public qty: number;
  public price: number;
  public part: Part;
  public purchaseRequisition: PurchaseRequisition;
}

export class PurchaseState {

  public purchaseStateId: number;
  public purchaseRequisitionId: number;
  public status: number;
  public remark: string;
  public createByUserId: string;
  public dateCreated: Date;
  public approveLevel: number;
  public seq: number;
  public purchaseRequisition: PurchaseRequisition;
}

export class RolePermission {

  public roleId: string;
  public permissionId: number;
  public isActive: boolean;
  public aspNetRole: AspNetRole;
  public permission: Permission;
}

export class Salary {
  public incomeRangeId: number;
  public companyId: number;
  public setStart: number;
  public setEnd: number;
  public description: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
}


export class SaleTarget {

  public saleTargetId: number;
  public targetItemId: number;
  public userId: string;
  public amount: number;
  public seq: number;
  public isActive: boolean;
  public dateStart: Date;
  public dateEnd: Date;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public targetItem: TargetItem;
  public userInfo: UserInfo;
}

export class Site {

  public siteId: number;
  public companyId: number;
  public siteCode: string;
  public siteName: string;
  public notes: string;
  public isActive: boolean;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public placeId: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public radius: number;
  public saleOrders: SaleOrder[];
  public siteItems: SiteItem[];
  public userInfos: UserInfo[];
}

export class SiteItem {

  public siteId: number;
  public itemId: number;
  public qty: number;
  public price: number;
  public salePrice: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public part: Part;
  public site: Site;
}

export class Status {

  public statusId: number;
  public statusName: string;
  public statusNameEn: string;
  public isActive: boolean;
  public statusDesc: string;
  public activityStates: ActivityState[];
}

export class TargetItem {

  public targetItemId: number;
  public targetId: number;
  public amount: number;
  public seq: number;
  public isActive: boolean;
  public dateStart: Date;
  public dateEnd: Date;
  public customerTargets: CustomerTarget[];
  public saleTargets: SaleTarget[];
  public target: Target;
}

export class TopicInterest {

  public topicId: number;
  public companyId: number;
  public topicName: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public isActive: boolean;
  public customerInterests: CustomerInterest[];
  public dealsInterests: DealsInterest[];
}

export class Unit {

  public unitId: number;
  public name: string;
  public companyId: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
}

export class weightUnit {

  public typeId: number;
  public typeName: string;
  public typeDesc: string;
  public status: boolean;
  public typeParent: number;
  public customers: Customer[];
}

export class Brand {

  public brandId: number;
  public brandName: string;
  public companyId: number;
  public isActive: boolean;
  public parts: Part;
}

export class productProperty {

  public productPropertyId: number;
  public productPropertyValueId: number;
  public propertyName: string;
  public description: string;
  public value: string;
  public companyId: number;
  public isActive: boolean;
  public isEnable: boolean;
  public productPropertyValues: productPropertyValue[];
}

export class productPropertyValue {

  public productPropertyId: number;
  public value: string;
  public productPropertyValueId: number;
  public isActive: boolean;
  public isEnable: boolean;
}



export class UnitConversion {

  public unitConversionId: number;
  public itemId: number;
  public fromUnitId: number;
  public fromQty: number;
  public toUnitId: number;
  public toQty: number;
  public part: Part;
}

export class UserAccessLog {

  public logId: number;
  public logType: number;
  public userId: string;
  public ip: string;
  public device: string;
  public userAgent: string;
  public dateCreated: Date;
  public isSuccess: boolean;
  public remark: string;
}

export class UserCompany {

  public userId: string;
  public companyId: number;
  public isDefault: boolean;
  public isActive: boolean;
  public company: Company;
  public userInfo: UserInfo;
}

export class UserInfo {

  public uniqueName: string;
  public userId: string;
  public username: string;
  public companyId: number;
  public staffCode: string;
  public title: string;
  public firstname: string;
  public lastname: string;
  public nickname: string;
  public position: string;
  public division: number;
  public department: number;
  public email: string;
  public extensionNo: number;
  public phoneNumber: string;
  public picture: string;
  public parentId: string;
  public orgString: string;
  public orgLevel: number;
  public isActive: boolean;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public placeId: string;
  public udid1: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public hasPicture: boolean;
  public headUserId: string;
  public isReceiveImportEmail: boolean;
  public staffId: string;
  public siteId: number;
  public activities: Activity[];
  public inventories: Inventory[];
  public purchaseRequisitions: PurchaseRequisition[];
  public saleTargets: SaleTarget[];
  public userCompanies: UserCompany[];
  public userTeams: UserTeam[];
  public userTokens: UserToken[];
  public company: Company;
  public site: Site;
}

export class UserLocationTracking {

  public userLocationTrackingId: number;
  public userId: string;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public dateCreated: Date;
}

export class UserMapToCustomer {

  public userId: string;
  public customerId: number;
  public companyId: number;
  public userOrgString: string;
  public userOrgLevel: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public customer: Customer;
}

export class UserPreference {

  public id: number;
  public userId: string;
  public preferenceId: number;
  public value: string;
  public dateModified: Date;
}

export class UserTeam {

  public userId: string;
  public teamId: number;
  public isDefault: boolean;
  public isActive: boolean;
  public team: Team;
  public userInfo: UserInfo;
}

export class UserToken {

  public userTokenId: number;
  public userId: string;
  public token: string;
  public isActive: boolean;
  public dateCreated: Date;
  public dateExpired: Date;
  public userAgent: string;
  public deviceId: string;
  public userInfo: UserInfo;
}

export class vwActivityAllContact {

  public activityId: number;
  public userOrgString: string;
  public contactId: number;
  public isActive: boolean;
  public teamId: number;
  public teamName: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public statusId: number;
}

export class vwActivityCancelReason {

  public activityId: number;
  public statusId: number;
  public reason: string;
}

export class vwActivityContact {

  public activityId: number;
  public contactNames: string;
  public position: string;
}

export class vwActivityExpense {

  public activityId: number;
  public activityTypeName: string;
  public createdByUserId: string;
  public fullname: string;
  public dateCreated: Date;
}

export class vwActivityForExpense {

  public activityId: number;
  public activityName: string;
  public assignToUserId: string;
  public totalDistance: number;
}

export class vwActivityGroupUser {

  public groupId: number;
  public userId: string;
  public fullname: string;
  public dateProfileModified: Date;
}

export class vwActivityParticipant {

  public activityParticipantId: number;
  public activityId: number;
  public groupId: number;
  public userId: string;
  public status: number;
  public remark: string;
  public dateCreated: Date;
  public createdByUserId: string;
  public username: string;
  public firstname: string;
  public lastname: string;
  public dateModified: Date;
  public companyId: number;
}

export class vwActivitySubtype {

  public activityId: number;
  public activityType: string;
}

export class vwCheckoutRemind {

  public activityId: number;
  public remain: number;
}

export class vwContactCustomer {

  public contactId: number;
  public customerId: number;
  public contactName: string;
  public customerName: string;
  public companyId: number;
  public position: string;
  public email: string;
  public phone: string;
  public mobile: string;
  public notes: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public teamId: number;
  public contactNameEN: string;
  public customerNameEn: string;
}

export class vwCustomerFeedback {

  public feedbackId: number;
  public activityId: number;
  public statusId: number;
  public statusName: string;
  public feedbackTypeId: number;
  public feedbackFromId: number;
  public feedbackFromName: string;
  public note: string;
  public feedbackAnswer: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public dateCheckout: Date;
  public feedbackType: string;
  public feedbackFrom: string;
  public customerId: number;
  public orgString: string;
  public orgLevel: number;
  public feedbackCategoryIds: string;
  public feedbackCategories: string;
  public createdByUserName: string;
  public detailActivities: string;
  public answerBy: string;
  public activityTypeName: string;
  public dateLatestAnswer: Date;
  public subject: string;
  public userOrgString: string;
}

export class vwCustomerLite {

  public customerId: number;
  public customerName: string;
  public customerNameEn: string;
  public isActive: boolean;
  public companyId: number;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public placeId: string;
  public aliasNames: string;
  public customerGroupId: number;
  public customerGroupName: string;
  public customerType: number;
  public addressPart1: string;
  public addressPart2: string;
  public notes: string;
  public dateCreated: Date;
  public dateModified: Date;
  public createdByUserId: string;
  public action: number;
}

export class vwCustomerTarget {

  public targetId: number;
  public companyId: number;
  public measure: number;
  public productTypeId: number;
  public interval: number;
  public targetFor: number;
  public targetItemId: number;
  public customerTargetId: number;
  public customerId: number;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public amount: number;
  public isActive: boolean;
  public dateStart: Date;
  public dateEnd: Date;
}

export class vwCustomerTargetHeader {

  public targetId: number;
  public companyId: number;
  public measure: number;
  public interval: number;
  public refId: number;
  public refId2: number;
  public name: string;
  public name2: string;
  public dateStart: Date;
  public dateEnd: Date;
  public totalAmount: number;
}

export class vwDateActivity {

  public activityId: number;
  public dateActivity: Date;
}

export class vwDeal {

  public dealsId: number;
  public dealsName: string;
  public dealsValue: number;
  public statusId: number;
  public statusName: string;
  public dealsDate: Date;
  public expectedCloseDate: Date;
  public closeDate: Date;
  public probability: number;
  public notes: string;
  public isActive: boolean;
  public dateCreated: Date;
  public createByUserId: string;
  public customerId: number;
  public customerName: string;
  public createByName: string;
  public orgString: string;
  public companyId: number;
  public companyName: string;
}

export class vwDetailActivity {

  public activityId: number;
  public detailActivities: string;
  public detailActivityIds: string;
}

export class vwDetailExpense {

  public expenseId: number;
  public expenseTypeNames: string;
  public expenseTypeIds: string;
}

export class vwExpense {

  public milesStart: number;
  public milesEnd: number;
  public quantity: number;
  public usedFuelCard: boolean;
  public amount1: number;
  public amount2: number;
  public amount3: number;
  public hours: number;
  public minutes: number;
  public note: string;
  public expenseDetailId: number;
  public activityId: number;
  public teamId: number;
  public subject: string;
  public userId: string;
  public username: string;
  public companyId: number;
  public staffCode: string;
  public firstname: string;
  public lastname: string;
  public expenseId: number;
  public statusId: number;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public reason: string;
  public title: string;
  public expenseTypeId: number;
  public name: string;
  public remark: string;
  public waitingApproveByUserId: string;
}

export class vwExpenseDetail {

  public expenseDetailId: number;
  public activityId: number;
  public expenseId: number;
  public expenseTypeId: number;
  public name: string;
  public title: string;
  public milesStart: number;
  public milesEnd: number;
  public quantity: number;
  public amount1: number;
  public amount2: number;
  public amount3: number;
  public note: string;
  public hours: number;
  public minutes: number;
  public usedFuelCard: boolean;
}

export class vwExpensesList {

  public expenseId: number;
  public companyId: number;
  public userId: string;
  public staffName: string;
  public orgString: string;
  public orgLevel: number;
  public role: string;
  public companyName: string;
  public statusId: number;
  public statusName: string;
  public activityId: number;
  public dateActivity: Date;
  public total: number;
  public dateCreated: Date;
  public dateTransaction: Date;
  public dateProfileModified: Date;
  public remark: string;
  public dateApproved: Date;
  public approvedByUserId: string;
  public waitingApproveByUserId: string;
  public dateCheckout: Date;
  public waitingApproveByRoleId: string;
  public subject: string;
  public teamName: string;
  public head1: string;
  public staffCode: string;
  public teamId: number;
}

export class vwFeedbackCategory {

  public feedbackId: number;
  public feedbackCategories: string;
  public feedbackCategoryIds: string;
}

export class vwFeedbackCategoryDetail {

  public feedbackId: number;
  public feedbackCategoryId: number;
  public feedbackCategoryName: string;
  public isActive: boolean;
}

export class vwFeedbackLite {

  public feedbackId: number;
  public activityId: number;
  public statusId: number;
  public feedbackTypeId: number;
  public feedbackFromId: number;
  public feedbackFrom: number;
  public feedbackFromName: string;
  public note: string;
  public feedbackAnswer: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public feedbackTypeName: string;
  public feedbackCategoryId: number;
  public feedbackCategoryName: string;
  public orgLevel: number;
  public orgString: string;
}

export class vwInventoryItem {

  public partId: number;
  public qty: number;
  public price: number;
  public totalPrice: number;
  public partName: string;
  public inventoryItemId: number;
  public inventoryId: number;
  public partCode: string;
}

export class vwLanguage {

  public languageId: string;
  public localeStringResourceId: string;
  public localeResourceKeyId: string;
  public languageName: string;
  public resourceName: string;
  public resourceValue: string;
  public languageCulture: string;
  public isActive: boolean;
}

export class vwMasterType {

  public typeId: number;
  public companyId: number;
  public companyName: string;
  public typeParent: number;
  public typeName: string;
  public typeDesc: string;
}

export class vwPart {

  public partId: number;
  public partTypeId: number;
  public partName: string;
  public qty: number;
  public price: number;
  public companyId: number;
  public unit: string;
  public isActive: boolean;
  public partTypeName: string;
  public partCode: string;
  public notes: string;
  public productCode: number;
}

export class vwPlanActivity {

  public userId: string;
  public staffName: string;
  public orgString: string;
  public orgLevel: number;
  public subject: string;
  public activityId: number;
  public statusId: number;
  public statusName: string;
  public dateActivity: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public dateActivityStart: Date;
  public dateActivityEnd: Date;
  public activityTypeId: number;
  public keyActivity: string;
  public detailActivityId: number;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public isAdHoc: boolean;
  public dateCreated: Date;
  public customerId: number;
  public customerName: string;
  public assignedByUserId: string;
  public notes: string;
  public dateProfileModified: Date;
  public companyId: number;
  public groupId: number;
  public approvedByUserId: string;
  public dateApproved: Date;
  public staffFirstname: string;
  public staffLastname: string;
  public customerPlaceId: string;
  public visitPlaceId: string;
  public totalDistance: number;
}

export class vwPlanLite {

  public activityId: number;
  public teamId: number;
  public assignToUserId: string;
  public orgString: string;
  public dateCreated: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public dateActivity: Date;
  public activityTypeId: number;
  public keyActivities: string;
  public detailActivities: string;
  public locationName: string;
  public location: string;
  public staff: string;
  public statusName: string;
  public statusId: number;
  public rejectReason: string;
  public assignedByUserId: string;
  public assignedBy: string;
}

export class vwProduct {

  public partId: number;
  public partTypeId: number;
  public partName: string;
  public qty: number;
  public price: number;
  public companyId: number;
  public unit: string;
  public notes: string;
  public isActive: boolean;
  public partCode: string;
  public partTypeName: string;
}

export class vwRolePermission {

  public permissionName: string;
  public parent: number;
  public roleId: string;
  public roleName: string;
  public level: number;
  public isActive: number;
  public teamId: number;
  public isActivePermission: boolean;
  public permissionId: number;
  public companyId: number;
}

export class vwSaleOrderDetail {

  public saleOrderId: number;
  public dateCreated: Date;
  public dateOrder: Date;
  public status: number;
  public statusName: string;
  public customerId: number;
  public customerName: string;
  public remark: string;
  public isActive: boolean;
  public saleOrderDetailId: number;
  public partId: number;
  public partName: string;
  public partTypeId: number;
  public partTypeName: string;
  public qty: number;
  public price: number;
  public totalPrice: number;
  public createdByUserId: string;
  public fullname: string;
  public companyId: number;
  public companyName: string;
  public orderType: number;
  public teamId: number;
  public teamName: string;
}

export class vwSalesTargetHeader {

  public targetId: number;
  public companyId: number;
  public measure: number;
  public interval: number;
  public refId: string;
  public name: string;
  public dateStart: Date;
  public dateEnd: Date;
  public totalAmount: number;
  public orgString: string;
}

export class vwSaleTarget {

  public saleTargetId: number;
  public userId: string;
  public fullname: string;
  public companyId: number;
  public companyName: string;
  public orgString: string;
  public amount: number;
  public seq: number;
  public dateStart: Date;
  public dateEnd: Date;
  public name: string;
  public measure: number;
  public interval: number;
  public targetFor: number;
  public targetId: number;
  public targetItemId: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
}

export class vwSiteItem {

  public siteId: number;
  public itemId: number;
  public qty: number;
  public price: number;
  public salePrice: number;
  public isActive: boolean;
  public createdByUserId: string;
  public teamId: number;
  public teamName: string;
  public staffCode: string;
  public fullname: string;
  public orgString: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public partTypeId: number;
  public partTypeName: string;
  public partCode: string;
  public partName: string;
  public companyId: number;
  public companyName: string;
  public siteCode: string;
  public siteName: string;
  public unit: string;
}

export class vwTarget {

  public targetId: number;
  public name: string;
  public measure: number;
  public interval: number;
  public targetFor: number;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public updatedByUserId: string;
  public dateUpdated: Date;
  public userId: string;
  public companyName: string;
  public staffCode: string;
  public firstname: string;
  public lastname: string;
  public fullname: string;
  public position: string;
  public orgString: string;
  public head1: string;
  public headUserId: string;
  public roleId: string;
  public role: string;
}

export class vwUserAncester {

  public fullname: string;
  public ancester: string;
}

export class vwUserCompany {

  public userId: string;
  public companyId: number;
  public companyName: string;
}

export class vwUserPermission {

  public userId: string;
  public roleId: string;
  public permissionId: number;
  public permissionName: string;
  public parent: number;
}

export class Activity_Customer_Summary_Result {

  public customerId: number;
  public keyActivity: string;
  public workingMin: number;
}

export class Activity_Summary_Result {

  public keyActivity: string;
  public averageMinute: number;
  public totalMinute: number;
  public totalActivity: number;
}

export class Agenda_Activity_Result {

  public dateOfMonth: Date;
  public startOfDay: Date;
  public endOfDay: Date;
  public activityId: number;
  public activityTypeName: string;
}

export class Customer_Activity_Result {

  public customerId: number;
  public customerName: string;
  public addressPart1: string;
  public addressPart2: string;
  public isActive: boolean;
  public dateCheckin: Date;
  public totalPlan: number;
  public totalActivity: number;
  public totalContact: number;
  public totalFeedback: number;
}

export class Customer_Centric_Result {

  public customerId: number;
  public customerType: number;
  public distances: number;
}

export class Customer_LeadGroups_Result {

  public customerId: number;
}

export class Customer_NearbyList_Result {

  public customerId: number;
  public companyId: number;
  public dateCreateActivity: Date;
  public dateCheckin: Date;
  public dateCreateFeedback: Date;
  public dateCreateExpense: Date;
  public dateCreateCustomer: Date;
  public dateCreateContact: Date;
  public dateUpdateCustoner: Date;
  public dateUpdateContact: Date;
  public dateModifiedCustomer: Date;
  public branch: string;
  public distances: number;
  public typeDistanceGroup: number;
}

export class Customer_ProspectGroups_Result {

  public customerId: number;
}

export class Customer_Summary_Result {

  public totalCustomer: number;
  public customerVisited: number;
  public customerNoVisit: number;
  public totalActivity: number;
  public feedbackFromCustomer: number;
}

export class CustomerEngagement_Summary_Result {

  public averageMin: number;
  public month: number;
  public year: number;
}

export class CustomerVisit_Summary_Result {

  public month: number;
  public monthName: string;
  public week: number;
  public dateStart: Date;
  public dateEnd: Date;
  public totalActivity: number;
}

export class Deals_Timeline_Result {

  public dealsId: number;
  public dateCreated: Date;
  public expectedCloseDate: Date;
  public statusId: number;
  public dealsName: string;
  public customerName: string;
  public notes: string;
  public dealsValue: number;
  public fullname: string;
  public interests: string;
  public closeDate: Date | string;
  public probability: number;
}

export class Employee_Expenses_Summary_Result {

  public expenseTypeId: number;
  public expenseTypeName: string;
  public total: number;
}

export class Employee_Feedback_Summary_Result {

  public feedbackCategoryId: number;
  public feedbackCategoryName: string;
  public total: number;
}

export class Filter_HomeCustomer_Result {

  public customerId: number;
  public customerCode: string;
  public customerName: string;
}

export class Filter_HomeKeyActivity_Result {

  public activityTypeId: number;
  public activityTypeName: string;
}

export class Filter_HomeRecentUser_Result {

  public userId: string;
  public fullname: string;
  public firstname: string;
  public lastname: string;
  public dateModified: Date;
  public dateActivity: Date;
}

export class GetExpensesSummary_Result {

  public sumPrice: number;
  public sumQty: number;
  public sumWaiting: number;
  public sumApproved: number;
  public sumRejected: number;
}

export class GetInventorySummary_Result {

  public sumPrice: number;
  public sumQty: number;
  public sumWaiting: number;
  public sumApproved: number;
  public sumRejected: number;
  public sumReceived: number;
}

export class GetPurchaseSummary_Result {

  public sumPrice: number;
  public sumQty: number;
  public sumWaiting: number;
  public sumApproved: number;
  public sumRejected: number;
}

export class GetWaitingApprovedBy_Result {

  public waitingApprovedByUserId: string;
  public waitingApprovedByRoleId: string;
  public statusId: number;
}

export class Inventories_Timeline_Result {

  public inventoryId: number;
  public subject: string;
  public customerName: string;
  public inventoryRemark: string;
  public total: number;
  public statusId: number;
  public statusName: string;
  public dateCreated: Date;
}

export class Merge_Customers_Result {

  public status: number;
}

export class Notification_AppBadge_Result {

  public group: string;
  public type: string;
  public badge: number;
}

export class Notification_Infobar_Result {

  public activityId: number;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public keyActivity: string;
  public detailActivities: string;
  public locationName: string;
  public location: string;
  public statusId: number;
}

export class Notification_TodayActivity_Result {

  public token: string;
  public assignToUserId: string;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public subject: string;
  public activityId: number;
}

export class Plan_WaitCheckins_Result {

  public activityId: number;
  public teamId: number;
  public assignToUserId: string;
  public orgString: string;
  public dateCreated: Date;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public dateActivity: Date;
  public activityTypeId: number;
  public keyActivities: string;
  public detailActivities: string;
  public locationName: string;
  public location: string;
  public staff: string;
  public statusName: string;
  public statusId: number;
  public rejectReason: string;
  public assignedByUserId: string;
  public assignedBy: string;
}

export class Report_ActivityDetail_Result {

  public detailActivity: string;
  public keyActivityId: number;
  public activityTypeId: number;
  public count: number;
  public totalHours: number;
}

export class Report_ActivitySeller_Result {

  public userId: string;
  public level: number;
  public staffCode: string;
  public staffName: string;
  public contactId: number;
  public contactName: string;
  public sellerPosition: string;
  public managerId1: string;
  public managerName1: string;
  public managerId2: string;
  public managerName2: string;
  public teamName: string;
  public customerName: string;
  public zoneName: string;
}

export class Report_ActivityWithSeller_Result {

  public activityTypeId: number;
  public userId: string;
  public contactId: number;
  public count: number;
}

export class Report_ActivityWithSellerByStatuses_Result {

  public activityTypeId: number;
  public userId: string;
  public contactId: number;
  public count: number;
  public managerFullname: string;
  public managerUserId: string;
}

export class Report_DashboardCustomerVisitByTeam_Result {

  public teamName: string;
  public visit: number;
  public nonVisit: number;
}

export class Report_DashboardKeyOverview_Result {

  public keyActivity: string;
  public count: number;
  public activityTypeId: number;
  public totalHours: number;
  public teamId: number;
}

export class Report_DashboardSellerVisit_Result {

  public statusId: number;
  public statusName: string;
  public isCheckin: number;
  public count: number;
  public userId: string;
  public firstName: string;
  public lastName: string;
  public fullname: string;
  public roleName: string;
  public managerFullName: string;
  public managerUserId: string;
  public contactId: number;
  public contactName: string;
}

export class Report_DetailActivity_Result {

  public detailActivity: string;
  public count: number;
  public parentId: number;
  public activityTypeId: number;
  public totalHours: number;
  public teamId: number;
}

export class Report_ExpenseByType_Result {

  public expenseTypeId: number;
  public name: string;
  public totalAmount: number;
}

export class Report_ExpenseByUser_Result {

  public teamName: string;
  public totalAmount: number;
}

export class Report_ExpenseOfTypeByUser_Result {

  public userId: string;
  public staffName: string;
  public totalAmount: number;
}

export class Report_ExpenseOfUserByType_Result {

  public expenseTypeId: number;
  public name: string;
  public totalAmount: number;
}

export class Report_Feedback_Result {

  public feedbackId: number;
  public activityId: number;
  public keyActivity: string;
  public detailActivities: string;
  public teamId: number;
  public teamName: string;
  public dateCreated: Date;
  public activityTypeId: number;
  public activityTypeName: string;
  public feedbackTypeId: number;
  public feedbackTypeName: string;
  public feedbackCategories: string;
  public feedbackCategoryIds: string;
  public feedbackFromId: number;
  public feedbackFrom: string;
  public feedbackFromName: string;
  public note: string;
  public statusId: number;
  public statusName: string;
  public createdByUserName: string;
  public createdByUserId: string;
  public feedbackAnswer: string;
  public answerBy: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public activityStatusId: number;
  public staffCode: string;
  public dateUpdated: Date;
  public updatedBy: string;
  public username: string;
  public fullname: string;
  public userRoleId: string;
  public userOrgString: string;
  public userOrgLevel: number;
  public roleName: string;
  public customerName: string;
}

export class Report_FeedbackSummary_Result {

  public feedbackCategoryId: number;
  public teamId: number;
  public feedbackCategoryName: string;
  public total: number;
  public pendingQty: number;
  public completeQty: number;
  public pendingPercent: number;
  public completePercent: number;
  public companyId: number;
}

export class Report_FeedbackTable_Result {

  public feedbackId: number;
  public activityId: number;
  public teamId: number;
  public teamName: string;
  public dateCreated: Date;
  public activityTypeId: number;
  public activityTypeName: string;
  public feedbackTypeId: number;
  public feedbackTypeName: string;
  public feedbackCategories: string;
  public feedbackCategoryIds: string;
  public feedbackFromId: number;
  public feedbackFrom: string;
  public feedbackFromName: string;
  public note: string;
  public statusId: number;
  public statusName: string;
  public createdByUserName: string;
  public updatedByUserName: string;
  public createdByUserId: string;
  public feedbackAnswer: string;
  public answerBy: string;
  public detailActivities: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public activityStatusId: number;
  public staffCode: string;
  public staffId: string;
  public username: string;
  public fullname: string;
  public userRoleId: string;
  public userOrgString: string;
  public userOrgLevel: number;
  public roleName: string;
  public customerName: string;
  public customerAlias: string;
  public userId: string;
  public companyId: number;
  public customerId: number;
  public dateLastAnswer: Date;
  public dateUpdated: Date;
  public lastPostUserId: string;
  public lastPostFullName: string;
  public lastAnswer: string;
  public subject: string;
  public rowNumber: number;
}

export class Report_InventoryByCategory_Result {

  public partTypeId: number;
  public partTypeName: string;
  public totalPrice: number;
}

export class Report_InventoryByTeam_Result {

  public teamName: string;
  public totalPrice: number;
}

export class Report_InventoryTable_Result {

  public dateCreated: Date;
  public invNo: string;
  public inventoryId: number;
  public createdByUserId: string;
  public username: string;
  public fullname: string;
  public teamId: number;
  public teamName: string;
  public subject: string;
  public customerName: string;
  public total: number;
  public totalQty: number;
  public statusId: number;
  public statusName: string;
  public statesRemark: string;
  public inventoryRemark: string;
}

export class ActivityReport {
  public activityId: number;
  public attachments: Attachment[];
  public report: string;
  public comment: string;
}

export class Report_MainActivity_Result {

  public activityId: number;
  public dateCreated: Date;
  public dateAppointmentStart: Date;
  public subject: string;
  public activityTypeName: string;
  public activityType: string;
  public activityReport: ActivityReport;
  public notes: string;
  public customerName: string;
  public contactNames: string;
  public staffCode: string;
  public fullname: string;
  public role: string;
  public head1: string;
  public teamName: string;
  public dateCheckin: Date;
  public dateCheckout: Date;
  public planLocation: string;
  public checkInLocation: string;
  public workingHour: number;
  public statusId: number;
  public statusName: string;
  public groupStatus: string;
  public total: number;
  public checkinAdhoc: string;
  public checkoutLocation: string;
  public totalDistance: number;
  public customerCode: string;
  public comments: Comment[];
  public customerTypeName: string;
  public contactMobiles: string;
  public expenses: Expense[];
}

export class Report_MainCase_Result {

  public feedbackId: number;
  public dateCreated: Date;
  public employee: string;
  public roleName: string;
  public subject: string;
  public caseFrom: string;
  public caseType: string;
  public note: string;
  public caseAction: string;
  public statusName: string;
  public feedbackCategories: string;
  public countConversation: number;
  public closeBy: string;
  public customerName: string;
  public durationDay: string;
  public durationHour: string;
  public durationMinute: string;
  public total: number;
}

export class Report_MainExpense_Result {

  public rowNumber: number;
  public expenseId: number;
  public statusId: number;
  public statusName: string;
  public dateTransaction: Date;
  public dateActivity: Date;
  public dateCreated: Date;
  public activityTypeName: string;
  public detailActivities: string;
  public location: string;
  public customerName: string;
  public contactNames: string;
  public night: number;
  public accomodation: number;
  public entertain: number;
  public taxi: number;
  public tollway: number;
  public parking: number;
  public travelling: number;
  public staffQty: number;
  public meetingStaff: number;
  public staffAllowance: number;
  public trainingTitle1: string;
  public trainingPerson1: number;
  public training1: number;
  public trainingTitle2: string;
  public trainingPerson2: number;
  public training2: number;
  public trainingTitle3: string;
  public trainingPerson3: number;
  public training3: number;
  public trainingAll: number;
  public otherDescription1: string;
  public other1: number;
  public otherDescription2: string;
  public other2: number;
  public otherDescription3: string;
  public other3: number;
  public otherAll: number;
  public milesStart: number;
  public milesEnd: number;
  public distances: number;
  public fuel: number;
  public teamName: string;
  public staffCode: string;
  public staffName: string;
  public role: string;
  public supName: string;
  public activityId: number;
  public startLocation: string;
  public visitLocation: string;
  public returnLocation: string;
  public timeSpending: number;
  public durationDay: string;
  public durationHour: string;
  public durationMinute: string;
  public remark: string;
  public usedFuelCard: string;
  public statusActivity: number;
  public totalExp: number;
  public distanceActivity: number;
  public total: number;
  public details: ExpenseDetail[];
}

export class Report_OpportunityConversionSummary_Result {

  public groupStatus: string;
  public total: number;
}

export class Report_OpportunitySaleTargetSummary_Result {

  public totalWon: number;
  public wonValue: number;
  public totalPotential: number;
  public potentialValue: number;
  public target: number;
}

export class Report_OppotunityByDealInterest_Result {

  public topicId: number;
  public topicName: string;
  public wonRate: number;
  public won: number;
  public totalDeal: number;
}

export class Report_PurchaseRequisitionsByStatus_Result {

  public statusName: string;
  public statusCount: number;
}

export class Report_PurchaseRequisitionsByTeam_Result {

  public teamName: string;
  public totalPrice: number;
}

export class Report_PurchaseRequisitionsTable_Result {

  public dateCreated: Date;
  public purchaseRequisitionId: number;
  public prNo: string;
  public createdByUserId: string;
  public username: string;
  public fullname: string;
  public teamId: number;
  public teamName: string;
  public subject: string;
  public vendorName: string;
  public companyId: number;
  public companyName: string;
  public status: number;
  public statusName: string;
  public totalPrice: number;
  public remark: string;
}

export class Report_SaleOrderSummary_Result {

  public rowNumber: number;
  public customerId: number;
  public customerName: string;
  public customerWallets: number;
  public totalPrice: number;
  public totalTarget: number;
  public percents: number;
}

export class Report_SalePerformance_Result {

  public customerId: number;
  public customerName: string;
  public dealsId: number;
  public dealsName: string;
  public interestIn: string;
  public dealDate: Date;
  public closeDate: Date;
  public statusId: number;
  public dealStage: string;
  public dealValue: number;
  public note: string;
}

export class SaleOrder_Timeline_Result {

  public saleOrderId: number;
  public fullname: string;
  public partName: string;
  public totalPrice: number;
  public dateComplete: Date;
  public dateOrder: Date;
  public status: number;
  public remark: string;
  public saleOrders: SaleOrderList_Result[];
}

export class SaleOrderList_Result {

  public saleOrderId: number;
  public saleOrderNo: string;
  public status: number;
  public statusName: string;
  public dateOrder: Date;
  public customerId: number;
  public customerName: string;
  public createdByUserId: string;
  public dateCreated: Date;
  public totalPrice: number;
  public fullname: string;
  public orgString: string;
  public isActive: boolean;
  public remark: string;
  public companyId: number;
  public orderType: number;
  public saleOrderDetails: SaleOrder;
}

export class Team_GetStaffsWithStatus_Result {

  public userId: string;
  public fullname: string;
  public teamId: number;
  public teamName: string;
  public dateAppointmentStart: Date;
  public dateAppointmentEnd: Date;
  public keyActivity: string;
  public detailActivities: string;
  public contact: string;
  public dateProfileModified: Date;
  public roleName: string;
}

export class User_CustomerVisit_Result {

  public customerId: number;
  public customerCode: string;
  public customerName: string;
  public keyActivity: string;
  public countPlan: number;
  public countActivity: number;
  public dateFirstVisited: Date;
  public dateLastVisited: Date;
  public countContact: number;
}

export class User_LocationTracking_Result {

  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public dateCreated: Date;
}

export class User_LocationTrackingGroup_Result {

  public dateGroup: Date;
  public dateLocation: Date;
  public locationFound: number;
}

export class User_ReportingManager_Result {

  public userId: string;
  public fullname: string;
  public username: string;
  public role: string;
}

export class User_ReportingUser_Result {

  public userId: string;
  public fullname: string;
  public username: string;
  public orgString: string;
  public orgLevel: number;
  public role: string;
}

export class WaitingApproveBy_Result {

  public userId: string;
  public username: string;
  public companyId: number;
  public companyName: string;
  public teamId: number;
  public teamName: string;
  public staffCode: string;
  public staffId: string;
  public title: string;
  public firstname: string;
  public lastname: string;
  public fullname: string;
  public nickname: string;
  public position: string;
  public positionName: string;
  public division: number;
  public divisionName: number;
  public department: number;
  public departmentName: number;
  public email: string;
  public extensionNo: number;
  public phoneNumber: string;
  public picture: string;
  public parentId: number;
  public orgString: string;
  public orgLevel: number;
  public latitude: number;
  public longitude: number;
  public location: string;
  public locationName: string;
  public udid1: string;
  public isActive: boolean;
  public createdByUserId: string;
  public dateCreated: Date;
  public modifiedByUserId: string;
  public dateModified: Date;
  public hasPicture: boolean;
  public head1: string;
  public head2: string;
  public headUserId: string;
  public isReceiveImportEmail: boolean;
  public role: string;
  public roleId: string;
  public level: number;
  public placeId: string;
  public siteId: number;
}

export class WaitingApprovedList_Result {

  public userId: string;
  public fullname: string;
  public dateCreated: Date;
  public dateModified: Date;
}

export class CustomerFieldConfiguration {
  public id: number;
  public customerType: number;
  public fixedFieldName: string;
  public fixedFieldState: number;
  public fixedFieldMaxLength: number;
  public type: number;
  public typeOptions: number;
  public options: Object;
  public isActive: boolean;
  public seq: number;
}

export class CustomerDynamicData {
  public id: number;
  public fixedFieldName: string;
  public value: string[];
  public seq: number;
}
