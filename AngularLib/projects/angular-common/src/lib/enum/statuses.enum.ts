export enum Statuses {
  Active = 1,
  InActive = 0,

  PendingPlan = 100,
  WaitingApprovePlan = 101,
  WaitingCheckin = 102,
  WaitingApproveRevisePlan = 103,
  CancelPlan = 104,
  InProgress = 105,
  DoneActivity = 106,
  PendingReport = 109,

  FeedbackOpen = 201,
  FeedbackInProgress = 202,
  FeedbackClosed = 203,
  FeedbackDeleted = 209,

  ExpWaitingApprove = 301,
  ExpApproved = 302,
  ExpRejected = 303,
  ExpProcessed = 304,
  ExpPaid = 305,

  WaitingApproveInventory = 401,
  ApprovedInventory = 402,
  RejectedInventory = 403,
  ReceivedInventory = 404,

  WaitingApprovePO = 501,
  ApprovePO = 502,
  RejectPO = 503,

  WaitingAccept = 601,
  Accept = 602,
  Decline = 603,
  WaitingAcceptRevisePlan = 604,

  DealOpen = 701,
  DealInProgress = 702,
  DealWon = 703,
  DealMiss = 704,
  CancelDeal = 705,


  WaitingApproveMemo = 801,
  ApprovedMemo = 802,
  RejectedMemo = 803,
  CancelMemo = 804,

  SoOpenOrder = 901,
  SoCompletedOrder = 902,
  SoRejectOrder = 903,

  Comment = 1001,
  CreatePlanComment = 1002,
  EditPlanComment = 1003,
  RevisePlanComment = 1004,
  CancelPlanComment = 1005,
  ApprovePlanComment = 1006,
  CheckInComment = 1007,
  CheckOutComment = 1008,

  OpenOrder = 1101,
  RecieptOrder = 1102,
  RejectOrder = 1103,

  UnassignCase = 1201,
  AssignCase = 1202,
  UnlinkActivityCase = 1203,
  LinkActivityCase = 1204,
  UnlinkDealCase = 1205,
  LinkDealCase = 1206,

  PendingApprovalQuotation = 1301,
  ApprovedQuotation = 1302,
  RejectedQuotation = 1303,
  Sent = 1304,
  Delete = 1305,


  InProgressImport = 1501,
  Completed = 1502,
  Failed = 1503,

  PaidInvoice = 12001,
  OverdueInvoice = 12002,
  SentInvoice = 12003,

  ContractNew = 1601,
  ContractActive = 1602,
  ContractCompleted = 1603,
  ContractCancelled = 1604,

  AddActivityReport = 1042,
  EditActivityReport = 1043
}



























































































