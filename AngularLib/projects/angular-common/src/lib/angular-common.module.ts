import { CommonModule, DatePipe, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { NgxPicaModule, NgxPicaService } from '@digitalascetic/ngx-pica';

import { AlertService } from './services/alert.service';
import { AngularCommonComponent } from './angular-common.component';
import { AssetsPipe } from './pipe/assets.pipe';
import { AssetsService } from './services/assets.service';
import { AttachmentLazyloadDirective } from './directive/attachment-lazyload.directive';
import { AttachmentListComponent } from './component/attachment-list/attachment-list.component';
import { AttachmentModalComponent } from './component/attachment-modal/attachment-modal.component';
import { AttachmentPipe } from './pipe/attachment.pipe';
import { AttachmentProfileComponent } from './component/attachment-profile/attachment-profile.component';
import { AttachmentSecurePipe } from './pipe/attachment-secure.pipe';
import { CollapseComponent } from './component/collapse/collapse.component';
import { DateRangePickerComponent } from './component/date-range-picker/date-range-picker.component';
import { DefaultPipe } from './pipe/default.pipe';
import { DialogComponent } from './component/dialog/dialog.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { DomChangeDirective } from './directive/dom-change.directive';
import { DragDropFileDirective } from './component/upload-file/upload-file.directive';
import { DropdownComponent } from './component/dropdown/dropdown.component';
import { DurationTimePipe } from './pipe/duration-time.pipe';
import { FileManagerComponent } from './component/file-manager/file-manager.component';
import { HotToastModule } from '@ngneat/hot-toast';
import { HoverCardComponent } from './component/hover-card/hover-card.component';
import { HoverCardDirective } from './component/hover-card/hover-card.directive';
import { ImgLazyloadDirective } from './directive/img-lazyload.directive';
import { LibConfig } from './model/config';
import { MarkedPipe } from './pipe/marked.pipe';
import { NgZorroAntdModule } from './module/ng-zorro-antd.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NumericTextBoxComponent } from './component/numeric-text-box/numeric-text-box.component';
import { PhrasePipe } from './pipe/phrase.pipe';
import { PhraseService } from './services/phrase.service';
import { ReadingTimePipe } from './pipe/reading-time.pipe';
import { SafePipe } from './pipe/safe.pipe';
import { SecureDataUrlPipe } from './pipe/secure-data-url.pipe';
import { ShortNumberPipe } from './pipe/short-number.pipe';
import { SidebarDirective } from './directive/sidebar.directive';
import { SizePipe } from './pipe/size.pipe';
import { TabComponent } from './component/tab/tab.component';
import { TabItemDirective } from './component/tab/tab-Item.directive';
import { TabItemsDirective } from './component/tab/tab-Items.directive';
import { TagsComponent } from './component/tags/tags.component';
import { ThaiDatePipe } from './pipe/thai-date.pipe';
import { TooltipComponent } from './component/tooltip/tooltip.component';
import { TooltipDirective } from './component/tooltip/tooltip-directive/tooltip.directive';
import { UploadFileComponent } from './component/upload-file/upload-file.component';
import { UploaderComponent } from './component/uploader/uploader.component';
import en from '@angular/common/locales/en';

registerLocaleData(en);

const components = [
  AttachmentListComponent,
  AttachmentModalComponent,
  AttachmentProfileComponent,
  CollapseComponent,
  DateRangePickerComponent,
  DropdownComponent,
  DialogComponent,
  FileManagerComponent,
  HoverCardComponent,
  NumericTextBoxComponent,
  TabComponent,
  TagsComponent,
  TooltipComponent,
  UploaderComponent,
  UploadFileComponent,
];
const pipes = [
  AttachmentPipe,
  AttachmentSecurePipe,
  AssetsPipe,
  DefaultPipe,
  DurationTimePipe,
  MarkedPipe,
  PhrasePipe,
  ReadingTimePipe,
  SafePipe,
  SecureDataUrlPipe,
  ShortNumberPipe,
  SizePipe,
  ThaiDatePipe,
];
const directives = [
  AttachmentLazyloadDirective,
  DomChangeDirective,
  DragDropFileDirective,
  HoverCardDirective,
  ImgLazyloadDirective,
  SidebarDirective,
  TabItemDirective,
  TabItemsDirective,
  TooltipDirective,
];
const modules = [
  NgZorroAntdModule,
];

@NgModule({
  declarations: [
    ...components,
    ...directives,
    ...pipes,
    AngularCommonComponent,
  ],
  imports: [
    CommonModule,
    DialogModule,
    FormsModule,
    HotToastModule.forRoot({ reverseOrder: true }),
    NgxExtendedPdfViewerModule,
    NgxPicaModule,
    ReactiveFormsModule,
    ...modules
  ],
  exports: [
    ...modules,
    ...components,
    ...directives,
    ...pipes
  ]
})
export class AngularCommonModule {
  static forRoot(libConfig: LibConfig): ModuleWithProviders<AngularCommonModule> {
    return {
      ngModule: AngularCommonModule,
      providers: [
        DatePipe,
        AssetsService,
        AlertService,
        PhraseService,
        NgxPicaService,
        {
          provide: 'LIB_CONFIG',
          useValue: libConfig
        },
        {
          provide: 'PHRASE_PROJECT',
          useValue: libConfig.phraseProject
        },
        {
          provide: 'API_URL_BASE',
          useValue: libConfig.apiUrlBase
        },
        { provide: NZ_I18N, useValue: en_US },
      ]
    };
  }
}
