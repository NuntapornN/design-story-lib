import { AssetsService } from './../services/assets.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sAssets'
})
export class AssetsPipe implements PipeTransform {
  constructor(private assets : AssetsService) {}
  transform(value: any): any {
    return this.assets.transform(value);
  }

}
