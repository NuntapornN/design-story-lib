import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { DEFAULT_FILE_TYPE } from '../constansts/global.constansts';

@Pipe({
  name: 'sAttachment'
})
export class AttachmentPipe implements PipeTransform {
  constructor(public sanitizer: DomSanitizer) { }
  transform(value: any, field: string = 'url'): Observable<any> {
    return new Observable<any>((observer) => {
      if (!value) { return; }
      if (value.extension.startsWith('image/')) {
        observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(value[field]));
        observer.complete();
      } else if (value.extension.startsWith('audio/')) {
        observer.next(DEFAULT_FILE_TYPE.Audio);
        observer.complete();
      } else if (value.extension.startsWith('video/')) {
        observer.next(DEFAULT_FILE_TYPE.Video);
        observer.complete();
      } else if (value.extension.startsWith('application/pdf')) {
        observer.next(DEFAULT_FILE_TYPE.PDF);
        observer.complete();
      } else if (value.extension.startsWith('application/vnd.ms-excel')) {
        observer.next(DEFAULT_FILE_TYPE.Excel);
        observer.complete();
      } else if (value.extension.startsWith('application/vnd.ms-powerpoint')) {
        observer.next(DEFAULT_FILE_TYPE.PPT);
        observer.complete();
      } else if (value.extension.startsWith('application/vnd.ms-works') || value.extension.startsWith('application/msword')) {
        observer.next(DEFAULT_FILE_TYPE.DOC);
        observer.complete();
      } else {
        observer.next(DEFAULT_FILE_TYPE.Default);
        observer.complete();
      }
    });
  }
}
