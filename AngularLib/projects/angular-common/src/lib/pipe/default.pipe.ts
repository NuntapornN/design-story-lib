import { AssetsService } from './../services/assets.service';
import { Pipe, PipeTransform, Inject } from '@angular/core';
import { DEFAULT_PICTURE_PLACEHOLDER } from '../constansts/global.constansts';

@Pipe({ name: 'sDefault', pure: true })
export class DefaultPipe implements PipeTransform {
  constructor(@Inject('API_URL_BASE') public apiUrl: string,private assets : AssetsService) { }

  transform(value: any, defaultValue: any): any {
    if (defaultValue == 'str') {
      return value || '-';

    } else if (defaultValue == 'pic') {
      if (value) {
        return this.apiUrl + value;
      } else {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.NoPhoto);
      }

    } else if (defaultValue == 'picUser') {
      if (value) {
        return this.apiUrl + value;
      } else {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.User);
      }

    } else if (defaultValue == 'attachment') {
      let val: any = value;
      let type = val.extension = val.extension || val.type;
      val.thumbnail = val.thumbnail || val.rawFile;
      if (type.search('image') != -1) {
        if (val.localFileURL) {
          return val.localFileURL;
        }
        return this.apiUrl + val.thumbnail;
      } else if (value.extension.startsWith('audio/')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Audio);
      } else if (value.extension.startsWith('video/')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Video);
      } else if (value.extension.startsWith('application/pdf')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Pdf);
      } else if (value.extension.startsWith('application/vnd.ms-excel')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Excel);
      } else if (value.extension.startsWith('application/vnd.ms-powerpoint')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.PPT);
      } else if (value.extension.startsWith('application/vnd.ms-works') || value.extension.startsWith('application/msword')) {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Document);
      } else {
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.UnSupport);
      }
    }
  }
}
