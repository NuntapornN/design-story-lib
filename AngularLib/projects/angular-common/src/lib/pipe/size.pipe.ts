import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sSize'
})
export class SizePipe implements PipeTransform {
  transform(number: number = 0, unit: any = 'bytes'): any {
    if (isNaN(number) || number === null || number === 0) number = 0;
    let bytes = 0;
    switch (unit) {
      case "MB": bytes = number * Math.pow(1024, 2); break;
      case "GB": bytes = number * Math.pow(1024, 3); break;
      default: bytes = number;
    }
    if (bytes < 1024) {
      return bytes + ' bytes';
    } else if (bytes / 1024 < 1024) {
      return (bytes / 1024).toFixed(0) + ' kb';
    } else if (bytes / Math.pow(1024, 2) < 1024) {
      return (bytes / Math.pow(1024, 2)).toFixed(1) + ' MB';
    } else {
      return (bytes / Math.pow(1024, 3)).toFixed(1) + ' GB';
    }
  }
}
