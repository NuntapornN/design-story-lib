import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { AttachmentSecureService } from '../services/attachment-secure.service';
import { Attachment } from './../model/venio.model';
@Pipe({
  name: 'sAttSecure'
})
export class AttachmentSecurePipe implements PipeTransform {
  constructor(private attachmentSecureService: AttachmentSecureService) { }

  transform(value: Attachment, isAllCompany: boolean = false, isAnnoucement = false): Observable<any> {
    return this.attachmentSecureService.transform(value, isAllCompany, isAnnoucement);
  }
}
