import { isNullOrUndefined } from '@syncfusion/ej2-base';
import { Pipe, PipeTransform } from '@angular/core';
import { map } from 'rxjs/operators';
import { Configuration } from '../model/config';

@Pipe({
  name: 'sDurationTime'
})
export class DurationTimePipe implements PipeTransform {
  constructor() { }

  transform(value: number, args: any = 'hr', empty: any = '-'): any {
    return Configuration.languageChange.pipe(
      map(
        lang => {
          let thLang: boolean = lang == 'th-TH';
          if (isNullOrUndefined(value) || value == 0) return empty;
          let min = value;
          let hours = Math.round(min / 60);
          let day: number;
          let month: number;
          min = min % 60;
          let year = Math.floor(hours * 0.000114);
          let result: string = '';
          if (year >= 1) {
            result += year + (thLang ? ' ปี ' : 'y ');
            hours = hours - (year * 365 * 24);
          }
          if (args == 'month') {
            month = Math.floor(hours / (30 * 24));
            hours = hours % (30 * 24);
            if (month) { result += month + (thLang ? ' เดือน ' : 'mo ') }
            else {
              if (hours < 24) args = 'hr'
              else args = 'day';
            };
          }
          if (args != 'hr') {
            day = Math.round(hours / 24);
            hours = hours % 24;
            if (day && year < 1) result += day + (thLang ? ' วัน ' : 'd ');
          }
          if (args != 'month' && args != 'day') {
            if (hours) result += hours + (thLang ? ' ชม. ' : 'hr ');
            if (min) result += min + (thLang ? ' น. ' : 'm ');
          }
          return result.trim();
        }
      )
    );


  }

}
