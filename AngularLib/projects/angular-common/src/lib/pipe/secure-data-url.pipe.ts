import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { SecureDataUrlService } from '../services/secure-data-url.service';


@Pipe({
  name: 'sSecureDataUrl'
})
export class SecureDataUrlPipe implements PipeTransform {

  /**
   *
   */
  constructor(
    private service: SecureDataUrlService
  ) {
  }

  transform(url: string, type: string = 'attachment', isAllCompany: boolean = false): Observable<any> {
    return this.service.transform(url, type, isAllCompany);
  }

}


