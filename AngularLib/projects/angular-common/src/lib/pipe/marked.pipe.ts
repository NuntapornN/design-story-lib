import { Pipe, PipeTransform } from "@angular/core";
import * as marked from "marked";

@Pipe({
  name: "sMarked"
})
export class MarkedPipe implements PipeTransform {
  transform(value: any): any {
    // TODO
    if (value && value.length > 0) {
      //   let data = value.split('\n');
      //   let text = data[0]; 
      //   data.slice(1).forEach((s, i) => {
      //     if ((s[0] == '|' && s[s.length - 1] == '|') || s == ''){
      //       text += '\n' + s;
      //     }
      //     else {
      //       text += '  \n' + s;      
      //     }
      //   });
      return marked(value);
    }
    return value;
  }

}
