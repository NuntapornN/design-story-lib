import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Configuration } from '../model/config';

@Pipe({
  name: 'sDate'
})
export class ThaiDatePipe implements PipeTransform {

  constructor(private datePipe: DatePipe) { }

  transform(dateStr: string | Date, format: string = 'd/MM/yyyy'): Observable<string> {
    return Configuration.languageChange.pipe(map(lang => {
      if (dateStr === null || dateStr === undefined) { return '-'; }
      if (lang === 'th-TH') {
        const date = new Date(dateStr);
        const m = parseInt(this.datePipe.transform(date, 'MM'), 0) - 1;
        const d = date.getDay();
        if (format.search('MMMM') > -1) {
          const monthT = [
            'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
          ];
          format = format.replace('MMMM', monthT[m]);
        } else if (format.search('MMM') > -1) {
          const monthT = [
            'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
            'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
          ];
          format = format.replace('MMM', monthT[m]);
        }
        if (format.search('EEEE') > -1) {
          const dayT = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
          format = format.replace('EEEE', dayT[d]);
        } else if (format.search('EEE|EE|E') > -1) {
          const dayT = ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'];
          format = format.replace(/(E{1,3})\w/g, dayT[d]);
        }
      }
      return this.datePipe.transform(dateStr, format);
    }))
  }
}
