import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { PhraseService } from '../services/phrase.service';

@Pipe({
  name: 'sPhrase'
})
export class PhrasePipe implements PipeTransform {
  constructor(
    private phraseService: PhraseService
  ) { }

  transform(key: string): Observable<any> {
    return this.phraseService.transform(key);
  }

}
