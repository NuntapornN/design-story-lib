import { Pipe, PipeTransform, OnDestroy, NgZone, ChangeDetectorRef } from '@angular/core';
import { ReadingTimeService } from './../services/reading-time.service';
import { Configuration } from './../model/config';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'sReadingTime'
})
export class ReadingTimePipe implements PipeTransform, OnDestroy {
  private timer: number | null;
  constructor(private _ngZone: NgZone,
    private _changeDetectorRef: ChangeDetectorRef,
    private timeAgoService: ReadingTimeService) {
  }


  transform(value: any, enableToday: boolean = false, isShortFormat: any = '-', isLowerCase?): any {
    if (!value) { return; }
    this.removeTimer();
    const date = new Date(value);
    const now = new Date();
    const seconds = Math.round(Math.abs((now.getTime() - date.getTime()) / 1000));
    const timeToUpdate = this.timeAgoService.getSecondsUntilUpdate(seconds) * 1000;
    this.timer = this._ngZone.runOutsideAngular(() => {
      if (typeof window !== 'undefined') {
        return window.setTimeout(() => {
          this._ngZone.run(() => this._changeDetectorRef.markForCheck());
        }, timeToUpdate);
      }
      return null;
    });
    if (enableToday && date.toLocaleDateString() == now.toLocaleDateString()) {
      return Configuration.languageChange.pipe(
        map(
          lang => {
            let result: string = this.timeAgoService.toDay(lang);         
            return result;
          }
        )
      );
    }
    return Configuration.languageChange.pipe(
      map(
        lang => {
          let result: string = this.timeAgoService.timeAgoSince(date, lang, now, isShortFormat);
          if (isLowerCase) { result = result.toLocaleLowerCase(); }
          return result;
        }
      )
    );
  }

  ngOnDestroy() {
    this.removeTimer();
  }

  private removeTimer() {
    if (this.timer) {
      window.clearTimeout(this.timer);
      this.timer = null;
    }
  }
}
