export const DEFAULT_FILE_TYPE = {
    Picture: 'assets/images/default-pic/file.svg',
    Audio: 'assets/images/default-pic/audio.svg',
    Video: 'assets/images/default-pic/vdo.svg',
    PDF: 'assets/images/default-pic/pdf.svg',
    Excel: 'assets/images/default-pic/excel.svg',
    DOC: 'assets/images/default-pic/doc.svg',
    PPT: 'assets/images/default-pic/ppt.svg',
    Default: 'assets/images/default-pic/file.svg',
};

export const DEFAULT_PICTURE_PLACEHOLDER = {
    Loading: 'assets/images/default-pic/loading.gif',
    User: 'assets/images/default-pic/user.svg',
    Photo: 'assets/images/default-pic/placeholder-photo.svg',
    Audio: 'assets/images/default-pic/audio.svg',
    Document: 'assets/images/default-pic/doc.svg',
    UnSupport: 'assets/images/default-pic/file.svg',
    Video: 'assets/images/default-pic/vdo.svg',
    NoPhoto: 'assets/images/default-pic/no-photo.svg',
    Pdf: 'assets/images/default-pic/pdf.svg',
    Excel: 'assets/images/default-pic/excel.svg',
    PPT: 'assets/images/default-pic/ppt.svg',
    venio: 'assets/images/empty-stage/venio-attachment.svg',
    product: 'assets/images/empty-stage/product-empty.svg',
};

export const DEFAULT_ASSETS_PATH = {
    dev: 'https://dev.tks.co.th/gofive-core/',
    uat: 'https://uat.tks.co.th/gofive-core/',
    prod: 'https://app.gofive.co.th/',
};

export const SECURE_DATA_URL_TYPE = {
    Picture: 'pic',
    UserPicture: 'picUser',
    UserDataUrl: 'userDataURL',
    FullPathUser: 'fullPathUser',
    Attachment: 'attachment',
    Product: 'product',
};

export const LANGUAGE_KEY = {
    EN: 'en-GB',
    TH: 'th-TH'
};

export const PHRASE_CACHE_KEY = {
    EN: 'sPhraseResponceEN',
    TH: 'sPhraseResponceTH',
    LastUpdate: 'sPhrase_response_stored_at',
    'en-GB': 'sPhraseResponceEN',
    'th-TH': 'sPhraseResponceTH',
    ExpiredIn: 3600000
};
export const PHRASE = {
    GofiveCore: {
        phraseId: 'f7dc3ae7fb780b1603695036d9494146',
        phraseToken: 'token ebc99fb96fee0009c8290178b1971cc54305a495e3fedc5be2681e348bccc5b7'
    },
    Venio: {
        phraseId: 'a136ca0e3de87f2709ac0db0cb04fa8d',
        phraseToken: 'token 1dcc75f33c544b7dc52052153f37b913bd170865e18fa2ab8f14a66702a9b5fa'
    },
    empeo: {
        phraseId: 'dca7aeaa2aa3ad4cf5cb68d1760854fa',
        phraseToken: 'token a06db78ca236d4e0bed85b7088fe9d81724597434ae99b9e6b32111c4b001590'
    }
};
