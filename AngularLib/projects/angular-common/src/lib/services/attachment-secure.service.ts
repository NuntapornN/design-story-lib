import { AssetsService } from './assets.service';
import { DEFAULT_PICTURE_PLACEHOLDER } from './../constansts/global.constansts';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, NgZone } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import heic2any from 'heic2any';
import { Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { ImageStorageService } from '../services/image-storage.service';
import { SecureDataUrlService } from './secure-data-url.service';
import { Attachment } from './../model/venio.model';

export enum TypeData {
  base64 = 'base64',
  dataURL = 'dataURL',
  blob = 'blob'
}

export enum ExtensionType {
  image = 'image/',
  audio = 'audio/',
  video = 'video/',
  pdf = 'application/pdf',
  excelXLS = 'application/vnd.ms-excel',
  excelXLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  excelXML = 'application/xml',
  ppt = 'application/vnd.ms-powerpoint',
  pptx = 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  docWork = 'application/vnd.ms-works',
  docWord = 'application/msword',
  docX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  file = '*',
}



@Injectable({
  providedIn: 'root'
})
export class AttachmentSecureService {
  constructor(
    private http: HttpClient,
    public sanitizer: DomSanitizer,
    private imageStorage: ImageStorageService,
    private secureDataUrlService: SecureDataUrlService,
    private assets: AssetsService,
    @Inject('API_URL_BASE') public apiUrlBase: string) {
  }

  transform(value: Attachment, notUseStorageKey: boolean = false, isFullPath = false, customCacheKey?: string, resizeToRect: DOMRect = null, useToken: boolean = false): Observable<any> {
    return new Observable<any>((observer) => {
      observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Loading));
      if(!value.extension)  value.extension = ExtensionType.file;
      const type = Object.keys(ExtensionType).find(key => value.extension.startsWith(ExtensionType[key]));
      switch (ExtensionType[type]) {
        case ExtensionType.image:
          let httpHeaders: HttpHeaders | { [header: string]: string | string[]; };
          let url: string;
          if (value.localFileURL) {
            observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(value.localFileURL));
            observer.complete();
          } else if (value.data) {
            observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(value.data));
            observer.complete();
          } else if (isFullPath) {
            url = value.url;
            httpHeaders = this.secureDataUrlService.createHttpHeaders(url, false, false, !notUseStorageKey, useToken);
          } else if (value.resourceUrl) {
            url = value.resourceUrl.replace('https://storage.googleapis.com/veniocrm-dev', 'https://veniocrm-dev.storage.googleapis.com');
            httpHeaders = this.secureDataUrlService.createHttpHeaders(url, false, false, !notUseStorageKey, useToken);
          } else if (value.url) {
            url = this.apiUrlBase + value.url;
            httpHeaders = this.secureDataUrlService.createHttpHeaders(url, false, true, !notUseStorageKey, useToken);
          } else {
            observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.NoPhoto));
          }
          const cacheKey = customCacheKey || url;
          const cachedImage = this.imageStorage.getBlobUrl(url);
          if (cachedImage) {
            observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(cachedImage));
            observer.complete();
            return { unsubscribe() { } };
          }
          this.http.get(url, {
            observe: 'response',
            responseType: 'blob',
            headers: httpHeaders
          }).pipe(flatMap((response: HttpResponse<Blob>) => {
            if (response.status === 205) {
              const redirectLocation = response.headers.get('location');
              return this.http.get(redirectLocation, {
                responseType: 'blob',
                headers: this.secureDataUrlService.createHttpHeaders(redirectLocation, false, false, true, useToken)
              });
            }
            return of(response.body);
          })).subscribe(response => {
            if (response.type.startsWith('image/heic')) {
              const blob: Blob = response;
              heic2any({
                blob,
                toType: 'image/jpeg',
                quality: 0.1,
              }).then((conversionResult) => {
                const urlData = URL.createObjectURL(conversionResult);
                this.imageStorage.addBlobUrl(cacheKey, urlData);
                if (resizeToRect) {
                  this.secureDataUrlService.resizeImage(response, resizeToRect, cacheKey, observer);
                } else {
                  observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(urlData));
                  observer.complete();
                }
              });
            } else {
              const urlData = URL.createObjectURL(response);
              this.imageStorage.addBlobUrl(cacheKey, urlData);
              if (resizeToRect) {
                this.secureDataUrlService.resizeImage(response, resizeToRect, cacheKey, observer);
              } else {
                observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(urlData));
                observer.complete();
              }
            }
          }, error => {
            console.log(error);
          });
          break;
        case ExtensionType.video:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Video));
          observer.complete();
          break;
        case ExtensionType.audio:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Audio));
          observer.complete();
          break;
        case ExtensionType.excelXLS:
        case ExtensionType.excelXLSX:
        case ExtensionType.excelXML:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Excel));
          observer.complete();
          break;
        case ExtensionType.docWord:
        case ExtensionType.docWork:
        case ExtensionType.docX:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Document));
          observer.complete();
          break;
        case ExtensionType.pdf:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Pdf));
          observer.complete();
          break;
        case ExtensionType.ppt:
        case ExtensionType.pptx:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.PPT));
          observer.complete();
          break;
        default:
          observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.UnSupport));
          observer.complete();
      }
      return { unsubscribe() { } };
    });
  }

  converDataURL(dataURL: string, type = TypeData.dataURL) {
    if (!dataURL) return
    switch (type) {
      case TypeData.base64: return dataURL.split(",")[1];
      case TypeData.dataURL: return dataURL;
    }
  }

  convertDataUrlToBlob(dataUrl): Blob {
    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
  }
}
