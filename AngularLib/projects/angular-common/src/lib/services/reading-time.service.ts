import { PhraseService } from './../services/phrase.service';
import { Injectable } from '@angular/core';
import { PhraseProject } from '../model/config';

@Injectable({
  providedIn: 'root'
})
export class ReadingTimeService {

  constructor(private phraseService : PhraseService) {
  }
  toDay(lang) {
    return this.phraseService.translate('timer_today', lang, PhraseProject.GofiveCore);
  }

  getSecondsUntilUpdate(seconds) {
    const min = 60;
    const hr = min * 60;
    const day = hr * 24;
    if (seconds < min) {
      // less than 1 min, update every 2 secs
      return 2;
    } else if (seconds < hr) {
      // less than an hour, update every 30 secs
      return 30;
    } else if (seconds < day) {
      // less then a day, update every 5 mins
      return 300;
    } else {
      // update every hour
      return 3600;
    }
  }

  getWeekNumber(d) {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    const yearStart: any = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    const weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    return weekNo;
  }
  public timeAgoSince(date: Date, lang?: string, now: Date = new Date(), isShortFormat?: string) {
    const second = ((now.getTime() - date.getTime()) / 1000);
    const minute = second / 60;
    const hour = minute / 60;
    const day = hour / 24;
    const week = day / 7;
    const month = day / 30;
    const year = day / 365;
    if (year <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(year)) + this.phraseService.translate('timer_years', lang, PhraseProject.GofiveCore) }
    if (year <= -1) { return this.phraseService.translate('timer_next_year', lang, PhraseProject.GofiveCore) }
    if (month <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(month)) + this.phraseService.translate('timer_months', lang, PhraseProject.GofiveCore) }
    if (month <= -1) { return this.phraseService.translate('timer_on_a_month', lang, PhraseProject.GofiveCore) }
    if (week <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(week)) + this.phraseService.translate('timer_weeks', lang, PhraseProject.GofiveCore) }
    if (week <= -1) { return this.phraseService.translate('timer_on_a_week', lang, PhraseProject.GofiveCore) }
    if (day <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(day)) + this.phraseService.translate('timer_days', lang, PhraseProject.GofiveCore) }
    if (day <= -1) { return this.phraseService.translate('timer_tomorrow', lang, PhraseProject.GofiveCore) }
    if (hour <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(hour)) + this.phraseService.translate('timer_hours', lang, PhraseProject.GofiveCore) }
    if (hour <= -1) { return this.phraseService.translate('timer_on_a_hour', lang, PhraseProject.GofiveCore) }
    if (minute <= -2) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(minute)) + this.phraseService.translate('timer_minutes', lang, PhraseProject.GofiveCore) }
    if (minute <= -1) { return this.phraseService.translate('timer_on_a_minute', lang, PhraseProject.GofiveCore) }
    if (second <= 0) { return this.phraseService.translate('timer_on', lang, PhraseProject.GofiveCore) + Math.abs(Math.floor(second)) + this.phraseService.translate('timer_seconds', lang, PhraseProject.GofiveCore) }

    if (year >= 2) { return Math.abs(Math.floor(year)) + this.phraseService.translate('timer_years_ago', lang, PhraseProject.GofiveCore) }
    if (year >= 1) { return this.phraseService.translate('timer_last_year', lang, PhraseProject.GofiveCore) }
    if (month >= 2) { return Math.abs(Math.floor(month)) + this.phraseService.translate('timer_months_ago', lang, PhraseProject.GofiveCore) }
    if (month >= 1) { return this.phraseService.translate('timer_last_month', lang, PhraseProject.GofiveCore) }
    if (week >= 2) { return Math.abs(Math.floor(week)) + this.phraseService.translate('timer_weeks_ago', lang, PhraseProject.GofiveCore) }
    if (week >= 1) { return this.phraseService.translate('timer_last_week', lang, PhraseProject.GofiveCore) }
    if (day >= 2) { return Math.abs(Math.floor(day)) + this.phraseService.translate('timer_days_ago', lang, PhraseProject.GofiveCore) }
    if (day >= 1) { return this.phraseService.translate('timer_yesterday', lang, PhraseProject.GofiveCore) }
    if (hour >= 2) { return Math.abs(Math.floor(hour)) + this.phraseService.translate('timer_hours_ago', lang, PhraseProject.GofiveCore) }
    if (hour >= 1) { return this.phraseService.translate('timer_an_hour_ago', lang, PhraseProject.GofiveCore) }
    if (minute >= 2) { return Math.abs(Math.floor(minute)) + this.phraseService.translate('timer_minutes_ago', lang, PhraseProject.GofiveCore) }
    if (minute >= 1) { return this.phraseService.translate('timer_a_minutes_ago', lang, PhraseProject.GofiveCore) }
    if (second >= 3) { return this.phraseService.translate('timer_a_seconds_ago', lang, PhraseProject.GofiveCore) }
    return this.phraseService.translate('timer_just_now', lang, PhraseProject.GofiveCore);
  }
}
