import { Injectable, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ImageStorageService implements OnDestroy {
  private urlToBlobUrls: Record<string, string> = {};
  private brokenUrls: string[] = [];
  private deadUrls: string[] = [];
  constructor(
    public sanitizer: DomSanitizer) {
  }
  ngOnDestroy(): void {
    for (const key in this.urlToBlobUrls) {
      const blob = this.urlToBlobUrls[key];
      URL.revokeObjectURL(blob);
      this.urlToBlobUrls[key] = null;
    }
  }

  getTrustResourceUrl(url: string) {
    const cached = this.getBlobUrl(url);
    if (cached) this.sanitizer.bypassSecurityTrustResourceUrl(cached);
    return null;
  }

  getBlobUrl(url: string) {
    return this.urlToBlobUrls[url];
  }

  addBlobUrl(url: string, blobUrl: string) {
    this.urlToBlobUrls[url] = blobUrl;
  }

  markAsBrokenUrl(url: string) {
    this.brokenUrls.push(url);
  }

  markAsDeadUrl(url: string) {
    this.deadUrls.push(url);
  }

  isMarkAsBrokenUrl(url: string) {
    return this.brokenUrls.indexOf(url) > -1;
  }

  isMarkAsDeadUrl(url: string) {
    return this.deadUrls.indexOf(url) > -1;
  }

  resolveBrokenUrl(url: string) {
    this.brokenUrls = this.brokenUrls.filter(e => e !== url);
  }

}
