import { Configuration } from './../model/config';
import { DialogComponent } from './../component/dialog/dialog.component';
import { HotToastService, ToastPosition, CreateHotToastRef, ToastOptions } from '@ngneat/hot-toast';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertData, AlertInfoModel } from '../model/alert-data';
export class AlertModel {
  title?: string;
  subtitle?: string;
  imageInfo?: string;
}
@Injectable()
export class AlertService {
  public toastRef: CreateHotToastRef<any>;
  private imageUrlPath: string = 'assets/images/dialog/{applicatiion}/{fileName}'
  public defualtSettingDialog: ToastOptions<AlertInfoModel> = {
    autoClose: false,
    dismissible: false,
    duration : 3000,
    className: 's-dialog',
    theme: <any>'gofive',
  }
  public defualtSettingToaste = {

  }
  public style = {
    'box-shadow': '0px 3px 6px rgba(26, 26, 26, 0.1)',
    'font-size': '16px',
    'border-radius': '6px',
    'width': '300px',
    'color': '#FFFFFF',
    'padding': '10px'
  };
  position: ToastPosition = 'top-right';
  primary = '#418AFD';
  public isOpen: boolean = false;
  constructor(private toast: HotToastService, private parent: Injector) {
    document.addEventListener('click', e => {
      const elToasts: HTMLElement = (e.target as HTMLElement).closest('.hot-toast-theme-gofive');
      const elDialog: HTMLElement = (e.target as HTMLElement).closest('.s-dialog');
      if ((!elToasts || !elDialog)) {
        this.close();
      }
    });
  }
  open(data: AlertInfoModel) {
    return new Observable<any>((observer) => {
      if (this.isOpen) { return }
      setTimeout(() => { this.isOpen = true }, 10)
      let toastOptions = this.getToastOptions(data);
      this.toastRef = this.toast.show<AlertInfoModel>(DialogComponent, toastOptions);
      this.toastRef.afterClosed.subscribe((e) => {
        this.isOpen = false;
        this.toastRef.updateToast({ theme: <any>'gofive-close' });
        observer.next(AlertData.getConfig());
        AlertData.setConfig({});
        observer.complete();
        observer.unsubscribe();
      });
    });
  }
  close() {
    if (this.isOpen) {
      this.isOpen = false;
      AlertData.setConfig({ cancel: true });
      this.toastRef?.close();
    }
  }
  getToastOptions(data : AlertInfoModel)  {
    let toastOptions : ToastOptions<AlertInfoModel> = this.defualtSettingDialog;
    toastOptions.data = data;
    toastOptions.data.imageUrl = this.getImageUrl(data?.imageUrl);
    return toastOptions;
  }
  getImageUrl(fileName: string) {
    return this.imageUrlPath.replace('{applicatiion}', Configuration.getConfig().application).replace('{fileName}', fileName);
  }
  error(msg = `This didn't work`) {
    this.toast.error(msg, {
      position: this.position,
      style: {
        ...this.style,
        background: '#DE3A45'
      },
      iconTheme: {
        primary: '#FFFFFF',
        secondary: '#DE3A45',
      },
    });
  }
  loading(msg = `Loading this file...`) {
    this.toast.loading(msg, {
      position: this.position,
      style: {
        ...this.style,
        background: this.primary
      },
      iconTheme: {
        primary: '#FFFFFF',
        secondary: this.primary,
      },
    });
  }
  success(msg = `Successfully toasted!`) {
    this.toast.success(msg, {
      position: this.position,
      style: {
        ...this.style,
        background: '#21CE9B'
      },
      iconTheme: {
        primary: '#FFFFFF',
        secondary: '#21CE9B',
      },
    });
  }
  warning(msg = `Please be cautious!`) {
    this.toast.warning(msg, {
      position: this.position,
      style: {
        ...this.style,
        background: '#FFC505'
      },
      iconTheme: {
        primary: '#FFFFFF',
        secondary: '#FFC505',
      },
    });
  }
  info(msg = `This is a info messege`) {
    this.toast.show(msg, {
      position: this.position,
      style: {
        ...this.style,
        background: this.primary
      },
      iconTheme: {
        primary: '#FFFFFF',
        secondary: this.primary,
      },
    });
  }



  badGateway(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-error.png',
      title: 'alert_dialog_title_bad_gateway',
      discription: 'alert_dialog_discription_bad_gateway',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  badRequest(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-400.png',
      title: 'alert_dialog_title_bad_request',
      discription: 'alert_dialog_discription_bad_request',
      confirmButtonText: 'alert_dialog_button_dismiss',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  serverError(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-500.png',
      title: 'alert_dialog_title_server_error',
      discription: 'alert_dialog_discription_server_error',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  serviceUnavailable(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-error.png',
      title: 'alert_dialog_title_service_unavailable',
      discription: 'alert_dialog_discription_service_unavailable',
      confirmButtonText: 'alert_dialog_button_dismiss',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  timeOut(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-error.png',
      title: 'alert_dialog_title_time_out',
      discription: 'alert_dialog_discription_time_out',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  unAuth(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'status-code-401.png',
      title: 'alert_dialog_title_un_auth',
      discription: 'alert_dialog_discription_un_auth',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_close',
    }
    data = { ...data, ...config };
    return this.open(data);
  }


  accept(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'approve.png',
      title: 'alert_dialog_title_accept',
      discription: 'alert_dialog_discription_accept',
      confirmButtonText: 'alert_dialog_button_accept',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }

  approve(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'approve.png',
      title: 'alert_dialog_title_approve',
      discription: 'alert_dialog_discription_approve',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }

  cancel(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'cancle.png',
      title: 'alert_dialog_title_cancel',
      discription: 'alert_dialog_discription_cancel',
      confirmButtonText: 'alert_dialog_button_confirm_cancel',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  changes(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'change.png',
      title: 'alert_dialog_title_changes',
      discription: 'alert_dialog_discription_changes',
      confirmButtonText: 'alert_dialog_button_changes',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  confirm(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'approve.png',
      title: 'alert_dialog_title_confirm',
      discription: 'alert_dialog_discription_confirm',
      confirmButtonText: 'alert_dialog_button_confirm',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  confirmClose(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'cancle.png',
      title: 'alert_dialog_title_confirm_close',
      discription: 'alert_dialog_discription_confirm_close',
      confirmButtonText: 'alert_dialog_button_confirm_close',
      cancelButtonText: 'alert_dialog_button_confirm_close_keep',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  deleteDialog(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'reject.png',
      title: 'alert_dialog_title_delete',
      discription: 'alert_dialog_discription_delete',
      confirmButtonText: 'alert_dialog_button_try_again',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  discardChanges(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'change.png',
      title: 'alert_dialog_title_discard_changes',
      discription: 'alert_dialog_discription_discard_changes',
      confirmButtonText: 'alert_dialog_button_yes',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  markComplete(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'approve.png',
      title: 'alert_dialog_title_mark_complete',
      discription: 'alert_dialog_discription_mark_complete',
      confirmButtonText: 'alert_dialog_button_mark_complete',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
  reject(config?: AlertInfoModel) {
    let data: AlertInfoModel = {
      imageUrl: 'reject.png',
      title: 'alert_dialog_title_reject',
      discription: 'alert_dialog_discription_reject',
      confirmButtonText: 'alert_dialog_button_reject',
      cancelButtonText: 'alert_dialog_button_cancel',
    }
    data = { ...data, ...config };
    return this.open(data);
  }
}
