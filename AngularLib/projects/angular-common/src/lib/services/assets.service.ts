import { Configuration, LibConfig } from './../model/config';
import { Inject, Injectable } from '@angular/core';
import { DEFAULT_ASSETS_PATH } from '../constansts/global.constansts';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {
  constructor(@Inject('LIB_CONFIG') private libConfig: LibConfig) {
    const production = libConfig?.production == undefined ? true : libConfig.production;
    Configuration.setLibConfig({ production: production });
    if (libConfig?.assetsEnvironment) {
      const environment = DEFAULT_ASSETS_PATH[libConfig?.assetsEnvironment] ? libConfig?.assetsEnvironment : 'prod';
      Configuration.setLibConfig({ assetsEnvironment: environment });
    }
  }
  transform(value: string) {
    if (Configuration.getConfig().assetsEnvironment) {
      const environment = Configuration.getConfig().assetsEnvironment;
      return DEFAULT_ASSETS_PATH[environment] + value;
    } else {
      // Waiting Change Venio Salesbear Empeo
      const url = Configuration.getConfig().production? DEFAULT_ASSETS_PATH.prod : DEFAULT_ASSETS_PATH.dev;
      return url + value;
    }

  }
}
