import { AssetsService } from './assets.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, NgZone } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { isNullOrUndefined } from '@syncfusion/ej2-base';
import heic2any from 'heic2any';
import { config, Observable, of, Subscriber } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { DEFAULT_PICTURE_PLACEHOLDER, SECURE_DATA_URL_TYPE } from '../constansts/global.constansts';
import { ImageStorageService } from '../services/image-storage.service';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { Configuration, LibConfig } from '../model/config';

@Injectable({
  providedIn: 'root'
})
export class SecureDataUrlService {
  static DEFAULT_RESIZE_OPTIONS = {
    aspectRatio: {
      keepAspectRatio: true,
      forceMinDimensions: true
    },
    exifOptions: {
      forceExifOrientation: false
    }
  };
  constructor(
    private http: HttpClient,
    public sanitizer: DomSanitizer,
    private imageStorage: ImageStorageService,
    private ngZone: NgZone,
    private picaService: NgxPicaService,
    private assets: AssetsService,
    @Inject('LIB_CONFIG') public libConfig: LibConfig
  ) {
    Configuration.setLibConfig(libConfig);
  }

  transform(url: string, type: string = 'attachment', isAllCompany: boolean = false, noCache = false, customCacheKey?: string, resizeToRect: DOMRect = null) {
    if (isNullOrUndefined(url)) { 
      return new Observable<any>((observer) => {
      observer.next(this.getEmptyState(type))
      observer.complete();
    })}

    const cacheKey = customCacheKey || url;
    if (type === SECURE_DATA_URL_TYPE.UserPicture || type === SECURE_DATA_URL_TYPE.Picture || type === SECURE_DATA_URL_TYPE.UserDataUrl) {
      url = Configuration.getConfig().apiUrlBase + url;
    } else if (type === SECURE_DATA_URL_TYPE.FullPathUser) {
      url = url;
    } else {
      url = url.replace('https://storage.googleapis.com/veniocrm-dev', 'https://veniocrm-dev.storage.googleapis.com');
    }
    const isDead = this.imageStorage.isMarkAsDeadUrl(cacheKey);
    const placeholder = type === SECURE_DATA_URL_TYPE.UserPicture || type === SECURE_DATA_URL_TYPE.FullPathUser || type === SECURE_DATA_URL_TYPE.UserDataUrl ? this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.User) : this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Loading);
    if (isDead) { return of(placeholder); }
    const isBroken = this.imageStorage.isMarkAsBrokenUrl(cacheKey);
    if (isBroken) { noCache = true; }
    return new Observable<any>((observer) => {
      observer.next(placeholder);
      const cachedImage = isBroken ? null : resizeToRect ? this.imageStorage.getBlobUrl('resized_' + cacheKey) : this.imageStorage.getBlobUrl(cacheKey);
      if (cachedImage && noCache === false) {
        if (type === SECURE_DATA_URL_TYPE.UserDataUrl) {
          url = cachedImage;
        } else {
          return observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(cachedImage));
        }
      }
      let httpHeaders: HttpHeaders | { [header: string]: string | string[]; };
      if (type == SECURE_DATA_URL_TYPE.UserPicture || type == SECURE_DATA_URL_TYPE.FullPathUser || type == SECURE_DATA_URL_TYPE.UserDataUrl) {
        httpHeaders = this.createHttpHeaders(url, noCache, true, !isAllCompany, true);
      } else {
        httpHeaders = this.createHttpHeaders(url, noCache, false, !isAllCompany);
      }

      this.http.get(url, {
        observe: 'response',
        responseType: 'blob',
        headers: httpHeaders
      })
        .pipe(
          mergeMap((response: HttpResponse<Blob>) => {
            if (response.status === 205) {
              const redirectLocation = response.headers.get('location');
              const useHeader = redirectLocation.indexOf('storage.googleapis') < 0 || redirectLocation?.toLowerCase().indexOf('venio') >= 0;
              const headers = useHeader ? this.createHttpHeaders(redirectLocation, noCache, false, true) : null;
              return this.http.get(redirectLocation, {
                responseType: 'blob',
                headers
              }).pipe(catchError((res: HttpResponse<Blob>) => {
                if (res.status === 0) {
                  return of(res.url);
                } else {
                  this.imageStorage.markAsDeadUrl(cacheKey);
                  return of(null);
                }
              }));
            } else if (response.status == 0) {
              return of(null);
            }
            this.imageStorage.addBlobUrl(cacheKey, url);
            return of(response.body);
          }),
          catchError((res: HttpResponse<Blob>) => {
            if (res.status == 0) {
              if (res.url.startsWith(Configuration.getConfig().apiUrlBase)) {
                this.imageStorage.markAsBrokenUrl(cacheKey);
                return of(res.url + '&t=' + Date.now());
              }
              return of(res.url);
            } else {
              this.imageStorage.markAsDeadUrl(cacheKey);
              return of(null);
            }
          })).subscribe(response => {
            this.imageStorage.resolveBrokenUrl(cacheKey);
            if (response instanceof Blob) {
              if (response.type.startsWith('image/heic')) {
                const blob: Blob = response;
                heic2any({
                  blob,
                  toType: 'image/jpeg',
                  quality: 0.1,
                }).then((conversionResult) => {
                  const urlData = URL.createObjectURL(conversionResult);
                  this.imageStorage.addBlobUrl(cacheKey, urlData);
                  if (type == SECURE_DATA_URL_TYPE.UserDataUrl) {
                    const file = new FileReader();
                    file.readAsDataURL(conversionResult as Blob);
                    file.onloadend = () => {
                      observer.next(file.result);
                      observer.complete();
                    };
                  } else if (resizeToRect) {
                    this.resizeImage(response, resizeToRect, cacheKey, observer);
                  } else {
                    observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(urlData));
                    observer.complete();
                  }
                });
              } else {
                const urlData = URL.createObjectURL(response);
                this.imageStorage.addBlobUrl(cacheKey, urlData);
                if (resizeToRect) {
                  this.resizeImage(response, resizeToRect, cacheKey, observer);
                } else if (type == SECURE_DATA_URL_TYPE.UserDataUrl) {
                  const reader = new FileReader();
                  reader.readAsDataURL(response);
                  reader.onloadend = () => {
                    observer.next(reader.result);
                    observer.complete();
                  };
                } else {
                  observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(urlData));
                  observer.complete();
                }
              }
            } else {
              const resourceUrl: string = response || placeholder;
              this.imageStorage.addBlobUrl(cacheKey, resourceUrl);
              observer.next(resourceUrl);
              observer.complete();
            }
            return {
              unsubscribe() { }
            };
          }, () => {
            this.imageStorage.markAsBrokenUrl(cacheKey);
            if (type != 'attachment') { observer.next(this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.User)); }
            observer.complete();
            observer.unsubscribe();
            return {
              unsubscribe() { }
            };
          });
    });
  }

  resizeImage(blob: Blob, resizeToRect: DOMRect, cacheKey, observer: Subscriber<any>) {
    const cachedImage = this.imageStorage.getBlobUrl('resized_' + cacheKey);
    if (cachedImage) {
      observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(cachedImage));
      observer.complete();
    } else {
      this.picaService
        .resizeImage(new File([blob], 'x.jpg'), resizeToRect.width, resizeToRect.height, this.resizeOptions())
        .toPromise()
        .then(res => {
          const urlData = URL.createObjectURL(res);
          this.imageStorage.addBlobUrl('resized_' + cacheKey, urlData);
          observer.next(this.sanitizer.bypassSecurityTrustResourceUrl(urlData));
          observer.complete();
        });
    }
  }

  createHttpHeaders(url: string, useNoCache = false, useCustomRedirect = false, useStorageKey = false, useToken = false): HttpHeaders | { [header: string]: string | string[]; } {
    if (!(useStorageKey || useCustomRedirect || useNoCache)) { return null; }
    const headers: HttpHeaders | { [header: string]: string | string[]; } = {};
    if (useNoCache) { headers['Cache-Control'] = 'no-cache'; }
    if (url.startsWith(Configuration.getConfig().apiUrlBase)) { headers['Cache-Control'] = 'max-age=0, must-revalidate'; }
    if (useCustomRedirect) { headers['x-custom-redirect'] = 'true'; }
    if (useStorageKey) {
      if (Configuration.getConfig().storageEncryptionKey && Configuration.getConfig().storageEncryptionKeyHash) {
        if(useToken) headers['Authorization'] = 'bearer ' + Configuration.getConfig().token;
        headers['x-goog-encryption-key'] = Configuration.getConfig().storageEncryptionKey;
        headers['x-goog-encryption-key-sha256'] = Configuration.getConfig().storageEncryptionKeyHash;
        headers['x-goog-encryption-algorithm'] = 'AES256';
      }
    }
    return headers;
  }

  getEmptyState(type) {
    switch (type) {
      case SECURE_DATA_URL_TYPE.Attachment:
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.venio);
      case SECURE_DATA_URL_TYPE.FullPathUser:
      case SECURE_DATA_URL_TYPE.UserPicture:
      case SECURE_DATA_URL_TYPE.Picture:
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.User);
      case SECURE_DATA_URL_TYPE.Product:
        return this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.product);
    }
  }

  resizeOptions() {
    return SecureDataUrlService.DEFAULT_RESIZE_OPTIONS;
  }

  convertDataUrlToBlob(dataUrl): Blob {
    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new Blob([u8arr], { type: mime });
  }
}
