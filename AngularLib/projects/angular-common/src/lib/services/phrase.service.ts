import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Configuration, PhraseProject } from '../model/config';
import { PHRASE_CACHE_KEY, LANGUAGE_KEY, PHRASE } from './../constansts/global.constansts';

@Injectable()
export class PhraseService {
  constructor(private httpClient: HttpClient, @Inject('PHRASE_PROJECT') private phraseProject: PhraseProject) {
    Configuration.configChange.subscribe(config => {
      if (config.phraseProject) {
        if (PhraseService.currentPhraseProject != config.phraseProject) {
          this.isPhraseReady$ = of(false);
        }
        this.isPhraseLoadeds = Object.keys(LANGUAGE_KEY).map(key => {
          const observer = new BehaviorSubject(false);
          this.loadLanguageResource(config.phraseProject, key, observer);
          return observer;
        });
        this.isPhraseReady$ = combineLatest(
          this.isPhraseLoadeds.map(s => s.asObservable())
        ).pipe(
          map(
            values => values.every(b => b)
          )
        );
      }
    });
    if (phraseProject) {
      Configuration.setLibConfig({ phraseProject: phraseProject });
    }
    Configuration.setLanguage(localStorage.getItem('user_prefered_language') || LANGUAGE_KEY.EN);
  }
  static currentPhraseProject: string = '';
  private phrases = {};
  private isPhraseLoadeds: BehaviorSubject<boolean>[];
  public isPhraseReady$: Observable<boolean>;
  private loadLanguageResource(project: PhraseProject, key: string, observer: BehaviorSubject<boolean>) {
    const lang = LANGUAGE_KEY[key];
    const resource = JSON.parse(localStorage.getItem(project + '_' + PHRASE_CACHE_KEY[lang]));
    const isUpToDated = this.isUpToDate(project);
    PhraseService.currentPhraseProject = project;
    if (!this.phrases[project]) {
      this.phrases[project] = {};
    }
    if (resource && isUpToDated) {
      this.phrases[project][lang] = resource;
      observer.next(true);
    } else {
      const cacheKey = new Date().toDateString();
      const phrase = PHRASE[project];
      const phraseUrl = `https://api.phrase.com/v2/projects/${phrase.phraseId}/locales/${key.toLowerCase()}/download?file_format=angular_translate`;
      const sub = this.httpClient.get<JSON>(phraseUrl + '&v=' + cacheKey, {
        headers: new HttpHeaders({ Authorization: phrase.phraseToken })
      }).subscribe(res => {
        this.phrases[project][lang] = res;
        localStorage.setItem(project + '_' + PHRASE_CACHE_KEY[lang], JSON.stringify(res));
        observer.next(true);
        sub.unsubscribe();
        localStorage.setItem(project + '_' + PHRASE_CACHE_KEY.LastUpdate, Date.now().toString());
        return {
          unsubscribe() {
          }
        };
      });
    }
  }

  private isUpToDate(project) {
    const now = Date.now();
    const storedAt = parseFloat(localStorage.getItem(project + '_' + PHRASE_CACHE_KEY.LastUpdate));
    return (now - storedAt) < PHRASE_CACHE_KEY.ExpiredIn;
  }

  public translate(key: string, lang?: string, project = PhraseService.currentPhraseProject): string {
    key = (key || '').toLowerCase();
    if (this.isPhraseReady$) {
      if (this.phrases[project] && this.phrases[project][lang][key]) {
        return this.phrases[project][lang][key];
      }
      Object.keys(this.phrases).forEach(pro => {
        if (project != pro && this.phrases[pro] && this.phrases[pro][lang][key]) {
          return this.phrases[pro][lang][key];
        }
      });
    }
    return key;
  }


  public transform(key: string, project = PhraseService.currentPhraseProject) {
    return combineLatest([Configuration.languageChange, this.isPhraseReady$]).pipe(
      map(
        res => {
          return this.translate(key, res[0]);
        }
      )
    );
  }
}
