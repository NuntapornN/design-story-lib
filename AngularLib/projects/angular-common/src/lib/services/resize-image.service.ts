import { Attachment } from "../model/venio.model";

export function resizeImage(file: File | Blob, maxWidth: number, maxHeight: number): Promise<Blob> {
  return new Promise((resolve, reject) => {
    let image = new Image();
    image.src = URL.createObjectURL(file);
    image.onload = () => {
      let width = image.width;
      let height = image.height;
      if (width <= maxWidth && height <= maxHeight) {
        resolve(file);
      }
      let newWidth;
      let newHeight;
      if (width > height) {
        newHeight = height * (maxWidth / width);
        newWidth = maxWidth;
      } else {
        newWidth = width * (maxHeight / height);
        newHeight = maxHeight;
      }
      let canvas = document.createElement('canvas');
      canvas.width = newWidth;
      canvas.height = newHeight;
      let context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, newWidth, newHeight);
      canvas.toBlob(resolve, file.type);

    };
    image.onerror = (event) => { reject(event) }
  });
}

export function readFileInfo(data: Blob | File, filename: string, attachmentType : number): Promise<Attachment> {
  return new Promise<Attachment>((resolve, reject) => {
    const reader: FileReader = new FileReader();
    reader.onload = () => {
      let att: Attachment = new Attachment();
      att.filename = filename;
      att.data = reader.result as string;
      att.fileSize = data.size;
      att.extension = data.type;
      att.isActive = true;
      if (attachmentType) att.type = attachmentType;
      resolve(att);
    }
    reader.onerror = (event) => { reject(event) }
    reader.readAsDataURL(data);
  });
}


