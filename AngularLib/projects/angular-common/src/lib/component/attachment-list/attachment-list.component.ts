import { Attachment } from './../../model/venio.model';
import { Component, ContentChild, Input, OnChanges, SimpleChanges, TemplateRef, Output, EventEmitter, ViewEncapsulation, OnInit } from '@angular/core';
import { AttachmentExtension } from '../../enum/AttachmentTypes';

export enum VIEW_TYPE {
  ListView,
  ListBlock
}

@Component({
  selector: 's-attachment-list',
  templateUrl: './attachment-list.component.html',
  styleUrls: ['./attachment-list.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class AttachmentListComponent implements OnInit, OnChanges {
  @Input() attachments: Attachment[] = [];
  @Input() isAllCompany: boolean = false;
  @Input() isFullPath: boolean = false;
  @Input() type: string = VIEW_TYPE[VIEW_TYPE.ListView];
  @Input() listToggle: number = 3;
  @Input() cssClass = '';
  @Input() fields = { url: 'url', name: 'filename', date: 'dateCreated' };
  @Input() toggleList = 0;
  @Input() filterExtension = 'All';
  @Output() viewAttachment = new EventEmitter<any>();
  @ContentChild('valueTemplate', { static: true, read: TemplateRef }) valueTemplate: TemplateRef<any>;
  get VIEW_TYPE() { return VIEW_TYPE }
  public files: Attachment[] = [];
  public viewType = VIEW_TYPE[this.type];
  constructor() { }
  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['type']?.currentValue) {
      this.viewType = VIEW_TYPE[changes['type']?.currentValue] || VIEW_TYPE.ListView;
    }
    if (changes['filterExtension']?.currentValue || changes['attachments']?.currentValue) {
      this.setFilterExtension()
    }
  }
  openClick(item: Attachment) {
    this.viewAttachment.emit({
      current: item,
      attachments: this.attachments,
      attachmentsFilter : this.files
    });
  }
  setFilterExtension() {
    switch (this.filterExtension) {
      case 'Media': {
        this.files = (this.attachments || []).filter(file => {
          return file.extension.startsWith(AttachmentExtension.image)
        });
        break;
      }
      case 'File': {
        this.files = (this.attachments || []).filter(file => {
          return !file.extension.startsWith(AttachmentExtension.image);
        });
        break;
      }
      default : {
        this.files = this.attachments || [];
      }
    }
  }


}
