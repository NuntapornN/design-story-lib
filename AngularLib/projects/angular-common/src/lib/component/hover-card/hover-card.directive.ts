import { isNullOrUndefined } from '@syncfusion/ej2-base';
import { Directive, HostListener, Input, ComponentRef, ComponentFactoryResolver, ElementRef, ApplicationRef, ViewContainerRef, Injector, EmbeddedViewRef } from '@angular/core';
import { HoverCardComponent } from './hover-card.component';
enum CardType {
  employee = 'employee',
  customer = 'customer'
}
@Directive({
  selector: '[sHoverCard]'
})
export class HoverCardDirective {
  @Input('sHoverCard') id: any;
  @Input('sUserCard') userId: any;
  @Input() type: CardType = CardType.employee;
  @Input('openClick') click = false;
  @Input() delay = 0;

  private component: ComponentRef<HoverCardComponent>;
  private show;
  constructor(private el: ElementRef, private appRef: ApplicationRef, private injector: Injector,
    private cfr: ComponentFactoryResolver, private vcr: ViewContainerRef) {
  }
  // @HostListener('mouseenter', ['$event']) onMouseEnter(event) {
  //   if(isNullOrUndefined(this.id || this.userId)) return
  //   this.type == 'employee'? this.app.hoverEmployee(this.id || this.userId,event) : this.app.hoverCustomer(this.id,event);
  // }

  // @HostListener('click') onClick(event) {
  //   if(isNullOrUndefined(this.id || this.userId)) return
  //   if(!this.click) return
  //   this.type == 'employee'? this.app.openEmployeeURL(this.id || this.userId) : this.app.openCustomerURL(this.id);
  // }

  // @HostListener('mouseleave') onMouseLeave() {
  //   this.app.hoverTooltipOut();
  // }
  // @HostListener('mouseenter', ['$event']) onMouseEnter(event) {
  //   this.show = setTimeout(() => {
  //     if (!this.component && (this.id || this.userId)) {
  //       // --- create component and append to body
  //       this.component = this.cfr.resolveComponentFactory(HoverCardComponent).create(this.injector);
  //       this.appRef.attachView(this.component.hostView);

  //       const domElem = (this.component.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
  //       document.body.appendChild(domElem);

  //       // // --- create component and append to ViewContainerRef
  //       // const cmpFactory = this.cfr.resolveComponentFactory(TooltipComponent);
  //       // this.component = this.vcr.createComponent(cmpFactory);

  //       this.component.instance.toastShow(this.userId, event, this.type);

  //       let observer = new MutationObserver((mutations) => {
  //         if (!document.body.contains(this.el.nativeElement)) {
  //           this.hide();
  //           observer.disconnect();
  //           observer = null;
  //         }
  //       });
  //       observer.observe(document.body, { childList: true, subtree: true });
  //     }
  //   }, 500);
  // }

  // @HostListener('mouseleave') onMouseLeave() {
  //   clearTimeout(this.show);
  //   this.hide();
  // }

  hide() {
    if (this.component) {
      window.setTimeout(() => {
        this.component.instance.toastHide();
        this.appRef.detachView(this.component.hostView);
        this.component.destroy();
        this.component = null;
      }, this.delay);
    }
  }

}
