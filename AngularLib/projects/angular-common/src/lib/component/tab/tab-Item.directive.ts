import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { HeaderTabOption } from './tab.component';

@Directive({
  selector: 's-item'
})
export class TabItemDirective {
  @ContentChild('header', { static: true, read: TemplateRef }) headerTemplate: TemplateRef<any>;
  @ContentChild('content', { static: true, read: TemplateRef }) valueTemplate: TemplateRef<any>;
  @Input() header: HeaderTabOption;
  hide : boolean;
  id : string;
  enableTab : boolean = true;
  created : boolean = false;
  constructor() { }

}
