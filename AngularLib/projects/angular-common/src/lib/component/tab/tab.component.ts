import { TabItemDirective } from './tab-Item.directive';
import { debounceTime } from 'rxjs/operators';
import { TabItemsDirective } from './tab-Items.directive';
import { Subject } from 'rxjs';
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, ContentChildren, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, QueryList, ViewChild } from '@angular/core';

export class SelectEventArgs {
  cancel?: boolean;
  event?: Event
  previousIndex?: number;
  previousItem?: TabItemDirective;
  selectedIndex?: number;
  selectedItem?: TabItemDirective;
  items?: TabItemDirective[];
}
export class HeaderTabOption {
  text: string;
  id: string;
  content: string;
}
@Component({
  selector: 's-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit, AfterViewInit, AfterContentChecked {
  @ContentChildren(TabItemsDirective) childItems: QueryList<TabItemsDirective>;
  @ViewChild('headerTab') private headerTab: ElementRef;
  @Input() cssClass: string;
  @Input() width: string;
  @Input() height: string;
  @Input() reloadTab: boolean = false;
  @Input() selectedItem: number = 0;
  @Input() timing = '250ms ease-in';
  @Output() selecting = new EventEmitter();
  @Output() selected = new EventEmitter<SelectEventArgs>();
  @Output() created = new EventEmitter();

  public tabItems: TabItemDirective[] = [];
  public tabContent: TabItemDirective[] = [];
  public currentSelect: SelectEventArgs = new SelectEventArgs();
  public select = new SelectEventArgs();
  public showPrevBtn: boolean = true;
  public showNextBtn: boolean = true;
  private resize$ = new Subject<void>();

  constructor(private ref: ChangeDetectorRef) {}

  ngOnInit() {
    this.resize$.pipe(debounceTime(100)).subscribe(event => {
      let target = this.headerTab.nativeElement;
      if (target.offsetWidth >= target.scrollWidth) {
        this.showPrevBtn = false;
        this.showNextBtn = false;
      } else {
        this.onScroll();
      }
    })
  }
  
  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  ngAfterViewInit(): void {
    this.onScroll();
    if (this.childItems.first.childItems) {
      this.tabItems = this.childItems.first.childItems.toArray();
      this.onCreated();
      this.onSelectIndex(this.selectedItem);
      this.ref.detectChanges();
      this.resize$.next();
    }
  }
  onSelectIndex(index: number) {
    if (this.tabItems[index]) {
      this.onSelect(this.tabItems[index], index)
    }
  }

  hideTab(index: number, value: boolean = false) {
    if (this.tabItems[index]) { this.tabItems[index].hide = value }
  }

  enableTab(index: number, value: boolean = true) {
    if (this.tabItems[index]) { this.tabItems[index].enableTab = value }
  }

  onCreated() {
    this.created.emit();
  }

  onSelect(item: TabItemDirective, index, event?) {
    item.id = 'tab_item_directive_' + index;
    if (item.enableTab && index != this.currentSelect.selectedIndex) {
      if (this.reloadTab) {
        this.tabContent = [];
        this.tabContent.push(item);
      }
      else {
        if (!this.tabContent.some(f => f.id == item.id)) {
          this.tabContent.push(item);
        }
      }
      let selected: SelectEventArgs = {
        event: event,
        previousItem: this.currentSelect.selectedItem,
        previousIndex: this.currentSelect.selectedIndex,
        selectedItem: item,
        selectedIndex: index,
        items: this.tabItems,
      }
      this.currentSelect = selected;
      if (this.reloadTab && this.currentSelect.previousItem) { this.currentSelect.previousItem.created = false }
      this.selected.emit(selected);
    }
  }

  onPreviousNext(type) {
    let target = this.headerTab.nativeElement;
    const nextCcrollTo = (target.scrollWidth / this.tabItems.length) * 2;
    target.scrollTo({
      left: type == 'next' ? target.scrollLeft + nextCcrollTo : target.scrollLeft - nextCcrollTo,
      behavior: 'smooth'
    });
  }

  onScroll(event?) {
    let target = this.headerTab.nativeElement;
    this.showPrevBtn = (target.scrollLeft > 0);
    this.showNextBtn = (target.scrollLeft < target.scrollWidth - target.offsetWidth);
  }

  @HostListener('window:resize', ['$event'])
  sizeChange(event) {
    this.resize$.next(event);
  }
}

