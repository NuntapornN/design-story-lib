import { ContentChildren, Directive, QueryList } from '@angular/core';
import { TabItemDirective } from './tab-Item.directive';

@Directive({
  selector: 's-tabItems',
})

export class TabItemsDirective {
  @ContentChildren(TabItemDirective) childItems: QueryList<TabItemDirective>;
  constructor() { }
}
