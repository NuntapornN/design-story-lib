import { AssetsService } from './../../services/assets.service';
import { Component, Input, ViewChild, ViewEncapsulation, OnInit, OnDestroy, Inject, HostListener, SecurityContext } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SecureDataUrlPipe } from './../../pipe/secure-data-url.pipe';
import { DefaultPipe } from './../../pipe/default.pipe';
import { ExtensionFileType } from '../../enum/AttachmentTypes';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CloudAttachment } from './../../model/cloud-attachment';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ActivatedRoute } from '@angular/router';
import { PhraseService } from '../../services/phrase.service';
import { FileSaver } from 'file-saver';
import * as JSZip from 'jszip';
import { Configuration, LibConfig } from '../../model/config';

export enum KEY_CODE {
  UP_DOWN = 40,
  RIGHT_ARROW = 39,
  UP_ARROW = 38,
  LEFT_ARROW = 37,
  ECS = 27
}

export enum AttMode {
  Attachment = 'Attachment',
  Vedio = 'Vedio'
}

@Component({
  selector: 's-attachment-modal',
  templateUrl: './attachment-modal.component.html',
  styleUrls: ['./attachment-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DefaultPipe, SecureDataUrlPipe, DatePipe]
})
export class AttachmentModalComponent implements OnInit, OnDestroy {
  get ExtensionFileType() { return ExtensionFileType; }
  get Mode() { return AttMode; }
  constructor(
    public secureDataUrl: SecureDataUrlPipe,
    public phraseService: PhraseService,
    private httpClient: HttpClient,
    private sanitizer: DomSanitizer,
    public datePipe: DatePipe,
    private _Activatedroute: ActivatedRoute,
    private assets: AssetsService,
    @Inject('LIB_CONFIG') libConfig: LibConfig
  ) {
    Configuration.setLibConfig(libConfig);
  }

  get allowDownload() { return !(this._Activatedroute.snapshot.paramMap.get('token') || this._Activatedroute.snapshot.paramMap.get('access_token')); }
  @Input() isAllCompany = false;
  @Input() isAnnoucement = false;
  @ViewChild('modalDialog') public modalDialog: DialogComponent;
  @Input() mode: string = AttMode.Attachment;

  public animationSettings = { effect: 'Fade' };
  public hide: any;
  public isOpen = false;
  public srcAttachment = '';
  public allAttachment: CloudAttachment[] = [];
  public nextAttachment: CloudAttachment = null;
  public currentAttachment: CloudAttachment = null;
  public currentIndex = 0;
  public maxIndex: number = null;
  public currentType: number;
  public currentExtention: string = null;
  public loading = true;
  public openLoad = true;
  private nameFile: string = null;
  public description: string = null;
  public loadGCS = false;
  public loadMedia = true;
  public error = false;
  public showOption = true;
  public localURL = false;
  public url: any;
  public listDownload = [];
  public listItems = [
    { id: 'download', text: 'common_download' },
    { id: 'downloadAll', text: 'common_download_all' }
  ];

  public dataDownload = null;

  @HostListener('window:keyup', ['$event'])
  KeyUp(event: KeyboardEvent) {
    if (!this.isOpen) { return; }
    const code = event.which || event.keyCode;
    if (code === KEY_CODE.RIGHT_ARROW || code === KEY_CODE.UP_ARROW) { this.next(); }
    if (code === KEY_CODE.LEFT_ARROW || code === KEY_CODE.UP_DOWN) { this.previous(); }
    if (code === KEY_CODE.ECS) { this.close(); }
  }
  ngOnInit() {
  }

  ngOnDestroy() { }

  setConfig(config: LibConfig) {
    Configuration.setLibConfig(config);
  }
  public modalVideoUrl(url: string) {
    if (url) {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      this.showOption = false;
      this.isOpen = true;
      this.mode = AttMode.Vedio;
      this.loading = true;
    }

  }
  public modalBtnClickUrl(url: string, description: string = '', localize = false) {
    this.localURL = true;
    this.srcAttachment = url;
    this.allAttachment = [];
    this.description = localize ? this.phraseService.translate(description) : description;
    this.isOpen = true;
    this.openLoad = this.loading = false;
  }


  onCreated() {
    this.modalDialog.show();
  }

  iframeLoaded() {
    this.loading = false;
  }

  public async modalBtnClickAtt(att, all, type?: string) {
    this.mode = AttMode.Attachment;
    this.showOption = true;
    this.isOpen = true;
    this.error = false;
    this.listDownload = [];
    if (type === 'xlsx') {
      window.open(Configuration.getConfig().apiUrlBase + att.url, '_blank');
      return;
    }

    if (all && att) {
      this.loading = true;
      this.allAttachment = all.map(a => new CloudAttachment(a, this.httpClient, this.sanitizer, this.assets));
      if (this.allAttachment.length > 1) {
        this.listDownload = this.listItems;
        this.currentIndex = this.allAttachment.findIndex(i => att.url ? i.url === att.url : i.resourceUrl === att.resourceUrl);
        this.maxIndex = this.allAttachment.length - 1;
      } else {
        this.listDownload.push(this.listItems[0]);
        this.currentIndex = 0;
      }
      this.currentAttachment = this.allAttachment[this.currentIndex];
    } else {
      this.currentAttachment = new CloudAttachment(att, this.httpClient, this.sanitizer, this.assets);
      this.currentIndex = 0;
      this.allAttachment = [];
      this.listDownload.push(this.listItems[0]);
      this.allAttachment.push(this.currentAttachment);
    }
    this.currentAttachment.download(this.isAllCompany);
    this.currentType = this.getCurrentType(att.extension);
    this.loading = false;
  }

  // Close the Dialog, while clicking "OK" Button of Dialog
  public dlgButtonClick = () => {
    this.close();
  }

  public overlayClick(e) {
    const target = e.target as HTMLElement;
    if (target.classList.contains('preview') || target.classList.contains('close-icon')) {
      this.close();
    }
  }

  close() {
    this.modalDialog.hide();
    this.modalDialog.destroy();
    this.isOpen = false;
    this.localURL = false;
    this.showOption = true;
    this.url = null;
  }



  previous() {
    this.currentIndex - 1 < 0 ? this.selectByIndex(this.maxIndex) : this.selectByIndex(this.currentIndex - 1);
  }

  next() {
    this.currentIndex + 1 > this.maxIndex ? this.selectByIndex(0) : this.selectByIndex(this.currentIndex + 1);
  }

  selectByIndex(index: number) {
    this.error = false;
    if (index !== this.currentIndex && this.allAttachment && this.allAttachment.length > 1) {
      this.currentAttachment = this.allAttachment[index];
      this.currentAttachment.download(this.isAllCompany);
      this.srcAttachment = this.currentAttachment.resourceUrl ? this.currentAttachment.resourceUrl : this.currentAttachment.url;
      this.currentIndex = index;
      this.currentType = this.getCurrentType(this.currentAttachment.extension);
    }
  }

  getPDFFile(file: SafeResourceUrl) {
    return this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, file);
  }

  selectDownload(event, currentFile) {
    // console.log("selectDownload");
    switch (event.selectedItem.id) {
      case 'download': this.onDownload(currentFile); break;
      case 'downloadAll': this.onDownloadAll(); break;
    }
  }

  onDownload(currentFile: SafeResourceUrl) {
    // console.log('onDownload');
    const url = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, currentFile);
    const link = document.createElement('a');
    link.href = url;
    link.download = this.currentAttachment.filename;
    link.click();
  }

  async onDownloadAll() {
    // console.log('onDownloadAll');
    const zip = new JSZip();
    const dataURL = [];
    const promises = this.allAttachment.map(async att => {
      await att.downloadData(this.isAllCompany).toPromise().then((response: HttpResponse<Blob>) => {
        dataURL.push(response.body);
        zip.file(att.filename, response.body, { base64: true });
      }).catch(error => {
        console.log(error);
      });
    });
    await Promise.all(promises);
    if (dataURL.length > 0) {
      zip.generateAsync({ type: 'blob' })
        .then(content => {
          FileSaver.saveAs(content, 'Attachment-' + this.datePipe.transform(new Date(), 'dd-MM-yyyy'));
        });
    }
  }

  getCurrentType(type: string) {
    if (type.search('image') !== -1) {
      return ExtensionFileType.Image;
      // } else if (type.search('pdf') !== -1) {
      //   return ExtensionFileType.PDF;
    } else if (type.search('audio') !== -1) {
      return ExtensionFileType.Audio;
    } else if (type.search('video') !== -1) {
      return ExtensionFileType.Video;
    } else {
      return ExtensionFileType.Other;
    }
  }

  createNamefile() {
    const name = 'Venio-image';
    return name;
  }
  errorMedia(event) {
    this.error = true;
  }

  loadedMedia(event) {
    this.error = false;
  }

}


