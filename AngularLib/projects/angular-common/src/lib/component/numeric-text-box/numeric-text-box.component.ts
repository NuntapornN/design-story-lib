import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewEncapsulation, forwardRef } from '@angular/core';

import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 's-numeric',
  templateUrl: './numeric-text-box.component.html',
  styleUrls: ['./numeric-text-box.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumericTextBoxComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class NumericTextBoxComponent implements OnInit {
  @Input() value = 0;
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter<number>();

  @Input() size = 'large';
  @Input() max: number;
  @Input() min: number;
  @Input() step = 1;

  public isDisabled = false;

  constructor(private _element: ElementRef, private _renderer: Renderer2) { }

  ngOnInit(): void {
  }
  writeValue(value: any): void {
    if (value) {
      this.value = value;
    }
  }
  _onChange = (val) => { };
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  _onTouched = () => { };
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  changeValue() {
    this._onChange(this.value);
    this._onTouched();
  }

  decease() {
    this.value -= this.step;
    if (this.value < this.min) {
      this.value = this.min;
    }
    this.ngModelChange.emit(this.value);
  }
  increase() {
    this.value += this.step;
    if (this.value > this.max) {
      this.value = this.max;
    }
    this.ngModelChange.emit(this.value);
  }
}
