import { Component, ContentChild, ElementRef, EventEmitter, forwardRef, Input, OnChanges,Output, SimpleChanges, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 's-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TagsComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class TagsComponent implements OnChanges , ControlValueAccessor{
  @ContentChild('valueTemplate', { static: true, read: TemplateRef })
  valueTemplate: TemplateRef<any>;
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef })
  toggleTemplate: TemplateRef<any>;
  @ContentChild('customToggleTemplate', { static: true, read: TemplateRef })
  customToggleTemplate: TemplateRef<any>;
  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;
  @Input() field = { text : 'text' , value : 'value' };
  @Input() value : any[] = [];
  @Input() showRemoveButton: boolean = true;
  @Input() placeholder = 'Enter a new tag';
  @Input() iconToggle = 'gfc-add-tag';
  @Input() iconRemove = 'gfc-close';
  @Input() visibleToggle = true;
  @Input() cssClass = '';
  @Input() sliceTag = 20;
  @Output() valueChange = new EventEmitter<any[]>();

  public inputVisible = false;
  public inputValue = '';
  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.value?.currentValue) {
      this.value = this.value || [];
    }
  }
  _onChange = (val) => {};
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  _onTouched = (val) => {};
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  writeValue(value: any): void {
    if (value) {
      this.value = value;
    }
  }

  handleClose(removedTag: {}): void {
    this.value = this.value.filter(tag => tag !== removedTag);
    this.onEmitValue();
  }

  sliceTagName(tag: string): string {
    if(tag) {
      const isLongTag = tag?.length > this.sliceTag;
      return isLongTag ? `${tag?.slice(0, this.sliceTag)}...` : tag;
    }
  }

  showInput(): void {
    this.inputVisible = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  handleInputConfirm(): void {
    if (this.inputValue && this.value?.indexOf(this.inputValue) === -1) {
      let tag : { [key: string] : any }= {};
      tag[this.field.text] = this.inputValue;
      let value = [...this.value, tag];
      this.setValue(value);
    }
    this.inputValue = '';
    this.inputVisible = false;
  }

  handleKeyBackspace() {
    if (this.inputValue == '' && this.value.length > 0) {
      this.value.splice(-1, 1);
      this.onEmitValue();
    }
  }

  setValue(value : any[]) {
    this.value = value;
    this.onEmitValue();
  }

  getValue() {
    return this.value;
  }

  onEmitValue(){
    this._onChange(this.getValue());
    this.valueChange.emit(this.getValue());
  }
}
