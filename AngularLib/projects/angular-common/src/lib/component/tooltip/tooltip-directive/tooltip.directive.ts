import { Directive, Input, TemplateRef, ElementRef, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ApplicationRef, Injector, EmbeddedViewRef, HostListener } from '@angular/core';
import { TooltipComponent } from '../tooltip.component';
import { Subject } from 'rxjs';

@Directive({
  selector: '[sTooltip],[sTooltipTemp]'
})
export class TooltipDirective {
  @Input('sTooltip') tooltipText: string;
  @Input('sTooltipTemp') tooltipTemp: TemplateRef<any>;
  @Input() placement = 'top';
  @Input() delay = 0;
  @Input() opacity = 0.8;
  @Input() color = 'white';
  @Input() bgColor = 'black';

  private component: ComponentRef<TooltipComponent>;
  private _show;
  constructor(private el: ElementRef, private _appRef: ApplicationRef, private injector: Injector,
    private cfr: ComponentFactoryResolver, private vcr: ViewContainerRef) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this._show = setTimeout(() => {
      if (!this.component && (this.tooltipText || this.tooltipTemp)) {
        // --- create component and append to body
        this.component = this.cfr.resolveComponentFactory(TooltipComponent).create(this.injector);
        this._appRef.attachView(this.component.hostView);

        const domElem = (this.component.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);
        
        // // --- create component and append to ViewContainerRef
        // const cmpFactory = this.cfr.resolveComponentFactory(TooltipComponent);
        // this.component = this.vcr.createComponent(cmpFactory);
        
        this.component.instance.tooltipTemplate = this.tooltipTemp;
        this.component.instance.tooltipText = this.tooltipText;
        this.component.instance.placement = this.placement;
        this.component.instance.el = this.el;
        this.component.instance.delay = this.delay;
        this.component.instance.opacity = this.opacity;
        this.component.instance.bgColor = this.bgColor;
        this.component.instance.color = this.color;
        this.component.instance.show();
        
        var observer = new MutationObserver((mutations)=> {
          if (!document.body.contains(this.el.nativeElement)) {
            this.hide();
            observer.disconnect();
            observer = null;
          }
        });
        observer.observe(document.body, { childList: true, subtree: true });
      }
    }, 500);
  }

  @HostListener('mouseleave') onMouseLeave() {
    clearTimeout(this._show);
    this.hide();
  }

  hide(){
    if (this.component) {
      window.setTimeout(() => {
        this.component.instance.hide();
        this._appRef.detachView(this.component.hostView);
        this.component.destroy();
        this.component = null;
      }, this.delay);
    }
  }
}
