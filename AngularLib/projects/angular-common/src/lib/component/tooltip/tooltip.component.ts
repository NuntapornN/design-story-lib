import { Component, OnInit, ContentChild, TemplateRef, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 's-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  tooltipTemplate: TemplateRef<any>;
  placement: string;
  tooltipText: string;
  el: ElementRef;
  delay:number;
  opacity:number;
  bgColor:string;
  color:string;
  
  offset = 10;
  tooltipStyle = {};
  tooltipAfterStyle = {};

  @ViewChild('sTooltip')
  sTooltip: ElementRef;

  constructor() { }

  ngOnInit(): void {
    this.tooltipStyle = { 
      'transition': `opacity ${this.delay}ms`,
      'background': this.bgColor,
      'color': this.color
    };
    this.tooltipAfterStyle = { 'border-color': 'transparent' };
    this.tooltipAfterStyle[`border-${this.placement}-color`] = this.bgColor;
  }

  show() {
    window.setTimeout(() => {
    this.setPosition();
    }, 1);
  }

  hide() {
    this.tooltipStyle['opacity'] = 0;
  }

  setPosition() {
    const hostPos = this.el.nativeElement.getBoundingClientRect();

    const tooltipPos = this.sTooltip.nativeElement.getBoundingClientRect();

    const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    let top, left;
    if (this.placement === 'top') {
      top = hostPos.top - tooltipPos.height - this.offset;
      left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
    }

    if (this.placement === 'bottom') {
      top = hostPos.bottom + this.offset;
      left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
    }

    if (this.placement === 'left') {
      top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
      left = hostPos.left - tooltipPos.width - this.offset;
    }

    if (this.placement === 'right') {
      top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
      left = hostPos.right + this.offset;
    }
    this.tooltipStyle = { 'top.px': top + scrollPos, 'left.px': left };
    this.tooltipStyle['opacity'] = this.opacity;
  }
}
