import { Component, ViewChild, ElementRef, ContentChild, TemplateRef, Input } from '@angular/core';

@Component({
  selector: 's-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss'],
})
export class CollapseComponent {
  @ViewChild('collapseToggle')
  collapseToggle: ElementRef;
  @ViewChild('collapseTarget')
  collapseTarget: ElementRef;
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef })
  toggleTemplate: TemplateRef<any>;
  @ContentChild('contentTemplate', { static: true, read: TemplateRef })
  contentTemplate: TemplateRef<any>;
  @Input() cssClass: string;
  @Input() clickToggleOnOpen: boolean = true;
  @Input() showArrow: boolean = true;
  public isOpen = false;
  constructor() {}

  toggleClass(canClick = true) {
    if (!canClick) return;
    if (!this.collapseTarget) return;
    this.isOpen = !this.isOpen;
    let element = <HTMLElement>this.collapseTarget.nativeElement;
    if (this.isOpen) {
      element.style.maxHeight = element.scrollHeight + 'px';
      element.classList.add('active');
    } else {
      element.style.maxHeight = 0 + 'px';
      element.classList.remove('active');
    }
  }
}
