import { AlertInfoModel } from './../../model/alert-data';
import { Component, Inject, Optional, ViewEncapsulation, ElementRef } from '@angular/core';
import { HotToastRef } from '@ngneat/hot-toast';
import { AlertData } from '../../model/alert-data';
import { BaseComponent } from '../../model/base';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DialogComponent extends BaseComponent {
  public modal: AlertInfoModel;
  public remark: string;
  constructor(@Optional() @Inject(HotToastRef) public toastRef: HotToastRef<AlertInfoModel>, private element: ElementRef) {
    super(element);
    this.modal = new AlertInfoModel(toastRef?.data);
    this.modal.requiredRemark = false;
  }
  confirm() {
    AlertData.setConfig({ confirm: true, remark: this.remark })
    this.toastRef.close();
  }
  cancel() {
    AlertData.setConfig({ cancel: true })
    this.toastRef.close();
  }
  disable(){
    if(this.remark) return false;
    return this.modal?.requiredRemark;
  }
}
