import { DomSanitizer } from '@angular/platform-browser';
import { Attachment } from '../../model/venio.model';
import { Component, ContentChild, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges, TemplateRef, AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import heic2any from 'heic2any';
import { readFileInfo, resizeImage } from '../../services/resize-image.service';
import { ExtensionType } from '../../services/attachment-secure.service';
import { Mode, SupportType } from './upload-file.enum';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { convertDataUrlToBlob } from '../../services/convert-to-blob.service';

@Component({
  selector: 's-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadFileComponent),
      multi: true
    }
  ],
})
export class UploadFileComponent implements OnChanges, AfterViewInit {
  @ContentChild('valueTemplate', { static: true, read: TemplateRef }) valueTemplate: TemplateRef<any>;
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef }) toggleTemplate: TemplateRef<any>;
  @Input() value: Attachment[] = [];
  @Input() allowExtensions: string = '*'; // Mode FileType || 
  @Input() supportType: SupportType[] = [SupportType.All]; // Mode FileType
  @Input() mode = Mode.FileList;
  @Input() max = 20;
  @Input() maxFileSize: number = 26214400;
  @Input() resizeWidth: number = 854;
  @Input() resizeHeight: number = 854;
  @Input() cssClass: string = '';
  @Input() iconAddClass: string = 'fa fa-plus-circle';
  @Input() iconRemoveClass: string = 'fa fa-times';
  @Input() placeholder: string;
  @Input() requiredStorageKey: boolean = true;
  @Input() isFullPath: boolean = false;
  @Input() multiple: boolean = true;
  @Input() disabled: boolean = false;
  @Input() showRemoveFile: boolean = true;
  @Input() isAutoResize: boolean = true;
  @Input() attachmentType: number;
  @Output() valueChange = new EventEmitter<any>();
  @Output() created: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() error: EventEmitter<any[]> = new EventEmitter<any[]>();
  private errorData: { [key: string]: Object }[] = [];
  private accept: string[] = [];
  public loading: boolean = false;
  public counter = Array;
  @Input() filesUpload = [
    { id: SupportType.Photo, name: 'common_photo', icon: 'gfc-camera-filled', accept: "image/*, .heic", isActive: true },
    { id: SupportType.Video, name: 'common_video', icon: 'gfc-vdo-filled', pathIcon: 2, accept: "video/*", isActive: true },
    { id: SupportType.Audio, name: 'common_voice', icon: 'gfc-voice-filled', accept: "audio/*", isActive: true },
    { id: SupportType.Attachment, name: 'common_attachment', icon: 'gfc-attachment-filled', accept: "*", isActive: true },
  ];
  get ExtensionType() { return ExtensionType }
  get Mode() { return Mode }
  get disabledMaxFile() { return this.max <= this.value.length }
  constructor(public sanitizer: DomSanitizer, private http: HttpClient) {
    this.accept = this.allowExtensions.split(/[ ,]+/);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.value) {
      this.setValue(changes.value?.currentValue || []);
    }
    if (changes.allowExtensions) {
      this.allowExtensions = changes.allowExtensions?.currentValue || '*';
      this.accept = this.allowExtensions.split(/[ ,]+/);
    }
    if (changes.supportType?.currentValue) {
      this.onChangeFileSupport();
    }
  }
  ngAfterViewInit(): void {
    this.created.emit(true);
  }
  // NG_VALUE_ACCESSOR
  _onChange = (val) => { };
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  _onTouched = (val) => { };
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  writeValue(value: any): void {
    if (value) { this.value = value; }
  }
  // ------------------------

  async handleFiles(event: File[] = []) {
    if (this.disabled) return
    if (this.disabledMaxFile) { this.setError({ max: true }); this.error.emit(this.errorData); return }
    this.loading = true;
    let selectedFiles: File[] = Array.from(event);
    let filteredFiles: File[] = this.filterOnlyValidFiles(selectedFiles);
    let optimizedFiles = await this.makeAttachmentsFrom(filteredFiles);
    this.value = this.value.concat(optimizedFiles);
    this.loading = false;
    this.valueChange.emit({ value: this.value })
    this._onChange({ value: this.value });
  }

  async makeAttachmentsFrom(files: File[]): Promise<Attachment[]> {
    let attachments: Attachment[] = [];
    const process = files.map(async file => {
      let attachment: Attachment;
      let filename = file.name;
      let fileFormatType = file.name.substr((file.name.lastIndexOf('.')));
      let fileExtension = file.type;
      let blob: Blob = file;

      // Convert Heic To .png
      let isHeicOrHeif: boolean = /image\/hei(c|f)/.test(fileExtension) || /.\hei(c|f)/.test(fileFormatType);
      if (isHeicOrHeif) {
        filename = file.name.substr(0, (file.name.lastIndexOf('.'))) + '.jpeg';
        await heic2any({ blob, toType: 'image/jpeg',quality : 0.5 }).then(res => { blob = <Blob>res }).catch(error => { console.log(error) });
      }

       // Image Resive File
      let isImageFile: boolean = fileExtension.startsWith(ExtensionType.image);
      if (isImageFile && this.isAutoResize) {
        await resizeImage(blob, this.resizeWidth, this.resizeHeight).then(res => { blob = res }).catch(error => { console.log(error) });
      }

      // Mark File to Attachment
      await readFileInfo(blob, filename, this.attachmentType).then(res => attachment = res).catch(error => { console.log(error) });
      if (attachment) attachments.push(attachment);
    });

    await Promise.all(process);
    return attachments;
  }

  filterOnlyValidFiles(files: File[]): File[] {
    return files.filter(file => {
      let isValidSize: boolean = this.validateFileSize(file);
      let isValidType: boolean = this.validateFileType(file);
      return isValidSize && isValidType
    }).slice(0, this.max - this.value?.length);
  }

  validateFileSize(file: File): boolean {
    let isValidSize: boolean = file.size <= this.maxFileSize;
    return isValidSize;
  }

  validateFileType(file: File): boolean {
    // ex. image01.jpeg --> .jpeg
    let fileFormatType = file.name.substr((file.name.lastIndexOf('.')));
    // ex. image/png
    let fileExtension = file.type;

    // Input accept
    let isAcceptAll: boolean = this.accept.includes('*')
    let isCorrectType: boolean = this.accept.includes(fileExtension) || this.accept.includes(fileFormatType)
    let isAcceptAllImage: boolean = this.accept.includes('image/*')
    let isAcceptAllAudio: boolean = this.accept.includes('audio/*')
    let isAcceptAllVideo: boolean = this.accept.includes('video/*')

    // Check file extension
    let isImageFile: boolean = fileExtension.startsWith(ExtensionType.image)
    let isAudioFile: boolean = fileExtension.startsWith(ExtensionType.audio)
    let isVideoFile: boolean = fileExtension.startsWith(ExtensionType.video)

    let isValidType = (
      isAcceptAll ||
      isCorrectType ||
      (isAcceptAllImage && isImageFile) ||
      (isAcceptAllAudio && isAudioFile) ||
      (isAcceptAllVideo && isVideoFile)
    )
    return isValidType;
  }

  remove(item: Attachment) {
    if (!item) return
    item.isActive = false;
    setTimeout(() => {
      this.value = this.value.filter(att => att.isActive);
      this.valueChange.emit({ value: this.value })
      this._onChange({ value: this.value });
    }, 200)
  }

  getTypeAttachment(data) {
    return Object.keys(ExtensionType).find(key => data.extension.startsWith(ExtensionType[key]));
  }

  onChangeFileSupport() {
    this.allowExtensions = '';
    this.filesUpload = this.filesUpload.map(type => {
      type.isActive = this.supportType.includes(SupportType.All) || this.supportType.includes(SupportType[type.id]);
      if (type.isActive) this.allowExtensions += type.accept + ',';
      return type;
    });
    this.accept = this.allowExtensions.split(/[ ,]+/);
  }

  reset() {
    return this.value = [];
  }
  setValue(value: Attachment[]) {
    value.map(attachment => { attachment.isActive = true; })
    this.value = value;
  }
  getValue() {
    return this.value;
  }
  onInputFileClick(event) {
    event.target.value = null
  }
  setError(error) {
    this.errorData.push(error);
  }
  async uploadFile(item: Attachment[], fields: string, headers: HttpHeaders) {
    const process = item.map(async (file, i) => {
      let blob = convertDataUrlToBlob(file.data);
      await this.uploadGCP(file[fields], blob, file.extension, headers).toPromise().then(res => {
        file['status'] = true;
      }).catch(error => {
        file['error'] = error;
        file['status'] = false;
      });
    });
    await Promise.all(process);
    return item;
  }

  public uploadGCP<T>(url: string, blob: Blob, type: string, headers: HttpHeaders) {
    headers['Content-Type'] = type;
    const httpOptions = { headers: headers };
    return this.http.put<any>(url, blob, httpOptions);
  }
}

