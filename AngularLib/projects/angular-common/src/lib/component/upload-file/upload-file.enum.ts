export enum Mode {
    FileType = 'FileType',
    FileList = 'FileList',
    Custom = 'Custom',
}

export enum SupportType {
    All = 'All',
    Photo = 'Photo',
    Video = 'Video',
    Audio = 'Audio',
    Attachment = 'Attachment'
}

