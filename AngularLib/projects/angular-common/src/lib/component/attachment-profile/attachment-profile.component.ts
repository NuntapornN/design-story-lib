import { AttachmentSecureService } from './../../services/attachment-secure.service';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Attachment } from '../../model/venio.model';
import { TypeData } from '../../services/attachment-secure.service';
import { resizeImage } from '../../services/resize-image.service';
@Component({
  selector: 's-attachment-profile',
  templateUrl: './attachment-profile.component.html',
  styleUrls: ['./attachment-profile.component.scss']
})
export class AttachmentProfileComponent implements OnInit {
  @ViewChild('inputFile') private inputFile: ElementRef;
  @Input() cssClass = '';
  @Input() disabled = false;
  @Input() resizeWidth = 250;
  @Input() resizeHeight = 250;
  @Input() attachmentTypes: number;
  @Input() notUseStorageKey: boolean = false;
  @Input() isFullPath: boolean = false;
  @Output() picture = new EventEmitter<Attachment>();
  @Input() attachment: Attachment;
  @Input() typeData = TypeData.base64;
  public allowExtensions: string = ".png, .jpg, .jpeg";

  constructor(private attachmentSecure: AttachmentSecureService) { }

  ngOnInit() {
  }
  onOpenClick(event) {
    this.inputFile?.nativeElement.click();
  }
  onInputFileClick(event) {
    event.target.value = null
  }
  async onFileChange(event) {
    let file = (<HTMLInputElement>event.target).files[0];
    if (file) {
      await resizeImage(file, this.resizeWidth, this.resizeHeight).then(async blob => {
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onload = () => {
          let att: Attachment = new Attachment();
          att.filename = file.name;
          att.data = reader.result as string
          att.fileSize = file.size;
          att.extension = file.type;
          att.type = this.attachmentTypes;
          this.attachment = att;
          this.onPictureEmit(att);
        }
      }, error => {
        console.log(error)
      });
    }
  }


  async onPictureEmit(attachment: Attachment) {
    let att = new Attachment();
    att.filename = attachment.filename;
    att.data = await this.attachmentSecure.converDataURL(attachment.data, this.typeData);
    att.fileSize = attachment.fileSize;
    att.extension = attachment.extension;
    att.type = this.attachmentTypes;
    this.picture.emit(att);
  }

  public setProfile(att: Attachment) {
    att.extension = att.extension || 'image/'
    this.attachment = att;
  }

  public resetProfile() {
    this.attachment = null;
  }



}
