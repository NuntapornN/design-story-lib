import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Output, Input, EventEmitter, ContentChild, TemplateRef, ViewChild, SimpleChanges, ElementRef, OnChanges, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Attachment } from '../../model/venio.model';
import { convertDataUrlToBlob } from '../../services/convert-to-blob.service';
import { resizeImage } from '../../services/resize-image.service';

export class  AttachmentModel extends Attachment{
  public file?: File;
  public extension: string;
  public fileSize: number;
  public isActive: boolean = true;
  public error: number;
  public status: boolean;
  public localfile: boolean;
}

export enum AttachmentType {
  Image,
  Audio,
  Video,
  Excel,
  DOC,
  PPT,
  PDF,
  Default
}

export const DEFAULT_OPTION_VALUE = {
  iconAdd: 'fa fa-plus-circle',
  iconRemove: 'fa fa-times',
  text: 'Add Attachment',
}

export class OptionsStyle {
  image?: object = {};
  text?: string;
  iconAdd?: string;
  iconRemove?: string;
  inputAdd?: object = {};
  border?: string;
}
export class UploaderItemModel {
  url?: string;
  name?: string;
  extension?: string;
}
@Component({
  selector: 's-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UploaderComponent implements OnChanges {
  @Input() allowExtensions: string = '.png, image/*';
  @Input() fields: UploaderItemModel = <UploaderItemModel><unknown>{ url: 'data', name: 'filename', extension: 'extension' };
  @Input() placeholder = '';
  @Input() min: number = 0;
  @Input() max: number = 20;
  @Input() showToggleAdd: boolean = true;
  @Input() visibleView: boolean = true;
  @Input() optionsStyle: OptionsStyle = {};
  @Input() minFileSize: number = 0;
  @Input() maxFileSize: number = 25000000;
  @Input() resizeWidth = 854;
  @Input() resizeHeight = 854;
  @Input() multiple: boolean = true;
  @Input() attachments: AttachmentModel[] = [];
  @Input() showRemoveFile: boolean = true;
  @Input() value = [];
  @Input() dataReverse = true;
  @Input() ngModel;
  @Input() optionViewImage: any;
  @Input() notUseStorageKey: boolean = false;
  @Input() isFullPath: boolean = false;
  @Output() ngModelChange: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() error: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() created: EventEmitter<boolean> = new EventEmitter<boolean>();
  // template
  @ContentChild('valueTemplate', { static: true, read: TemplateRef })
  valueTemplate: TemplateRef<any>;
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef })
  toggleTemplate: TemplateRef<any>;
  @ViewChild('inputFile') private inputFile: ElementRef;
  private errorData: { [key: string]: Object }[] = [];
  public onload: boolean = false;
  get AttachmentType() { return AttachmentType }
  constructor(public sanitizer: DomSanitizer, private http: HttpClient) {
    this.setStyle();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['optionsStyle']?.currentValue) {
      this.setStyle();
    }
    if (changes['attachments']?.currentValue) {
      this.setValue(changes['attachments']?.currentValue);
    }
  }
  ngAfterViewInit(): void {
    this.created.emit(true);
  }
  openFile() {
    this.inputFile.nativeElement.click();
  }
  validatorFileSize(files: File[] = []) {
    let fileTemp = files.filter(file => file.size <= this.maxFileSize);
    if (fileTemp.length != files.length) this.setError({ maxSize: true });
    return fileTemp;
  }
  validatorFileMax(files: File[] = []) {
    if (this.max && this.max > 0) {
      if (this.attachments.length + files.length > this.max) this.setError({ max: true })
      const count = this.max - this.attachments.length;
      return files.slice(0, count);
    }
    return files;
  }
  async validatorFileInput(files: File[]) {
    files = await this.validatorFileSize(files);
    files = await this.validatorFileMax(files);
    return files
  }
  setValue(attachments: AttachmentModel[] = []) {
    this.attachments = attachments.map(it => {
      let att: AttachmentModel = new AttachmentModel();
      att = { ...it, ...att }
      att.isActive = true;
      return att;
    });
    this.valueChange.emit({
      value: attachments,
      error: null,
    })
  }
  resetValue() {
    this.attachments = [];
  }
  readFileInfo(file: File, data?: Blob) {
    return new Promise<AttachmentModel>((resolve, reject) => {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        let att: AttachmentModel = new AttachmentModel();
        att.file = file;
        att.filename = file.name;
        att.data = reader.result as string;
        att.fileSize = data?.size || file?.size;
        att.extension = file.type;
        att.localfile = true;
        resolve(att);
      }
      reader.onerror = (event) => {
        reject(event)
      }
      if (data) reader.readAsDataURL(data)
      else reader.readAsDataURL(file)
    });
  }
  async onFileChange(event) {
    this.resetError();
    this.onload = true;
    let temp: File[] = Array.from((<HTMLInputElement>event.target).files);
    let files = await this.validatorFileInput(temp);
    let attachments: AttachmentModel[] = [];
    const process = await files.map(async (file, i) => {
      if (file.type.startsWith('image/')) {
        await resizeImage(file, this.resizeWidth, this.resizeHeight).then(async blob => {
          await this.readFileInfo(file, blob).then(res => {
            attachments.push(res);
          });
        });
      } else {
        await this.readFileInfo(file).then(res => {
          attachments.push(res);
        })
      }
    });
    await Promise.all(process);
    this.onload = false;
    this.attachments = this.attachments.concat(attachments);
    this.valueChange.emit({
      value: this.attachments,
      error: this.errorData,
    })
  }
  onInputFileClick(event) {
    if (event) event.target.value = null
  }
  setError(error) {
    this.errorData.push(error);
  }
  resetError() {
    this.errorData = [];
  }
  remove(item: AttachmentModel) {
    if (!item) return
    item.isActive = false;
    setTimeout(() => {
      this.attachments = this.attachments.filter(att => att.isActive);
      this.valueChange.emit({
        value: this.attachments,
        error: null,
      });
    }, 200)
  }
  getTypeAttachment(item: AttachmentModel) {
    if (item.extension.startsWith('image/')) {
      return AttachmentType.Image;
    } else if (item.extension.startsWith('audio/')) {
      return AttachmentType.Audio;
    } else if (item.extension.startsWith('video/')) {
      return AttachmentType.Video;
    } else if (item.extension.startsWith('application/pdf')) {
      return AttachmentType.PDF;
    } else if (item.extension.startsWith('application/vnd.ms-excel')) {
      return AttachmentType.Excel;
    } else if (item.extension.startsWith('application/vnd.ms-powerpoint')) {
      return AttachmentType.PPT;
    } else if (item.extension.startsWith('application/vnd.ms-works') || item.extension.startsWith('application/msword')) {
      return AttachmentType.DOC;
    } else {
      return AttachmentType.Default;
    }
  }
  setStyle() {
    this.optionsStyle = { ...DEFAULT_OPTION_VALUE, ...this.optionsStyle, };
  }

  async uploadFile(item: AttachmentModel[], fields: string, headers: HttpHeaders) {
    const process = await item.map(async (file, i) => {
      let blob = convertDataUrlToBlob(file.data);
      await this.uploadGCP(file[fields], blob, file.extension, headers).toPromise().then(res => {
        file.status = true;
      }).catch(error => {
        file.status = false;
      })
    });
    await Promise.all(process);
    return item;
  }

  public uploadGCP<T>(url: string, blob: Blob, type: string, headers: HttpHeaders) {
    headers['Content-Type'] = type;
    const httpOptions = { headers: headers };
    return this.http.put<any>(url, blob, httpOptions);
  }
}
