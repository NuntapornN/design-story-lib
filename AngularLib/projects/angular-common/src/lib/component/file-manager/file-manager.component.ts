import { element } from 'protractor';
import { UploaderComponent } from './../uploader/uploader.component';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { Attachment } from '../../model/venio.model';


export enum SUPPORT_TYPE {
  All = 'All',
  Photo = 'Photo',
  Video = 'Video',
  Audio = 'Audio',
  Attachment = 'Attachment'
}

@Component({
  selector: 's-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FileManagerComponent implements OnInit, OnChanges {
  @Input() max = 20;
  @Input() maxFileSize = 26214400;
  @Input() supportType: SUPPORT_TYPE[];
  @Input() attachments: Attachment[] = [];
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('uploader') uploader: UploaderComponent;
  counter = Array;
  public selectType;
  public filesUpload = [
    { id: SUPPORT_TYPE.Photo, name: 'common_photo', icon: 'gfc-camera-filled', accept: "image/*", isActive: true },
    { id: SUPPORT_TYPE.Video, name: 'common_video', icon: 'gfc-vdo-filled', pathIcon: 2, accept: "video/*", isActive: true },
    { id: SUPPORT_TYPE.Audio, name: 'common_voice', icon: 'gfc-voice-filled', accept: "audio/*", isActive: true },
    { id: SUPPORT_TYPE.Attachment, name: 'common_attachment', icon: 'gfc-attachment-filled', accept: "*", isActive: true },
  ];
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.supportType?.currentValue) {
      this.onChangeFileSupport();
    }
  }

  onChangeFileSupport() {
    this.filesUpload = this.filesUpload.map(type => {
      type.isActive = this.supportType.includes(SUPPORT_TYPE.All) || this.supportType.includes(SUPPORT_TYPE[type.id]);
      return type;
    });
  }

  onSelectType(item) {
    this.selectType = item;
    var input = document.createElement('input');
    input.type = "file";
    input.setAttribute('accept',this.selectType.accept);
    input.setAttribute('multiple','true');
    setTimeout(function () {input.click();}, 200);
    input.addEventListener('change', (event: Event) => {
      this.onFileChange(event);
    });
    input.addEventListener('click', (event: Event) => {
      this.onInputFileClick(event);
    });
  }

  onFileChange(event) {
    this.uploader?.onFileChange(event);
  }

  setAttachment(event) {
    this.valueChange.emit(event);
  }

  onInputFileClick(event) {
    event.target.value = null
  }
}
