import { Component, ContentChild, Input, OnInit, TemplateRef, ViewEncapsulation, Output, EventEmitter, ViewChild, SimpleChanges, OnChanges } from '@angular/core';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import startOfYear from 'date-fns/startOfYear';
import endOfYear from 'date-fns/endOfYear';
import startOfQuarter from 'date-fns/startOfQuarter';
import endOfQuarter from 'date-fns/endOfQuarter';
import startOfMonth from 'date-fns/startOfMonth'
import endOfMonth from 'date-fns/endOfMonth'
import startOfDay from 'date-fns/startOfDay';
import endOfDay from 'date-fns/endOfDay';
import setQuarter from 'date-fns/setQuarter'
import getYear from 'date-fns/getYear'
import getQuarter from 'date-fns/getQuarter'
import { PhraseService } from '../../services/phrase.service';

export enum IntervalTypes {
  Date = 1,
  Month = 2,
  Quarter = 3,
  Year = 4,
}

export enum CalendarMode {
  Date = 'date',
  Month = 'month',
  Year = 'year',
}

export class DateRange {
  start : Date;
  end : Date;
}

class FilterType {
  keyName: string;
  type: IntervalTypes;
  calendarMode: CalendarMode;
}


@Component({
  selector: 's-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DateRangePickerComponent implements OnInit, OnChanges {
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef }) toggleTemplate: TemplateRef<any>;
  @ContentChild('filterTemplate', { static: true, read: TemplateRef }) filterTemplate: TemplateRef<any>;
  @ViewChild('dateStartPicker') dateStartPicker: NzDatePickerComponent;
  @ViewChild('dateEndPicker') dateEndPicker: NzDatePickerComponent;
  @Input() placement = ['bottomLeft', 'bottom', 'bottomRight'];
  @Input() dateRange: DateRange = new DateRange();
  @Output() change = new EventEmitter<any>();
  public intervalTypes: FilterType[] = [
    { keyName: 'common_day', type: IntervalTypes.Date, calendarMode: CalendarMode.Date },
    { keyName: 'common_month', type: IntervalTypes.Month, calendarMode: CalendarMode.Month },
    { keyName: 'common_quarter', type: IntervalTypes.Quarter, calendarMode: CalendarMode.Year },
    { keyName: 'common_year', type: IntervalTypes.Year, calendarMode: CalendarMode.Year }
  ];
  public intervalType: FilterType;
  get IntervalTypes() { return IntervalTypes }
  public dateStart = new Date();
  public dateEnd = new Date();
  public date = null;
  public rangeDate = null;
  public visible: boolean = false;
  public quarters = [1, 2, 3, 4]
  public isPhraseReady;
  constructor(phraseService: PhraseService) {
    phraseService.isPhraseReady$.subscribe(res => { this.isPhraseReady = res; });
    this.setDefaultSetting();
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.dateRange?.currentValue) {
      this.dateStart = this.dateRange.start;
      this.dateEnd = this.dateRange.end;
      this.onSelectInterval();
    }
  }

  public setDefaultSetting() {
    this.intervalType = this.intervalTypes.find(type => type.type == IntervalTypes.Date);
    this.onSelectInterval();
  }
  onSelectInterval() {
    switch (this.intervalType.type) {
      case IntervalTypes.Date:
        this.dateStart = startOfDay(this.dateStart);
        this.dateEnd = endOfDay(this.dateEnd);
        break;
      case IntervalTypes.Month:
        this.dateStart = startOfMonth(this.dateStart);
        this.dateEnd = endOfMonth(this.dateEnd);
        break;
      case IntervalTypes.Quarter:
        this.dateStart = startOfQuarter(this.dateStart);
        this.dateEnd = endOfQuarter(this.dateEnd);
        break;
      case IntervalTypes.Year:
        this.dateStart = startOfYear(this.dateStart);
        this.dateEnd = endOfYear(this.dateEnd);
        break;
    }
    this.dateRange.start = this.dateStart;
    this.dateRange.end = this.dateEnd;
  }
  onSelectStartQuarter(quarter: number) {
    this.dateRange.start = this.dateStart = setQuarter(this.dateStart, quarter);
  }
  public currentEndQuarter;
  onSelectEndQuarter(quarter: number) {
    this.dateRange.end = this.dateEnd = setQuarter(this.dateEnd, quarter);
  }
  onCancel(): void {
    this.visible = false;
  }
  onSubmit() {
    let dateRange : DateRange = {
      start : startOfDay(this.dateStart),
      end : endOfDay(this.dateEnd)
    }
    this.change.emit({
      value : dateRange
    });
    this.visible = false;
  }

  getActiveQuarter(type, quarter) {
    switch (type) {
      case 'start':
        return getYear(this.dateStart) < getYear(this.dateEnd) || (getYear(this.dateStart) == getYear(this.dateEnd) && (quarter) * 3 <= this.dateEnd.getMonth() + 1);
      case 'end':
        return getYear(this.dateStart) < getYear(this.dateEnd) || getYear(this.dateStart) == getYear(this.dateEnd) && (quarter - 1) * 3 >= this.dateStart.getMonth();
    }
  }

  getSelectQuarter(type, quarter) {
    switch (type) {
      case 'start':
        return quarter == getQuarter(this.dateStart);
      case 'end':
        return quarter == getQuarter(this.dateEnd);
    }
  }

  onSelectedDateStart(date) {
    this.setDisabledDateEnd();
    switch (this.intervalType.type) {
      case IntervalTypes.Date:
        this.dateRange.start = startOfDay(date);
        break;
      case IntervalTypes.Month:
       this.dateRange.start = startOfMonth(date);
        break;
      case IntervalTypes.Quarter:
       this.dateRange.start = startOfQuarter(date);
        break;
      case IntervalTypes.Year:
       this.dateRange.start = startOfYear(date);
        break;
    }
  }

  onSelectedDateEnd(date) {
    this.setDisabledDateStart();
    switch (this.intervalType.type) {
      case IntervalTypes.Date:
        this.dateRange.end = endOfDay(date);
        break;
      case IntervalTypes.Month:
        this.dateRange.end = endOfMonth(date);
        break;
      case IntervalTypes.Quarter:
        this.dateRange.end = endOfQuarter(date);
        break;
      case IntervalTypes.Year:
        this.dateRange.end = endOfYear(date);
        break;
    }
  }


  setDisabledDateEnd() {
    const tempDate = this.dateEnd;
    this.dateEnd = null;
    setTimeout(() => {
      if (tempDate) this.dateEnd = endOfDay(tempDate);
    }, 100);
  }
  setDisabledDateStart() {
    const tempDate = this.dateStart;
    this.dateStart = null;
    setTimeout(() => {
      if (tempDate) this.dateStart = startOfDay(tempDate);
    }, 100);
  }
  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !(this.dateRange.end)) { return false; }
    return startValue.getTime() > this.dateRange.end.getTime();
  }
  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !(this.dateRange.start)) { return false; }
    return endValue.getTime() < this.dateRange.start.getTime();
  };
}
