import { Component, ContentChild, ElementRef, EventEmitter, forwardRef, Input, OnChanges, OnDestroy, OnInit, Output, Renderer2, SimpleChanges, TemplateRef, ViewChild, AfterViewInit, ViewContainerRef, ViewEncapsulation, EmbeddedViewRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as Rx from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

export enum DropType {
  Dropdown,
  Dropup
}
export enum Mode {
  single = 'single',
  multi = 'multi',
  btn = 'btn'
}
export class DropdownItemModel {
  text = 'text';
  value = 'value';
  subText = '';
  pictureSrc = '';
  children: { name: string, field: DropdownItemModel };
}
export class DropdownTreeModel {
  parent: any[];
  current: any[];
  children: any[];
}
export class SDropdownEventArgs {
  value: any[];
  selectValue?;
  selectedItem?;
  isSelect?;
  selectedItems: any[];
  removedItems?: any[];
}
@Component({
  selector: 's-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class DropdownComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit, ControlValueAccessor {

  constructor(private _element: ElementRef, private _renderer: Renderer2, public viewContainerRef: ViewContainerRef) { }

  get DropType() { return DropType; }
  get Mode() { return Mode; }

  @ViewChild('sDropdown')
  public dropdown: ElementRef;
  @ViewChild('searchEle')
  public searchEle: ElementRef;

  @ViewChild('options')
  public options: ElementRef;
  @ViewChild('optionsPanel', { static: true, read: TemplateRef })
  optionsPanel: TemplateRef<any>;
  optionsEmbeddedViewRef: EmbeddedViewRef<any>;

  @ViewChild('options2nd')
  public options2nd: ElementRef;
  @ViewChild('options2ndPanel', { static: true, read: TemplateRef })
  options2ndPanel: TemplateRef<any>;
  options2ndEmbeddedViewRef: EmbeddedViewRef<any>;

  // template
  @ContentChild('valueTemplate', { static: true, read: TemplateRef })
  valueTemplate: TemplateRef<any>;
  @ContentChild('itemTemplate', { static: true, read: TemplateRef })
  itemTemplate: TemplateRef<any>;
  @ContentChild('toggleTemplate', { static: true, read: TemplateRef })
  toggleTemplate: TemplateRef<any>;
  @ContentChild('footerTemplate', { static: true, read: TemplateRef })
  footerTemplate: TemplateRef<any>;
  @ContentChild('itemSelectedTemplate', { static: true, read: TemplateRef })
  itemSelectedTemplate: TemplateRef<any>;

  @Input() value = [];
  @Input() ngModel;
  @Output() ngModelChange: EventEmitter<any[]> = new EventEmitter<any[]>();

  @Output() selected = new EventEmitter<SDropdownEventArgs>();
  @Output() change = new EventEmitter<SDropdownEventArgs>();
  @Output() removed = new EventEmitter<SDropdownEventArgs>();
  @Output() removedAll = new EventEmitter<SDropdownEventArgs>();
  @Output() filtering = new EventEmitter();
  @Output() addBtnClick = new EventEmitter();
  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() created = new EventEmitter();

  @Input() filterEventObj = { cancel: false, text: '', updateData: (data) => this.updateData(data) };
  @Input() openEventObj = { cancel: false };
  @Input() closeEventObj = { cancel: false };

  @Input() allowFiltering: boolean;
  @Input() allowAddBtn = false;
  @Input() position = DropType.Dropdown;
  @Input() placeholder = '';
  @Input() selectThis = 'Select this';
  @Input() filterPlaceholder = 'Search';
  @Input() popupHeight = '300px';
  @Input() popupWidth_px: number;
  @Input() cssClass = 'sDropdown-outline'; // sDropdown-default, sDropdown-primary, sDropdown-outline
  @Input() fields: DropdownItemModel = { text: 'text', value: 'value' } as unknown as DropdownItemModel;
  @Input() selectedItem = [];
  @Input() dataSource: any[];
  @Input() header = ' ';
  @Input() isFilterDropdown;
  @Input() showClearButton = true;
  @Input() showTooltip: boolean;
  @Input() mode = Mode.multi;
  @Input() max: number;
  @Input() isSelectedDropdown = false;
  @Input() allowSelected = false;
  @Input() headerSelected = '';
  @Input() isSubMenu = false;
  // @Input() min = 0;

  public previousValue = [];
  public datas: any[] = [];
  public childDatas = [];
  public selectedItems = [];
  public searchText = '';
  public itemHover;
  public align = '';
  public isOpen = false;
  public isDisabled = false;
  public optionsStyle: object = {};
  public options2ndStyle: object = {};
  public arrowPosition: object = {};
  public arrow2ndPosition: object = {};
  public innerArrow2ndPosition: object = {};
  public storageSelectedItem = [];
  public treeData: DropdownTreeModel = { parent: [], current: [], children: [] };

  private _search: Rx.Subject<any> = new Rx.Subject();
  private _itemHover: Rx.Subject<any> = new Rx.Subject();
  private _toggleRender: Rx.Subject<any> = new Rx.Subject();

  @ViewChild('widgetsContent', { read: ElementRef }) public widgetsContent: ElementRef<any>;
  ngOnDestroy(): void {
    this._search.unsubscribe();
    this._itemHover.unsubscribe();
    this._toggleRender.unsubscribe();
  }
  ngOnInit(): void {
    if (!this.isDisabled) {
      document.addEventListener('click', e => {
        const elDropdown: HTMLElement = (e.target as HTMLElement).closest('.sDropdown');
        const elOptions: HTMLElement = (e.target as HTMLElement).closest('.sDropdown-menu');
        const elItems: HTMLElement = (e.target as HTMLElement).closest('.sDropdown-item');
        const elItemSelected: HTMLElement = (e.target as HTMLElement).closest('.sDropdown-select');
        if ((!elDropdown || (elDropdown !== this._element.nativeElement.getElementsByClassName('sDropdown')[0]))
          && (!elOptions || (elOptions !== document.getElementsByClassName('sDropdown-menu')[0])) && !(elItems || elItemSelected)) {
          this.hidePopup();
        }
      });
      const mousewheel: Rx.Observable<Event> = Rx.fromEvent(window, 'mousewheel');
      mousewheel.pipe(debounceTime(200)).subscribe(e => this.setDropdownRect());
      this._search.pipe(debounceTime(500)).subscribe(() => {
        this.filterEventObj.text = this.searchText;
        this.filtering.emit(this.filterEventObj);
        if (!this.filterEventObj.cancel) {
          const text = this.searchText.trim().toLocaleLowerCase();
          // this.datas = this.dataSource.filter(s => !text || (s[this.fields['text']] as string).toLocaleLowerCase().search(text) > -1);
          const searchTexts = (dataSource) => {
            let dataSearch = [];
            dataSource.forEach(e => {
              if (e?.children?.length > 0) {
                dataSearch = dataSearch.concat(searchTexts(e.children));
              }
            });
            dataSearch = dataSearch.concat(dataSource.filter(s => !text || (s[this.fields.text] as string).toLocaleLowerCase().search(text) > -1));
            return dataSearch;
          };
          if (!text) {
            this.datas = this.treeData.children = this.dataSource;
            this.treeData.current = [];
          }
          else { this.datas = searchTexts(this.dataSource); }
        }
      });
      this._itemHover.subscribe(item => {
        if (!item) { this.itemHover = null; }
        if (this.itemHover != item[this.fields.value]) {
          this.closeSubMenu();
        }
        this.itemHover = item[this.fields.value];
      });
    }
    if (this.selectedItem?.length > 0) { this.selectedItems = this.selectedItem; }
    this.datas = this.dataSource;
    if (this.allowFiltering == null || this.allowFiltering == undefined) {
      this.allowFiltering = !(this.isFilterDropdown || this.isSelectedDropdown || this.isSubMenu);
    }
    if (this.showTooltip == null || this.showTooltip == undefined) {
      this.showTooltip = !(this.isFilterDropdown || this.isSelectedDropdown || this.isSubMenu);
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ngModel) {
      this.value = this.ngModel;
      this.selectValue();
    }
    if (changes.selectedItem) {
      this.value = this.selectedItem.map(s => s[this.fields.value]);
      this.selectValue();
    }
    if (changes.value) {
      this.value = this.value || [];
      this.selectValue();
    }
    if (changes.dataSource) {
      this.datas = this.dataSource;
      this.selectValue();
    }
    if (changes.max && changes.max.currentValue == 1) {
      this.mode = Mode.single;
    }
    if (changes.mode && changes.mode.currentValue == Mode.single) {
      this.max = 1;
    }
  }
  ngAfterViewInit() {
    const dropdown = (this.dropdown.nativeElement as HTMLElement);
    this.optionsStyle['left.px'] = dropdown.getBoundingClientRect().left;
    this.optionsStyle['top.px'] = dropdown.getBoundingClientRect().bottom + 4;
    this._toggleRender.pipe(debounceTime(300), distinctUntilChanged((x, y) => x.type == y.type)).subscribe(e => {
      this.created.emit();
    });
  }
  onDomChange(e) {
    this._toggleRender.next(e);
  }
  writeValue(value: any): void {
    if (value) {
      this.value = value;
      this.selectValue();
    }
  }
  _onChange = (val) => { };
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  _onTouched = () => { };
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this._renderer.setProperty(this._element.nativeElement, 'disabled', isDisabled);
  }
  updateData(data: any[]) {
    this.datas = this.dataSource = data;
    this.selectValue();
  }
  selectValue() {
    if (this.value && this.datas) {
      if (!Array.isArray(this.value)) { this.value = [this.value]; }
      this.selectedItems = this.selectedItems.filter(s => this.value.includes(s[this.fields.value]));
      this.value.forEach(val => {
        if (!this.selectedItems.some(a => a[this.fields.value] == val)) {
          const data = this.datas.find(a => a[this.fields.value] == val);
          if (data) {
            this.selectedItems.push(data);
          }
        }
      });
    }
  }
  onSearch(e) {
    this._search.next();
  }
  mouseenter(e, item) {
    if (this.mode != Mode.multi && this.isSubMenu) {
      this._itemHover.next(item);
      if (this.fields.children && item[this.fields.children.name]) {
        this.childDatas = item[this.fields.children.name];
        if (!this.options2ndEmbeddedViewRef) {
          this.options2ndEmbeddedViewRef = this.viewContainerRef.createEmbeddedView(this.options2ndPanel);
          this.options2ndEmbeddedViewRef.detectChanges();

          for (const node of this.options2ndEmbeddedViewRef.rootNodes) {
            this.options2nd = node;
            document.body.appendChild(node);
          }
          setTimeout(() => {
            this.setSubMenuRect(e.target);
            const options2nd = (this.options2nd.nativeElement as HTMLElement).getElementsByClassName('sDropdown-items');
            if (options2nd) { options2nd[0].scrollTop = 0; }
          }, 10);
        }
      }
    }
  }
  mouseleave(item) {
    if (!this.fields.children || !item[this.fields.children.name]) {
      this.itemHover = null;
    }
  }
  closeSubMenu() {
    if (this.options2ndEmbeddedViewRef) {
      this.options2ndEmbeddedViewRef.destroy();
      this.options2ndEmbeddedViewRef = null;
    }
  }
  selectSubMenu(item) {
    if (this.fields.children && item[this.fields.children.name]) { return; }
    this.max = 1;
    const previousItem = this.selectedItems;
    this.selectedItems = [item];

    this.value = [item[this.fields.value]];
    this.ngModel = item[this.fields.value];
    this.ngModelChange.emit(this.ngModel);
    this._onChange(this.getValue());
    const res: SDropdownEventArgs = {
      isSelect: true,
      value: this.getValue(),
      selectValue: item[this.fields.value],
      selectedItem: item,
      selectedItems: this.selectedItems
    };
    this.selected.emit(res);
    this.change.emit(res);
  }
  rollback(item) {
    this.datas = this.treeData.children = item.parent[item.parent?.length - 1];
    this.treeData.current.pop();
    this.treeData.parent.pop();
  }
  select(item,isParent) {
    if (this.fields.children && item[this.fields.children.name]?.length > 0 && !this.isSubMenu && !isParent) {
      this.treeData.parent.push(this.datas);
      this.treeData.current.push(item);
      this.datas = this.treeData.children = item[this.fields.children.name];
      return;
    }
    if (this.mode != Mode.multi) {
      this.max = 1;
      this.selectedItems = [];
    } else {
      if (this.max <= this.value?.length) {
        return;
      }
    }
    let isSelected = true;
    if (this.selectedItems.some(s => s[this.fields.value] == item[this.fields.value])) {
      isSelected = false;
      this.selectedItems = this.selectedItems.filter(s => s[this.fields.value] != item[this.fields.value]);
    } else {
      this.selectedItems.push(item);
    }
    this.value = this.selectedItems.map(s => s[this.fields.value]);
    this.ngModel = this.getValue();
    this.ngModelChange.emit(this.ngModel);
    this._onChange(this.getValue());

    const res: SDropdownEventArgs = {
      isSelect: isSelected,
      value: this.getValue(),
      selectValue: item[this.fields.value],
      selectedItem: item,
      selectedItems: this.selectedItems
    };
    if (isSelected) {
      this.selected.emit(res);
    } else {
      res.removedItems = [item];
      this.removed.emit(res);
    }
    if (this.max === this.value?.length ||isParent) {
      this.hidePopup();
    }
    if (this.allowSelected) {
      this.selectedItem = this.selectedItems;
    }
    this.change.emit(res);
    if (this.mode === Mode.btn) {
      this.value = [];
      this.selectedItems = [];
    }
    setTimeout(() => {
      this.setDropdownRect();
    }, 10);
  }
  countSelected(data) {
    let runningSum = 0;
    data.forEach(e => {
      if (e.children?.length > 0) {
        runningSum += this.countSelected(e.children);
      }
      else {
        runningSum += this.selectedItems.find(s => s[this.fields.value] == e[this.fields.value]) ? 1 : 0;
      }
    });
    return runningSum;

  }
  calculate(data) {
    return this.countSelected(data);
  }
  getValue() {
    return this.mode != Mode.multi ? (this.value[0] || null) : this.value;
  }
  removeAll() {
    this.removed.emit({ value: [], selectedItems: [], removedItems: this.selectedItems });
    this.selectedItems = [];
    this.ngModel = this.value = this.selectedItems.map(s => s[this.fields.value]);
    this.ngModelChange.emit(this.ngModel);
    this._onChange(this.getValue());
  }
  getActive(item) {
    return this.mode != Mode.btn && this.value?.some(s => s == item[this.fields.value]);
  }
  onClose() {
    this.hidePopup();
  }
  onOpen() {
    this.showPopup();
  }
  hidePopup() {
    if (this.isOpen) {
      this._onTouched();
      this.close.emit(this.closeEventObj);
      if (this.closeEventObj.cancel) { return; }
      this.optionsEmbeddedViewRef.destroy();
      this.closeSubMenu();
      this.itemHover = null;
    }
    this.isOpen = false;
    this.searchText = '';
  }
  showPopup() {
    if (this.dataSource) {
      this.open.emit(this.openEventObj);
      if (this.openEventObj.cancel) { return; }
      this.isOpen = true;

      this.optionsEmbeddedViewRef = this.viewContainerRef.createEmbeddedView(this.optionsPanel);
      this.optionsEmbeddedViewRef.detectChanges();

      for (const node of this.optionsEmbeddedViewRef.rootNodes) {
        this.options = node;
        document.body.appendChild(node);
      }

      setTimeout(() => {
        if (this.searchEle) { this.searchEle.nativeElement.focus(); }
        if (this.options) {
          this.setDropdownRect();
          const options = (this.options?.nativeElement as HTMLElement).getElementsByClassName('sDropdown-items');
          if (options) { options[0].scrollTop = 0; }
        }
      }, 100);
    }
  }
  toggle(e: MouseEvent) {
    if ((e.target as HTMLElement).closest('.fa-times')) { return; }
    if (this.isOpen) {
      this.hidePopup();
    } else {
      this.showPopup();
    }
  }
  onAddBtnClick() {
    this.addBtnClick.emit();
    this.hidePopup();
  }
  setSubMenuRect(el: HTMLElement) {
    if (this.isOpen) {
      const options = (this.options.nativeElement as HTMLElement);
      const optionsRect = (options.getBoundingClientRect());
      const options2nd = (this.options2nd.nativeElement as HTMLElement);
      const options2ndRect = (options2nd.getBoundingClientRect());
      const elRect = (el.getBoundingClientRect());

      const popupWidth = options2ndRect.width;

      // X
      const left = optionsRect.left - popupWidth;
      if ((this.isFilterDropdown || this.align == 'right') && left > 0) {
        this.arrow2ndPosition = {
          'left.px': popupWidth - 6,
          'border-bottom-color': 'transparent',
          'border-left-color': 'transparent'
        };
        this.options2ndStyle['left.px'] = left - 14;
      } else {
        this.arrow2ndPosition = {
          'left.px': - 6,
          'border-top-color': 'transparent',
          'border-right-color': 'transparent'
        };
        this.options2ndStyle['left.px'] = optionsRect.right + 14;
      }
      this.innerArrow2ndPosition = JSON.parse(JSON.stringify(this.arrow2ndPosition));
      this.innerArrow2ndPosition['left.px'] = this.arrow2ndPosition['left.px'] - 2;

      // Y
      if (elRect.top + options2ndRect.height > window.innerHeight) {
        this.options2ndStyle['top.px'] = elRect.bottom - options2ndRect.height - 6;
        this.position = DropType.Dropup;
      } else {
        this.options2ndStyle['top.px'] = elRect.top - 6;
        this.position = DropType.Dropdown;
      }
    }
  }
  setDropdownRect() {
    if (this.isOpen) {
      const options = (this.options.nativeElement as HTMLElement);
      const optionsRect = (options.getBoundingClientRect());
      const dropdown = (this.dropdown.nativeElement as HTMLElement);
      const targetRect = (dropdown.getBoundingClientRect());
      if (targetRect.bottom < 0 || window.innerHeight < targetRect.top) {
        this.hidePopup();
      } else {
        let popupWidth = this.popupWidth_px ? this.popupWidth_px : targetRect.width;
        popupWidth = popupWidth < 150 ? optionsRect.width : popupWidth;
        this.optionsStyle['width.px'] = popupWidth;

        // X
        const left = targetRect.left + (targetRect.width / 2) - (popupWidth / 2);
        if (this.isFilterDropdown || this.align == 'right' || left + popupWidth > window.innerWidth) {
          this.align = 'right';
          this.arrowPosition = { 'left.px': popupWidth - (targetRect.width / 2 + 8) };
          this.optionsStyle['left.px'] = targetRect.right - popupWidth;
        } else if (this.align == 'left' || left < 0) {
          this.align = 'left';
          this.arrowPosition = { 'left.px': targetRect.width / 2 - 8 };
          this.optionsStyle['left.px'] = targetRect.left;
        } else {
          this.arrowPosition = {};
          this.align = 'center';
          this.optionsStyle['left.px'] = left;
        }

        // Y
        const top = targetRect.top - optionsRect.height - 4;
        if (top > 0 && targetRect.bottom + optionsRect.height > window.innerHeight) {
          this.optionsStyle['top.px'] = top;
          this.position = DropType.Dropup;
        } else {
          this.optionsStyle['top.px'] = targetRect.bottom + 4;
          this.position = DropType.Dropdown;
        }
      }
    }
  }

  public scrollRight(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 275), behavior: 'smooth' });
  }

  public scrollLeft(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 275), behavior: 'smooth' });
  }
}
