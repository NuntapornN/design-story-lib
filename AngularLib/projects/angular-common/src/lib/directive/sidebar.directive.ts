import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Directive, ElementRef, EmbeddedViewRef, EventEmitter, HostListener, Injector, Input, Output, ViewContainerRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Directive({
  selector: '[s-sidebar]'
})
export class SidebarDirective {
  @Input('s-sidebar') component: any;
  @Input() sbName: any;
  @Input() sbData: any = {};

  @Output() dynamicEvent: EventEmitter<any> = new EventEmitter<any>();
  private static _componentRef: ComponentRef<any>[] = [];
  private static _previousPath = '';
  constructor(private el: ElementRef, private _appRef: ApplicationRef, private injector: Injector,
    private cfr: ComponentFactoryResolver, private vcr: ViewContainerRef, private router: Router) {
    router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      if (SidebarDirective._previousPath != event.url.split('/')[1]) {
        this.destroyComponent();
      }
      SidebarDirective._previousPath = event.url.split('/')[1];
    });
  }
  destroyComponent() {
    Object.keys(SidebarDirective._componentRef).forEach(name => {
      SidebarDirective._componentRef[name].destroy();
      delete SidebarDirective._componentRef[name];
    });
  }
  @HostListener('click') onMouseEnter(componentName = this.sbName, component = this.component, data = this.sbData, closeOther = true) {
    Object.keys(SidebarDirective._componentRef).forEach(name => {
      if ((SidebarDirective._componentRef[name].instance.sidebar.isOpen && closeOther) || name == componentName) {
        SidebarDirective._componentRef[name].instance.toggleByDirective(data);
      }
    });
    if (!SidebarDirective._componentRef[componentName]) {
      // --- create component and append to body 
      SidebarDirective._componentRef[componentName] = this.cfr.resolveComponentFactory(component).create(this.injector);
      this._appRef.attachView(SidebarDirective._componentRef[componentName].hostView);

      const domElem = (SidebarDirective._componentRef[componentName].hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
      document.body.appendChild(domElem);

      SidebarDirective._componentRef[componentName].instance.onReady.subscribe(() => {
        setTimeout(() => {
          SidebarDirective._componentRef[componentName].instance.toggleByDirective(data || {});
        }, 100);
      });

      Object.keys(SidebarDirective._componentRef[componentName].instance).forEach(attr => {
        if (SidebarDirective._componentRef[componentName].instance[attr] instanceof EventEmitter) {
          SidebarDirective._componentRef[componentName].instance[attr].subscribe(e => {
            let data = {
              event: componentName + '_' + attr,
              data: e
            };
            this.dynamicEvent.emit(data)
          });
        }
      });
    }
  }

}
