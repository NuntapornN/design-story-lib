import {
  AfterContentInit, AfterViewInit, ChangeDetectorRef, Directive, ElementRef, HostBinding,
  Inject, Input, NgZone, OnDestroy, OnInit
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DEFAULT_PICTURE_PLACEHOLDER } from '../constansts/global.constansts';
import { Configuration, LibConfig } from '../model/config';
import { AssetsService } from '../services/assets.service';
import { AttachmentSecureService } from '../services/attachment-secure.service';
import { ImageStorageService } from '../services/image-storage.service';
import { Attachment } from './../model/venio.model';

@Directive({
  selector: 'img[sAttLazyload]',

})
export class AttachmentLazyloadDirective implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
  @HostBinding('attr.src') srcAttr = null;
  @Input('sAttLazyload') src: Attachment;
  @Input() isAllCompany: boolean = false;
  @Input() isAnnoucement: boolean = false;
  @Input() isAutoResize: boolean = false;
  @Input() customCacheKey?: string;
  private sub: Subscription;
  private obs: IntersectionObserver;
  private element: HTMLImageElement;
  private rect: DOMRect;

  constructor(
    private el: ElementRef,
    private service: AttachmentSecureService,
    private imageStorage: ImageStorageService,
    private ngZone: NgZone,
    private changeRef: ChangeDetectorRef,
    private assets : AssetsService,
    @Inject('LIB_CONFIG') private libConfig: LibConfig,
  ) {
    Configuration.setLibConfig(libConfig);
    this.srcAttr = DEFAULT_PICTURE_PLACEHOLDER.Loading;
    this.element = el.nativeElement;
  }

  ngOnInit() {
    let url = this.src.url;
    if (this.src.resourceUrl) {
      url = this.src.resourceUrl.replace('https://storage.googleapis.com/veniocrm-dev', 'https://veniocrm-dev.storage.googleapis.com');
    } else if (this.src.url) {
      url = Configuration.getConfig().apiUrlBase + this.src.url;
    }
    this.srcAttr = this.imageStorage.getTrustResourceUrl(this.customCacheKey || url) || this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Loading);
  }

  ngAfterViewInit() {
    if (this.src === null || this.src === undefined) {
      this.srcAttr = DEFAULT_PICTURE_PLACEHOLDER.NoPhoto;
      return;
    }
    if (this.srcAttr !== DEFAULT_PICTURE_PLACEHOLDER.Loading) return;
    this.rect = this.element.getBoundingClientRect();
    (this.el.nativeElement as HTMLElement).addEventListener('error', this.onError.bind(this));
    this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
    this.changeRef.markForCheck();
  }
  ngAfterContentInit(): void {
    this.rect = this.element.getBoundingClientRect();
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.obs) {
      this.obs.unobserve(this.el.nativeElement);
    }
    (this.el.nativeElement as HTMLElement).removeEventListener('error', this.onError)
  }

  onError() {
    this.srcAttr = DEFAULT_PICTURE_PLACEHOLDER.NoPhoto;
    this.changeRef.markForCheck();
  }


  private canLazyLoad() {
    return window && 'IntersectionObserver' in window;
  }

  private lazyLoadImage() {
    this.obs = new IntersectionObserver(entries => {
      this.ngZone.runOutsideAngular(() => {
        entries.forEach(({ isIntersecting }) => {
          if (isIntersecting) {
            this.obs.unobserve(this.el.nativeElement);
            this.loadImage();
          }
        });
      });
    });
    this.obs.observe(this.el.nativeElement);
  }

  private loadImage() {
    this.sub = this.service
      .transform(this.src, this.isAllCompany, this.isAnnoucement, this.customCacheKey, this.isAutoResize ? this.rect : null)
      .subscribe((x: any) => {
        this.srcAttr = x;
        this.changeRef.markForCheck();
        if (x instanceof Object) {
          if (this.sub) this.sub.unsubscribe();
        }
      });
  }
}
