import { AssetsService } from './../services/assets.service';
import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Directive, ElementRef, HostBinding, Input, NgZone, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { DEFAULT_PICTURE_PLACEHOLDER } from '../constansts/global.constansts';
import { SECURE_DATA_URL_TYPE } from './../constansts/global.constansts';
import { SecureDataUrlService } from './../services/secure-data-url.service';
import { ImageStorageService } from './../services/image-storage.service';

@Directive({
  selector: 'img[sImgLazyload]'
})
export class ImgLazyloadDirective implements OnInit, AfterViewInit, AfterContentInit, OnDestroy, OnChanges {
  @HostBinding('attr.src') srcAttr = null;
  @Input('sImgLazyload') src: string;
  @Input() secureDataUrlType = 'attachment';
  @Input() isAllCompany = false;
  @Input() isAutoResize = false;
  @Input() customCacheKey?: string;
  private sub: Subscription;
  private obs: IntersectionObserver;
  private element: HTMLImageElement;
  private rect: DOMRect;

  constructor(
    private el: ElementRef,
    private secureDataUrlService: SecureDataUrlService,
    private imageStorage: ImageStorageService,
    private ngZone: NgZone,
    private changeRef: ChangeDetectorRef,
    private assets : AssetsService
  ) {
    this.element = el.nativeElement;
  }
  ngOnChanges(changes: SimpleChanges): void {
    const srcChange = changes.src;
    if (srcChange === null || srcChange === undefined) { return; }
    if (srcChange.previousValue) {
      this.ngOnInit();
      this.loadImage(true);
    }
  }

  ngOnInit() {
    this.srcAttr = this.imageStorage.getTrustResourceUrl(this.customCacheKey || this.src) || this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Loading);
    this.element.crossOrigin = 'anonymous';
    this.element.referrerPolicy = 'no-referrer';
    this.changeRef.markForCheck();
  }

  ngAfterViewInit() {
    if (this.src === null || this.src === undefined) {
      return;
    }
    if (this.srcAttr !== this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Loading)) { return; }
    this.rect = this.element.getBoundingClientRect();
    (this.el.nativeElement as HTMLElement).addEventListener('error', this.onError.bind(this));
    this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
    this.changeRef.markForCheck();
  }

  ngAfterContentInit(): void {
    this.rect = this.element.getBoundingClientRect();
  }

  onError() {
    if (this.secureDataUrlType === SECURE_DATA_URL_TYPE.UserPicture || this.secureDataUrlType === SECURE_DATA_URL_TYPE.FullPathUser) { this.srcAttr = this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.User); }
    else { this.srcAttr = this.assets.transform(DEFAULT_PICTURE_PLACEHOLDER.Photo);}
    this.changeRef.markForCheck();
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.obs) {
      this.obs.unobserve(this.el.nativeElement);
    }
    (this.el.nativeElement as HTMLElement).removeEventListener('error', this.onError);
  }

  private canLazyLoad() {
    return window && 'IntersectionObserver' in window;
  }

  private lazyLoadImage() {
    this.obs = new IntersectionObserver(entries => {
      entries.forEach(({ isIntersecting }) => {
        if (isIntersecting) {
          this.obs.unobserve(this.el.nativeElement);
          this.loadImage();
        }
      });
    });
    this.obs.observe(this.el.nativeElement);
  }

  private loadImage(noCahce = false) {
    this.sub = this.secureDataUrlService
      .transform(this.src, this.secureDataUrlType, this.isAllCompany, noCahce, this.customCacheKey, this.isAutoResize ? this.rect : null)
      .subscribe((x: any) => {
        this.srcAttr = x;
        this.changeRef.markForCheck();
        if (x instanceof Object) {
          if (this.sub) { this.sub.unsubscribe(); }
        }
      });
  }

}
