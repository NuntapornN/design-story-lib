export var items = [
    { "id": "th-TH", "text": "ภาษาไทย", "iconCss": "flag flag-th" },
    { "id": "en-GB", "text": "English (United Kingdom)", "iconCss": "flag flag-gb" },
    { "separator": true },
    { "id": "changePassword", "text": "change_password" },
    {
        "id": "notification", "text": "notification", 
        "children": [
            { "id": "email", "text": "notification_email" },
            { "id": "inApp", "text": "notification_inapp" },
        ]
    },
    { "id": "logout", "text": "logout" },
    { "separator": true },
    { "id": "app-version", "text": "version", "version": "5.24.0", "disabled": true }
];
export var customers = [
    {
        disabled: true,
        "customerId": 111709,
        "customerName": "Janejira",
        "groupName": "999 รวย",
        "classificationName": "CC",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2021-01-07T09:00:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "count": 2,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79114,
        "customerName": "company onlylyly",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 8,
                "topicName": "V-Visit"
            },
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2021-01-06T16:00:00",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "count": 1,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 111710,
        "customerName": "ทดสอบสร้างแบบ ไม่มี owner",
        "classificationName": "123",
        "owners": [],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2021-01-06T15:30:35.84",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 85298,
        "customerName": "สร้างใหม่",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            },
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            }
        ],
        "latestUpdateByUser": "ธิปไตย",
        "latestUpdateType": "SalesOrder",
        "dateLatestUpdated": "2021-01-06T11:17:27",
        "activityStats": [
            {
                "count": 7,
                "type": "Conversation"
            },
            {
                "count": 3,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 111708,
        "customerName": "Test by benzi",
        "classificationName": "CC",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2021-01-05T16:59:34.5733333",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 111701,
        "customerName": "fa",
        "classificationName": "S",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Deal",
        "dateLatestUpdated": "2021-01-05T00:00:00",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110939,
        "customerName": "TEST COMPANY",
        "classificationName": "340",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2020-12-23T18:56:32.3866667",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110170,
        "customerName": "12++",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-23T14:53:00",
        "activityStats": [
            {
                "count": 4,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110163,
        "customerName": "wowowowowo",
        "classificationName": "36",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-23T13:34:00",
        "activityStats": [
            {
                "count": 4,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110160,
        "customerName": "ohoooo",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-23T13:22:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110158,
        "customerName": "printza",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 6,
                "topicName": "IMC"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-23T11:16:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110174,
        "customerName": "Pangleader",
        "classificationName": "A",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            }
        ],
        "latestUpdateByUser": "ธิปไตย",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2020-12-22T14:53:30.4033333",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 107674,
        "customerName": "name ios",
        "groupName": "test customerGroup2",
        "classificationName": "BB",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Deal",
        "dateLatestUpdated": "2020-12-22T14:40:47",
        "activityStats": [
            {
                "count": 3,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110171,
        "customerName": "Printengja",
        "groupName": "test customerGroup2",
        "classificationName": "A",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "ธิปไตย",
        "latestUpdateType": "Modified",
        "dateLatestUpdated": "2020-12-22T14:16:57.7433333",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110169,
        "customerName": "test create ios",
        "classificationName": "A",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Modified",
        "dateLatestUpdated": "2020-12-21T15:52:43.0766667",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79112,
        "customerName": "name oonlyyyy company",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 8,
                "topicName": "V-Visit"
            },
            {
                "topicId": 31,
                "topicName": "YEAST"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Deal",
        "dateLatestUpdated": "2020-12-21T10:02:03",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110145,
        "customerName": "leadA01",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Deal",
        "dateLatestUpdated": "2020-12-17T18:13:47",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110168,
        "customerName": "noownerja",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Modified",
        "dateLatestUpdated": "2020-12-17T16:08:03.7633333",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79101,
        "customerName": "test terrr",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2020-12-16T18:49:14",
        "activityStats": [
            {
                "count": 2,
                "type": "Conversation"
            },
            {
                "count": 1,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79043,
        "customerName": "nutcga",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-16T16:34:18",
        "activityStats": [
            {
                "count": 5,
                "type": "Conversation"
            },
            {
                "count": 1,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110164,
        "customerName": "mytel",
        "classificationName": "222",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 115,
                "topicName": "Inkjet"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-16T10:49:00",
        "activityStats": [
            {
                "count": 2,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110162,
        "customerName": "haha",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2020-12-16T10:04:50.7166667",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110161,
        "customerName": "mygod",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Created",
        "dateLatestUpdated": "2020-12-16T10:00:42.6166667",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110159,
        "customerName": "printzoo",
        "groupName": "test customerGroup3",
        "classificationName": "A",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 7,
                "topicName": "eTax"
            }
        ],
        "latestUpdateByUser": "ธิปไตย",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-15T17:33:00",
        "activityStats": [
            {
                "count": 3,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 85096,
        "customerName": "ทดสอบBBB",
        "owners": [
            {
                "id": "ab86aaee-6230-4194-bb9a-131486790b89",
                "title": "staff77 last",
                "pictureUrl": "User/ab86aaee-6230-4194-bb9a-131486790b89/Thumbnail?v=0"
            },
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 31,
                "topicName": "YEAST"
            }
        ],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Modified",
        "dateLatestUpdated": "2020-12-14T11:03:40.6633333",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79106,
        "customerName": "พอเถอะ",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-11T14:43:53",
        "activityStats": [
            {
                "count": 4,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79038,
        "customerName": "ตั้งชื่ออะไรดี",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            },
            {
                "id": "45813b91-12be-46f0-9adc-6b396f864852",
                "title": "ภูมิพัฒน์ จิระตระกูลวัชร์",
                "pictureUrl": "User/45813b91-12be-46f0-9adc-6b396f864852/Thumbnail?v=0"
            },
            {
                "id": "5639ad4c-465a-4d5c-ac48-2db2dc0b0b0a",
                "title": "pohnpawit buaban",
                "pictureUrl": "User/5639ad4c-465a-4d5c-ac48-2db2dc0b0b0a/Thumbnail?v=0"
            },
            {
                "id": "fa7a9126-a731-42a1-b28e-27661b3fd281",
                "title": "ไนน์ เชียร์ลุง",
                "pictureUrl": "User/fa7a9126-a731-42a1-b28e-27661b3fd281/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            },
            {
                "topicId": 6,
                "topicName": "IMC"
            },
            {
                "topicId": 8,
                "topicName": "V-Visit"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2020-12-10T19:00:08",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "count": 14,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 109592,
        "customerName": "fe41d0f1-ce30-45d4-bbda-63cd63e72bea-Edit",
        "owners": [
            {
                "id": "00e710f0-f4bd-4839-a631-a86f68ae19de",
                "title": "พิชชากร จันทณี",
                "pictureUrl": "User/00e710f0-f4bd-4839-a631-a86f68ae19de/Thumbnail?v=0"
            },
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-10T16:54:26",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "count": 1,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110147,
        "customerName": "Test 111233",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 5,
                "topicName": "Venio"
            },
            {
                "topicId": 8,
                "topicName": "V-Visit"
            }
        ],
        "latestUpdateByUser": "สุทธวีร์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-10T16:53:38",
        "activityStats": [
            {
                "count": 2,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110061,
        "customerName": "0e0ceb7b-094a-46c1-8bb5-6fba133b39f6",
        "owners": [],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2020-12-08T15:00:00",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "count": 1,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 107804,
        "customerName": "12",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-05T15:17:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79092,
        "customerName": "sawatdee",
        "owners": [
            {
                "id": "1811a1c3-e039-47dd-a77a-e86494d33aa3",
                "title": "testFirst25 testLast25",
                "pictureUrl": "User/1811a1c3-e039-47dd-a77a-e86494d33aa3/Thumbnail?v=0"
            },
            {
                "id": "f4a575ba-e375-4006-a2ea-d19dade656b5",
                "title": "ธัชพสุ โชติจินดาชัชกุล",
                "pictureUrl": "User/f4a575ba-e375-4006-a2ea-d19dade656b5/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 36,
                "topicName": "whyผ้าม่าน"
            },
            {
                "topicId": 38,
                "topicName": "เอื้ออออออออออออvbvbvbvbvb"
            },
            {
                "topicId": 39,
                "topicName": "เอื้ออออออบ55555555555555555555555"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-05T14:46:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 109960,
        "customerName": "fa930001-4faf-4e7e-89c7-43e6cb336106",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-05T14:45:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79098,
        "customerName": "งงละ",
        "owners": [
            {
                "id": "421487dc-3e41-4f58-811e-0a76d67445da",
                "title": "อัครภาส เกียรติเสริมขจร",
                "pictureUrl": "User/421487dc-3e41-4f58-811e-0a76d67445da/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-05T14:43:00",
        "activityStats": [
            {
                "count": 2,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 107300,
        "customerName": "test apk ohoto",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 52,
                "topicName": "empeo2"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-12-05T14:42:00",
        "activityStats": [
            {
                "count": 5,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110146,
        "customerName": "leadC01",
        "owners": [
            {
                "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
                "title": "จุติพันธุ์ มงคลสุธี",
                "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Deal",
        "dateLatestUpdated": "2020-12-04T00:00:00",
        "activityStats": [
            {
                "count": 5,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 79041,
        "customerName": "kguxnidye9r",
        "owners": [
            {
                "id": "eea8d646-5fb4-4efd-a3de-9cd0114b9d47",
                "title": "สุทธวีร์ เส็งทอง",
                "pictureUrl": "User/eea8d646-5fb4-4efd-a3de-9cd0114b9d47/Thumbnail?v=0"
            }
        ],
        "topicInterests": [],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Plan.Create",
        "dateLatestUpdated": "2020-11-30T12:00:33",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "count": 4,
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110074,
        "customerName": "be7432b6-e9d3-497f-9603-af25a894003a",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            },
            {
                "id": "ee6d421e-821d-4733-be1d-291b517e2a82",
                "title": "testFirst testLast",
                "pictureUrl": "User/ee6d421e-821d-4733-be1d-291b517e2a82/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-11-24T19:15:00",
        "activityStats": [
            {
                "count": 2,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110076,
        "customerName": "efef7b7d-dcb8-49fe-9082-b16318722601",
        "owners": [
            {
                "id": "008d6dfd-974c-4d48-82d6-703340aa2028",
                "title": "อาทิตย์ สารคุณ",
                "pictureUrl": "User/008d6dfd-974c-4d48-82d6-703340aa2028/Thumbnail?v=0"
            },
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "จุติพันธุ์",
        "latestUpdateType": "Conversation",
        "dateLatestUpdated": "2020-11-16T02:25:00",
        "activityStats": [
            {
                "count": 1,
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    },
    {
        "customerId": 110075,
        "customerName": "ccdd1518-d0b3-42d6-90c0-7c3f75d30414-Edit",
        "classificationName": "123",
        "owners": [
            {
                "id": "03e34d65-5e4d-462e-89d9-2ceccda4dd53",
                "title": "ธิปไตย ศรีสนอง",
                "pictureUrl": "User/03e34d65-5e4d-462e-89d9-2ceccda4dd53/Thumbnail?v=0"
            },
            {
                "id": "92828e3c-06e3-4179-9607-9f44e082d5f9",
                "title": "จักรินทร์ สังหาร",
                "pictureUrl": "User/92828e3c-06e3-4179-9607-9f44e082d5f9/Thumbnail?v=0"
            }
        ],
        "topicInterests": [
            {
                "topicId": 32,
                "topicName": "VITAMINC"
            }
        ],
        "latestUpdateByUser": "ธิปไตย",
        "latestUpdateType": "Modified",
        "dateLatestUpdated": "2020-11-16T00:19:56.6566667",
        "activityStats": [
            {
                "type": "Conversation"
            },
            {
                "type": "Plan"
            }
        ],
        "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
    }
]

export var treeData = [
    { 
      "text": 'Laptop',
      "id": 1,
      "children": [
        {
            "text": 'Apple',
            "id": 11,
            "children": [{"text": 'Macbook Air' , "id": 111 ,"children":[]}, {"text": 'Macbook Pro',"id":112,"children":[]}]
        },
        { 
            "text": 'Microsoft',
            "id": 12,
            "children": 
                [{  "text": 'Surface Pro', 
                    "id": 121, 
                    "children":[{"text": 'Surface Pro XX' , "id": 1211 ,"children":[{"text": 'Surface Pro Max' , "id": 12111 ,"children":[]}
                ]}
            ]}]
        },
        {
            "text": 'Sumsung',
            "id": 13,
            "children": []
        }
      ]
    },
    {
        "text": 'Desktop',
        "id": 2,
        "children": [{"text": 'Dell', "id": 21,"children":[]}]
    }
];

export var selectedData= [{
    "customerId": 110146,
    "customerName": "leadC01",
    "owners": [
        {
            "id": "7fdd36b6-9f8b-44e9-98c8-bd71c36c7035",
            "title": "จุติพันธุ์ มงคลสุธี",
            "pictureUrl": "User/7fdd36b6-9f8b-44e9-98c8-bd71c36c7035/Thumbnail?v=0"
        }
    ],
    "topicInterests": [],
    "latestUpdateByUser": "จุติพันธุ์",
    "latestUpdateType": "Deal",
    "dateLatestUpdated": "2020-12-04T00:00:00",
    "activityStats": [
        {
            "count": 5,
            "type": "Conversation"
        },
        {
            "type": "Plan"
        }
    ],
    "pictureUrl": "https://www.w3schools.com/css/img_lights.jpg"
}];