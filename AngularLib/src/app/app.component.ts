import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
 
  constructor() {
    
  }

  ngOnInit(): void {
    //this.sourceValue = [79114];
    //this.test.setValue([79114]);
    // console.log(this.test)
  }


  ngAfterViewInit(): void {
    // this.upload.setValue(this.attachment);
  }
}
