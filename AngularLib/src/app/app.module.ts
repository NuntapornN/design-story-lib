// import { AngularCommonModule, Application, PhraseProject, Theme } from '@gofive/angular-common';

import { AngularCommonModule, Application, PhraseProject, Theme } from 'projects/angular-common/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
    AngularCommonModule.forRoot({
      storageEncryptionKey: 'hbopHuzFl/pZ6bQkQo7TtzxUH7EFKBVaZtaONdOoduU=',
      storageEncryptionKeyHash: 'OUVGQMYEx35NJUkTr+OY+niEBehMbdk0tzvyQNeH4Yc=',
      apiUrlBase: 'https://uat.tks.co.th/Venio4Api/',
      phraseProject: PhraseProject.GofiveCore,
      assetsEnvironment: 'dev',
      theme: Theme.SalesbearLight,
      application: Application.Salesbear
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
